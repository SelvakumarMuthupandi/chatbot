import React from "react";
import $ from "jquery";

import {
  createStore,
  createStyleSet,
  createDirectLine
} from "botframework-webchat";

import WebChat from "./WebChat";

import "./fabric-icons-inline.css";
import "./MinimizableWebChat.css";
import sideMenu from "./menu.svg";
import menubuttonCall from "./call.svg";
import menubuttonTalk from "./talk.svg";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.handleFetchToken = this.handleFetchToken.bind(this);
    this.handleMaximizeButtonClick = this.handleMaximizeButtonClick.bind(this);
    this.handleMinimizeButtonClick = this.handleMinimizeButtonClick.bind(this);
    this.handleSwitchButtonClick = this.handleSwitchButtonClick.bind(this);
    this.handleExpandMenuClick = this.handleExpandMenuClick.bind(this);
    this.handleAgentButtonClick = this.handleAgentButtonClick.bind(this);
    this.handleScheduleButtonClick = this.handleScheduleButtonClick.bind(this);
    const store = createStore({}, ({ dispatch }) => next => action => {
      var clientUserName = window.localStorage.getItem("clientUserName");
      var sessionUserName = window.sessionStorage.getItem("sessionUserName");
      var sessionConversationId = window.sessionStorage.getItem("webchat.convid");
      if (clientUserName === null) {
        clientUserName = "";
      } 
      if (action.type === "DIRECT_LINE/CONNECT") {
        console.log( "before sess   " + sessionConversationId);
        if ( typeof sessionConversationId !== undefined )
        {
          console.log( "sess   " + sessionConversationId);
          action.payload.userID = "You";
          action.payload.directLine.conversationId = sessionConversationId;
        }
      }

      if (action.type === "DIRECT_LINE/CONNECT_FULFILLED") {
        console.log( "ful  " + JSON.stringify(action.payload.directLine.conversationId));
        sessionStorage.setItem("webchat.userid", action.meta.userID);
        if ( sessionUserName === null)
        {
            dispatch({
              type: "WEB_CHAT/SEND_EVENT",
              payload: {
                from: { id: "You" },
                name: "setUserName",
                value: clientUserName
              }
            });
        }
      } else if (action.type === "DIRECT_LINE/INCOMING_ACTIVITY") {
        if (action.payload.activity.name === "getConversationId") {
          window.sessionStorage.setItem(
            "webchat.convid",
            action.payload.activity.value
          );         
          return;
        }
        if (action.payload.activity.name === "getUserName") {
          window.localStorage.setItem(
            "clientUserName",
            action.payload.activity.value
          );
          window.sessionStorage.setItem(
            "sessionUserName",
            action.payload.activity.value
          );          
          return;
        }
        if (action.payload.activity.from.role === "bot") {
          this.setState(() => ({ newMessage: true }));
        }
      }
      return next(action);
    });

    this.state = {
      minimized: true,
      newMessage: false,
      userInitials: "You",
      side: "right",
      store,
      styleSet: createStyleSet({
        backgroundColor: "Transparent",
        bubbleBackground: "rgba(0, 0, 255, .1)",
        bubbleFromUserBackground: "rgba(0, 255, 0, .1)",
        botAvatarInitials: "P"
      }),
      token: null
    };
  }

  async handleFetchToken() {
    if (!this.state.token) {
      const token = "2do-aloq6Ts.bLZuIP7TuWso5lTspLdVBoBG4hoZT4KKrw_QF5jN9CE";
      this.setState(() => ({ token }));
    }
  }

  handleMaximizeButtonClick() {
    this.setState(() => ({
      minimized: false,
      newMessage: false
    }));
  }

  handleMinimizeButtonClick() {
    this.setState(() => ({
      minimized: true,
      newMessage: false
    }));
  }

  handleSwitchButtonClick() {
    this.setState(({ side }) => ({
      side: side === "left" ? "right" : "left"
    }));
  }

  handleExpandMenuClick() {
    $(".wc-call-wrapper").slideToggle(500);
    $(".wc-console").toggleClass("active");
  }

  handleAgentButtonClick() {
    this.state.store.dispatch({
      type: "WEB_CHAT/SEND_MESSAGE",
      payload: { text: "customer service" }
    });
    this.handleExpandMenuClick();
  }

  handleScheduleButtonClick() {
    this.state.store.dispatch({
      type: "WEB_CHAT/SEND_MESSAGE",
      payload: { text: "schedule meeting" }
    });
    this.handleExpandMenuClick();
  }

  render() {
    const {
      state: { minimized, newMessage, side, store, styleSet, token }
    } = this;

    return (
      <div className="minimizable-web-chat">
        {minimized ? (
          <button className="maximize" onClick={this.handleMaximizeButtonClick}>
            <span
              className={
                token
                  ? "ms-Icon ms-Icon--MessageFill"
                  : "ms-Icon ms-Icon--Message"
              }
            />
            {newMessage && (
              <span className="ms-Icon ms-Icon--CircleShapeSolid red-dot" />
            )}
            <div className="chatCount" />
            <div className="chat-notification">How can I help you today?</div>
          </button>
        ) : (
          <div className={side === "left" ? "chat-box left" : "chat-box right"}>
            <header>
              <button className="wc-menu" onClick={this.handleExpandMenuClick}>
                <img alt="" src={sideMenu} className="menu-svg" />
              </button>
              <div className="chat-title">PreludeSys</div>
              <div className="filler" />
              <button className="switch" onClick={this.handleSwitchButtonClick}>
                <span className="ms-Icon ms-Icon--Switch" />
              </button>
              <button
                className="minimize"
                onClick={this.handleMinimizeButtonClick}
              >
                <span className="ms-Icon ms-Icon--ChromeMinimize" />
              </button>
            </header>
            <div className="wc-call-wrapper">
              <button className="wc-talk" onClick={this.handleAgentButtonClick}>
                <img alt="Agent" src={menubuttonTalk} className="talk-svg" />{" "}
                Talk to Agent
              </button>
              <button
                className="wc-call"
                onClick={this.handleScheduleButtonClick}
              >
                <img alt="Schedule" src={menubuttonCall} className="call-svg" />{" "}
                Schedule a Call
              </button>
            </div>
            <WebChat
              className="react-web-chat"
              onFetchToken={this.handleFetchToken}
              store={store}
              styleSet={styleSet}
              token={token}
            />
          </div>
        )}
      </div>
    );
  }
}

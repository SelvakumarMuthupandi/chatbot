import memoize from "memoize-one";
import React from "react";
import ReactWebChat, {
  createDirectLine,
  createStyleSet
} from "botframework-webchat";

import "./WebChat.css";

export default class extends React.Component {
  constructor(props) {
    super(props);
    const styleSet = createStyleSet({
      backgroundColor: "Transparent",
      bubbleBackground: "rgba(0, 0, 255, .1)",
      bubbleFromUserBackground: "rgba(0, 255, 0, .1)",
      botAvatarInitials: "P"
    });
    this.state = {
      styleSet: styleSet
    };
    styleSet.uploadButton = { ...styleSet.uploadButton, display: "none" };
    //var waterMark1 = sessionStorage.getItem("webchat.watermark");
    //var streamUrl1 = sessionStorage.getItem("webchat.streamUrl");
    var sessionUserName = sessionStorage.getItem("sessionUserName");
    var sessionConversationId = window.sessionStorage.getItem("webchat.convid");
   /* if (convId && waterMark1 && streamUrl1) {      
      this.createDirectLine = memoize(token => createDirectLine(
        { token : token,
          conversationId: convId,
          watermark: waterMark1,
          streamUrl: streamUrl1,
          webSocket:false,
          pollingInterval:5000
         }));
    }   
    else { */
      if (sessionUserName === null)
      {
        this.createDirectLine = memoize(token => createDirectLine({
          token: token
        }));
      }
      else
      {
        
      this.createDirectLine = memoize(token => createDirectLine({
        token: token ,
        conversationId: sessionConversationId,
        watermark: null,
        streamUrl: null,
        webSocket:false,
        pollingInterval:5000
      }));
    }
   // } 
    
  }

  componentDidMount() {
    !this.props.token && this.props.onFetchToken();
  }

  render() {
    const {
      props: { className, store, token },
      state: { styleSet }
    } = this;
    return token ? (
      <ReactWebChat
        className={`${className || ""} web-chat`}
        directLine={this.createDirectLine(token)}
        store={store}
        styleSet={styleSet}
      />
    ) : (
      <div className={`${className || ""} connect-spinner`}>
        <div className="content">
          <div className="icon">
            <span className="ms-Icon ms-Icon--Robot" />
          </div>
          <p>Please wait while we are connecting.</p>
        </div>
      </div>
    );
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using PreludeSysDebi.Logging;
using System.Text;
using System.IO;

namespace PreludeSysDebi.SMTPEmailService
{
    public class EmailService
    {

        private static IPreludeLogger _logger;
        private static IConfiguration _configuration;
        public EmailService(IPreludeLogger logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public void SendEmail(MailAddress[] ToAddresses, string mailSubject, string mailBody)
        {
            string fromEmail = _configuration.GetSection("EmailFrom")?.Value;
            string password = _configuration.GetSection("Password")?.Value;
            string SMTPOutGoingServer = _configuration.GetSection("OutGoingServer")?.Value;
            string OutGoingServerPort = _configuration.GetSection("OutGoingServerPort")?.Value;
            MailMessage msg = new MailMessage();
            Array.ForEach(ToAddresses, adr => msg.To.Add(adr));
            msg.From = new MailAddress(fromEmail);
            msg.Subject = mailSubject;
            msg.Body = mailBody;
            msg.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(fromEmail, password);
            client.Port = Convert.ToInt32(OutGoingServerPort);
            client.Host = SMTPOutGoingServer;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;

            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
        }

        public void SendMeetingInvites(MailAddress emailFrom, MailAddress[] emailTos, string subject,
             bool isMeeting, string bodyContent, Attachment attachedContent, string location, DateTime? start, DateTime? end,
             TimeZoneInfo timezone, int eventID, bool isCancel, bool htmlEnable, MailAddress adminEmail, string salesRepMessage)
        {
            string password = _configuration.GetSection("Password")?.Value;
            int outGoingServerPort = Convert.ToInt32(_configuration.GetSection("OutGoingServerPort")?.Value);
            string outGoingServer = _configuration.GetSection("OutGoingServer")?.Value;

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = emailFrom;
            mailMessage.CC.Add(emailFrom);
            mailMessage.Subject = subject;
            Array.ForEach(emailTos, toAddress =>
            {
                mailMessage.To.Add(toAddress);
            });
           
            if (isMeeting)
            {
                string meetingInfo = MeetingRequestString(emailFrom, emailTos, subject, bodyContent, location,
                    start.Value, end.Value, timezone, eventID, isCancel);
               
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(meetingInfo, ct);
                mailMessage.AlternateViews.Add(avCal);
            }
            else
            {
                mailMessage.Body = bodyContent;
            }
            mailMessage.IsBodyHtml = htmlEnable;
           
            SendEmailMsg(mailMessage);
            mailMessage = new MailMessage();
            mailMessage.From = emailFrom;
            mailMessage.Subject = subject;
            mailMessage.To.Add(adminEmail);
            mailMessage.IsBodyHtml = false;
            mailMessage.Attachments.Add(attachedContent);
            mailMessage.Body = salesRepMessage;
            SendEmailMsg(mailMessage);
        }

        private static void SendEmailMsg(  MailMessage msg)
        {

            string password = _configuration.GetSection("Password")?.Value;
            int outGoingServerPort = Convert.ToInt32(_configuration.GetSection("OutGoingServerPort")?.Value);
            string outGoingServer = _configuration.GetSection("OutGoingServer")?.Value;
            string fromEmail = _configuration.GetSection("EmailFrom").Value;

            using (SmtpClient client = new SmtpClient())
            {
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(fromEmail, password);
                client.Port = Convert.ToInt32(outGoingServerPort);
                client.Host = outGoingServer;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                try
                {
                    client.Send(msg);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.ToString());
                }
            }
        }
        private static string MeetingRequestString(MailAddress from, MailAddress[] tos, string subject, string desc, string location, DateTime startTime, DateTime endTime, TimeZoneInfo timezone, int? eventID = null,  bool isCancel = false)
        {
            StringBuilder str = new StringBuilder();

            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN");
            str.AppendLine("VERSION:2.0");
            str.AppendLine(string.Format("METHOD:{0}", (isCancel ? "CANCEL" : "REQUEST")));
            str.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
            str.AppendLine("BEGIN:VEVENT");

            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startTime.Subtract(  timezone.GetUtcOffset(DateTime.Now))));
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmss}", DateTime.UtcNow));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endTime.Subtract(timezone.GetUtcOffset(DateTime.Now))));
            str.AppendLine(string.Format("LOCATION: {0}", location));
            str.AppendLine(string.Format("UID:{0}", (eventID.HasValue ? "ChatBot_" + eventID : Guid.NewGuid().ToString())));
            str.AppendLine(string.Format("DESCRIPTION:{0}", desc.Replace(Environment.NewLine, "<br/>")));
            str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", $"{desc.Replace(Environment.NewLine, "<br>" ) }"));
           
            str.AppendLine(string.Format("SUMMARY:{0}", subject));

            str.AppendLine(string.Format("ORGANIZER;CN=\"{0}\";RSVP=TRUE:MAILTO:{1}", from.Address, from.Address));
            Array.ForEach(tos, toAddress =>
            {
                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", toAddress.Address, toAddress.Address));
            });
            str.AppendLine("BEGIN:VALARM");
            str.AppendLine("TRIGGER:-PT15M");
            str.AppendLine("ACTION:DISPLAY");
            str.AppendLine("DESCRIPTION:Reminder");
            str.AppendLine("END:VALARM");
            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");

            return str.ToString();


        }
        
        private static int GetTimeZoneOffsetHours(TimeZoneInfo timezone)
        {
            return 0;
            throw new NotImplementedException();
        }

    const string htmlOpenContent = @"<html xmlns:v='urn:schemas-microsoft-com:vml' 
	xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-mic
	rosoft-com:office:word' xmlns:m='http://schemas.microsoft.com/office/2004/12/omml' xmlns='http://www.w3.org/TR/REC-html40'>
    <head>
        <meta name='Generator' content = 'Microsoft Word 15 (filtered medium)' >
        <style >< !--\n/* Font Defin
	itions */\n @font-face\n	{font-family:'Cambria Math'\;\n panose-1:2 4 5 3 5
	 4 6 3 2 4\;
    }\n @font-face\n	{font-family:Calibri\;\n panose-1:2 15 5 2 2 2
	 4 3 2 4\;}\n/* Style Definitions */\np.MsoNormal\, li.MsoNormal\, div.Mso
    Normal\n	{margin:0in\;\n margin-bottom:.0001pt\;\n font-size:11.0pt\;\n fo

    nt-family:'Calibri'\,sans-serif\;}\na:link\, span.MsoHyperlink\n	{mso-styl

    e-priority:99\;\n color:#0563C1\;\n	text-decoration:underline\;}\na:visite
	d\, span.MsoHyperlinkFollowed\n	{mso-style-priority:99\;\n color:#954F72\;
	\n text-decoration:underline\;}\nspan.EmailStyle17\n	{mso-style-type:perso

    nal-compose\;\n font-family:'Calibri'\,sans-serif\;\n color:windowtext\;}\
	n.MsoChpDefault\n	{mso-style-type:export-only\;\n font-family:'Calibri'\,s

    ans-serif\;}\n @page WordSection1\n	{size:8.5in 11.0in\;\n margin:1.0in 1.0
	in 1.0in 1.0in\;}\ndiv.WordSection1\n	{page:WordSection1\;}\n-->
	</style>
	<!--[if gte mso 9]><xml>\n<o:shapedefaults v:ext='edit' spidmax='1026' />
	</xml><![endif]-->
	<!--[if gte mso 9]><xml>\n<o:shapelayout v:ext='edit'>\n<
    o:idmap v:ext='edit' data='1' />\n</o:shapelayout></xml><![endif]--></head>
    <body lang = EN - US link='#0563C1' vlink='#954F72'>
        <div class=WordSection1>
            <p class=MsoNormal>";

    const string htmlCloseContent = "</p></div></body></html>";

    }
}

﻿using Microsoft.Bot.Connector;
using Microsoft.Bot.Schema;
using System;
using System.Linq;

namespace PreludeSysDebi.Extensions
{
    public static class ActivityExtensions
    {
        public static bool IsStartActivity(this Activity activity)
        {
            switch (activity.ChannelId)
            {
                case Channels.Skype:
                    {
                        if (activity.Type == ActivityTypes.ContactRelationUpdate && activity.Action == "add")
                        {
                            return true;
                        }

                        return false;
                    }

                case Channels.Directline:
                case Channels.Emulator:
                case Channels.Webchat:
                case Channels.Msteams:
                    {
                        //if (activity.Type == ActivityTypes.ConversationUpdate)
                        //{
                        //    if (activity.MembersAdded.Any(m => m.Id == activity.Recipient.Id))
                        //    {
                        //        return true;
                        //    }
                        //    return false;
                        //}
                        if (activity.Type == ActivityTypes.Event && activity.Name == "setUserName")
                        {
                            return true;
                        }

                        return false;
                    }

                default:
                    {
                        return false;
                    }
            }
        }
    }
}

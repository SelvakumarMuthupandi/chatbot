﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ChatBotConversation
{
    public class ConversationTranscript
    {
        private readonly AzureBlobTranscriptStore _transcriptStore;



        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationHistoryBot"/> class.
        /// </summary>
        /// <param name="transcriptStore">Injected via ASP.NET dependency injection.</param>
        public ConversationTranscript(AzureBlobTranscriptStore transcriptStore)
        {
            _transcriptStore = transcriptStore ?? throw new ArgumentNullException(nameof(transcriptStore));
        }

        public async Task<ConversationData> GetConversationDetails(DateTime selectedDate, string channelId)
        {
            string continuationToken = null;
            List<string> storedTranscripts = new List<string>();
            PagedResult<TranscriptInfo> pagedResult = null;
            do
            {
                pagedResult = await _transcriptStore.ListTranscriptsAsync(channelId, pagedResult?.ContinuationToken);

                // transcript item contains ChannelId, Created, Id.
                // save the converasationIds (Id) found by "ListTranscriptsAsync" to a local list.
                foreach (var item in pagedResult.Items)
                {
                    // Make sure we store an unescaped conversationId string.
                    var strConversationId = item.Id;
                    storedTranscripts.Add(Uri.UnescapeDataString(strConversationId));
                }
                continuationToken = pagedResult.ContinuationToken;
            } while (continuationToken != null);

            //return storedTranscripts;
            ConversationData conversationDetails = new ConversationData();
            conversationDetails.Conversations = new List<ConversationData>();
            var numTranscripts = storedTranscripts.Count();
            for (int i = 0; i < numTranscripts; i++)
            {
                PagedResult<IActivity> pagedActivities = null;
                do
                {
                    string ConversationId = string.Empty;
                    ConversationData conversationData = new ConversationData();
                    string thisConversationId = storedTranscripts[i];
                    // Retrieve iActivities for this transcript.
                    pagedActivities = await _transcriptStore.GetTranscriptActivitiesAsync("webchat", thisConversationId, pagedActivities?.ContinuationToken, selectedDate);
                    if (pagedActivities == null || pagedActivities.Items.Count() == 0)
                    {
                        pagedActivities = await _transcriptStore.GetTranscriptActivitiesAsync("emulator", ConversationId, pagedActivities?.ContinuationToken);
                    }

                    foreach (var item in pagedActivities.Items)
                    {
                        // View as message and find value for key "text" :
                        if (item.Type.ToUpper() == "MESSAGE")
                        {
                            var thisMessage = item.AsMessageActivity();
                            if (ConversationId != thisMessage.Conversation.Id)
                            {
                                conversationDetails.Conversations.Add(conversationData);
                                conversationData.Id = thisMessage.Conversation.Id;
                                conversationData.ChannelId = thisMessage.ChannelId;
                                conversationData.Text = thisMessage.Text;
                                conversationData.TimeStamp = thisMessage.Timestamp.Value.Date;
                                ConversationId = thisMessage.Conversation.Id;
                            }
                            else if (ConversationId == thisMessage.Conversation.Id)
                            {
                                conversationData.Text += @"<br />" + thisMessage.Text;
                            }
                        }
                    }

                } while (pagedActivities.ContinuationToken != null);
            }
            return conversationDetails;
        }

        public async Task<ConversationData> GetConversationDetailsById(string ConversationId)
        {
            ConversationData conversationDetails = new ConversationData();
            conversationDetails.Conversations = new List<ConversationData>();
            ConversationData conversationData = new ConversationData();
            string continuationToken = null;
            string currentConversationId = string.Empty;
            bool parentDataDone = false;
            PagedResult<IActivity> pagedActivities = null;
            string conversationIdChanged = "N";
            string chanell = "webchat";
            bool firstIteration = true;
            pagedActivities = await _transcriptStore.GetTranscriptActivitiesAsync(chanell, ConversationId, pagedActivities?.ContinuationToken);
            if (pagedActivities == null || pagedActivities.Items.Count() == 0)
            {
                chanell = "emulator";
                pagedActivities = await _transcriptStore.GetTranscriptActivitiesAsync(chanell, ConversationId, pagedActivities?.ContinuationToken);
            }
            do
            {
                if (!firstIteration)
                {
                    pagedActivities = await _transcriptStore.GetTranscriptActivitiesAsync(chanell, ConversationId, pagedActivities?.ContinuationToken);
                }
                foreach (var item in pagedActivities.Items)
                {
                    if (item.Type.ToUpper() == "MESSAGE")
                    {
                        var thisMessage = item.AsMessageActivity();
                        var userName = string.Empty;
                        if (thisMessage.From.Name == "PreludeSysChatBotDev")
                        { userName = "Bot"; }
                        else
                        { userName = "User"; }
                        if (currentConversationId != thisMessage.Conversation.Id)
                        {
                            if (parentDataDone)
                            {
                                conversationIdChanged = "Y";
                            }
                            parentDataDone = !parentDataDone;
                            currentConversationId = thisMessage.Conversation.Id;
                            conversationDetails.Conversations.Add(conversationData);
                            conversationData.Id = thisMessage.Conversation.Id;
                            conversationData.ChannelId = thisMessage.ChannelId;
                            conversationData.Text = userName + ":" + thisMessage.Text;
                            conversationData.TimeStamp = thisMessage.Timestamp.Value.Date;
                            ConversationId = thisMessage.Conversation.Id;
                        }
                        else if (ConversationId == thisMessage.Conversation.Id)
                        {
                            if (!string.IsNullOrWhiteSpace(thisMessage.Text))
                                conversationData.Text += Environment.NewLine + userName + ":" + thisMessage.Text;
                        }
                        else
                        {
                            conversationIdChanged = "Y";
                        }
                    }
                }

                if (conversationIdChanged == "N")
                {
                    continuationToken = pagedActivities.ContinuationToken;
                }
                firstIteration = false;

            } while (continuationToken != null);

            return conversationDetails;
        }
    }
}
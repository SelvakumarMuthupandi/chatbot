﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSysDebi.BlobStorageUtils
{
    public class Utility
    {

        public static ConversationData GetConversationData(PagedResult<IActivity> pagedResult)
        {

            var conversationDetails = new ConversationData();
            conversationDetails.Conversations = new List<ConversationData>();
            string ConversationId = string.Empty;
            ConversationData _conversationData = new ConversationData();
            foreach (var item in pagedResult.Items)
            {
                // View as message and find value for key "text" :
                if (item.Type.ToUpper() == "MESSAGE")
                {
                    var thisMessage = item.AsMessageActivity();
                    if (ConversationId != thisMessage.Conversation.Id)
                    {
                        _conversationData = new ConversationData();
                        conversationDetails.Conversations.Add(_conversationData);
                        _conversationData.Id = thisMessage.Conversation.Id;
                        _conversationData.ChannelId = thisMessage.ChannelId;
                        _conversationData.Text = thisMessage.Text;
                        _conversationData.TimeStamp = thisMessage.Timestamp.Value.Date;
                        ConversationId = thisMessage.Conversation.Id;
                    }
                    else if (ConversationId == thisMessage.Conversation.Id)
                    {
                        _conversationData.Text += @"<br />" + thisMessage?.Text ?? "";
                    }
                }
            }
            return conversationDetails;
        }

    }
}

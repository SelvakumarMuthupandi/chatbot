﻿using System;
using System.Collections.Generic;

public class ConversationData
{
    public string ChannelId { get; set; }

    // [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
    public DateTime? TimeStamp { get; set; }

    public string Id { get; set; }

    public string Text { get; set; }

    public List<ConversationData> Conversations { get; set; }

}
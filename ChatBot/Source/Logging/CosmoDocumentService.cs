﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSysDebi.Logging
{
    public class CosmoDocumentService
    {
        

        private static DocumentClient _client;
        private static string _databaseId ;
        private static string _collectionId;


        Uri collectionLink;
        public CosmoDocumentService(DocumentClient client, string databaseId, string collectionId)
        {
            _client = client;
            _databaseId = databaseId;
            _collectionId = collectionId;
             collectionLink =  UriFactory.CreateDocumentCollectionUri(databaseId, collectionId);
        }

        public List<Document> ReadTopFive()
        {
            //List<PreludeLogItem> result;
            List<string> stringResultList = new List<string>();
            var query = _client.CreateDocumentQuery<Document>(collectionLink, new SqlQuerySpec()
            {
                QueryText = "SELECT top 5 * FROM c where c.MsgType = 'Info'",
            }, new FeedOptions {EnableCrossPartitionQuery =true});

            var res = query.ToList();
            res.ForEach(r => stringResultList.Add(r.ToString()));
            return res;
        }


        public async Task InsertDocumentsAsync(List<object> items)
        {
            foreach (var item in items)
            {
                Document created = await _client.CreateDocumentAsync(collectionLink, item);
            }
            
        }

        public Task InsertDocumentAsync(object item)
        {
             _client.CreateDocumentAsync(collectionLink, item);
            return Task.FromResult(0);
        }

    }
}

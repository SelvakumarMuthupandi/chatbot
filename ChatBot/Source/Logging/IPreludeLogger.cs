﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSysDebi.Logging
{
    public interface IPreludeLogger
    {
        void Error(string message);
        void Warn(string message);
        void Info(string message);
        void Fatal(string message);
        void Trace(string message);
    }
}

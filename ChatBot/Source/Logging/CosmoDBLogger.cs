﻿using Microsoft.Bot.Builder.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi.Logging
{
    public class CosmoDBLogger : IPreludeLogger
    {

        private static CosmosDbStorage _loggingStorage;
        private static CosmoDocumentService _docService;
        CancellationToken cToken;
        Dictionary<string, object> data = new Dictionary<string, object>();

        public CosmoDBLogger(CosmosDbStorage loggingStorage, CosmoDocumentService docService)
        {
            _loggingStorage = loggingStorage;
            _docService = docService;
        }

        public void Error(string message)
        {
            LogMessage(LogLevels.Error.ToString(), message);
        }

        public void Warn(string message)
        {
            LogMessage(LogLevels.Warn.ToString(), message);
        }

        public void Info(string message)
        {
            LogMessage(LogLevels.Info.ToString(), message);
        }

        public void Fatal(string message)
        {
            LogMessage(LogLevels.Fatal.ToString(), message);
        }

        public void Trace(string message)
        {
            LogMessage(LogLevels.Trace.ToString(), message);
        }

        private Task LogMessage(string level, string message)
        {
            level = level ?? string.Empty;
            data.Clear();
            string id = Guid.NewGuid().ToString();
            _loggingStorage.WriteAsync(data, cToken);
            _docService.InsertDocumentAsync(new PreludeLogItem { Id = id, MsgType = level, Message = message, LogggedDate = DateTime.Now, User = string.Empty });
            return Task.FromResult(0);

        }
    }
}

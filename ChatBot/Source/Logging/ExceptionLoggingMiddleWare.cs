﻿using Microsoft.Bot.Builder;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi.Logging
{
    public class ExceptionLoggingMiddleWare : IMiddleware
    {

        private IPreludeLogger _logger;

        public ExceptionLoggingMiddleWare(IPreludeLogger logger)
        {
            _logger = logger;
        }
        public async Task OnTurnAsync(ITurnContext context, NextDelegate next, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                await next(cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());

            }

        }
    }
}

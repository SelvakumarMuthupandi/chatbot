﻿
using Microsoft.Bot.Builder;
using System;
using System.Collections.Generic;


namespace PreludeSysDebi.Logging
{
    public class PreludeLogItem 
    {
        public string Id { get; set; }

        public string MsgType { get; set; }
        public string Message { get; set; }

        public string User { get; set; }

        public DateTime LogggedDate { get; set; }

    }
}

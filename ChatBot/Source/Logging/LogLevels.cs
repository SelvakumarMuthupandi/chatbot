﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSysDebi.Logging
{
    public enum LogLevels
    {
        Error = 1,
        Warn =  2,
        Info =  3,
        Fatal = 4,
        Trace = 5
    }
}

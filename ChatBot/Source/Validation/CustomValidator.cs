﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PreludeSysDebi.Validation
{
    public static class CustomValidator
    {

        public static bool IsEmailValid(string emailAddress)
        {
           
                if (string.IsNullOrWhiteSpace(emailAddress))
                    return false;

                try
                {
                // Normalize the domain
                emailAddress = Regex.Replace(emailAddress, @"(@)(.+)$", DomainMapper,
                                          RegexOptions.None, TimeSpan.FromMilliseconds(200));

                    // Examines the domain part of the email and normalizes it.
                    string DomainMapper(Match match)
                    {
                        // Use IdnMapping class to convert Unicode domain names.
                        var idn = new IdnMapping();

                        // Pull out and process domain name (throws ArgumentException on invalid)
                        var domainName = idn.GetAscii(match.Groups[2].Value);

                        return match.Groups[1].Value + domainName;
                    }
                }
                catch (RegexMatchTimeoutException )
                {
                    return false;
                }
                catch (ArgumentException )
                {
                    return false;
                }

                try
                {
                    return Regex.IsMatch(emailAddress,
                        @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                        RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                }
                catch (RegexMatchTimeoutException)
                {
                    return false;
                }
            
        }

        public static bool IsPhoneNumberValid(string phoneNo)
        {
            if (string.IsNullOrEmpty(phoneNo)) return false;
            // return Regex.IsMatch(phoneNo, "(1-)?\\p{N}{3}-\\p{N}{3}-\\p{N}{4}\\b"); // for iphenated format
            return Regex.IsMatch(phoneNo, @"^\d{10}$");
            
        }

        public static bool IsNameValid(string name)
        {
            if (string.IsNullOrEmpty(name)) return false;
            var nameToTest = name.Trim();
            if (!Regex.IsMatch(nameToTest, @"^[\p{L}\p{M}' \.\-]+$"))
                return false;

            return true;
           
        }
    }
}

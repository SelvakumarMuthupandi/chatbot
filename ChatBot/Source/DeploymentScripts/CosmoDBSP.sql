// SAMPLE STORED PROCEDURE
function sample(datestr, endDateStr, Name, Email, Phone) {
    var collection = getContext().getCollection();
    Name = Name || "";
    Email = Email || "";
    Phone = Phone || "";
 var tokens;
 var testresult = [];
    // Query documents and take 1st item.
    var isAccepted = collection.queryDocuments(
        collection.getSelfLink(),
        'SELECT * FROM root r order by r._ts desc',
        function(err, feed, options) {
            if (err) throw err;

            // Check the feed and if empty, set the body to 'no docs found', 
            // else take 1st element from feed
            if (!feed || !feed.length) {
                var response = getContext().getResponse();
                response.setBody('no docs found');
            } else {
                var response = getContext().getResponse();
                //var body = { prefix: prefix, feed: feed[0] };
                var result = [];
                var curDate = new Date();
                var currDateStr = curDate.getMonth() + "/" + curDate.getDate() + "/" + curDate.getFullYear();

                if (!datestr) {
                    datestr = currDateStr;
                }
                if (!endDateStr) {
                    endDateStr = currDateStr;
                }
                tokens = datestr.split('/'),
                    mm = tokens[0],
                    dd = tokens[1];

                if (mm.charAt(0) === '0') tokens[0] = mm.replace("0", "");
                if (dd.charAt(0) === '0') tokens[1] = dd.replace("0", "");

                var startDate = new Date(tokens[2], mm - 1, dd);
                tokens = endDateStr.split("/");
                var endDate = new Date(tokens[2], tokens[0] - 1, tokens[1]);

                for (i = 0; i < feed.length; i++) {
                    if (feed[i]['document'] && feed[i]['document']['OnboardingState']) {

                        var dbDateStr = feed[i]['document']['OnboardingState'].Date
                        var dbDate;
                        if (dbDateStr) {
                            tokens = dbDateStr.split("/");
                            dbDate = new Date(tokens[2], tokens[0] - 1, tokens[1]);
                        } else {
                            var dt = new Date();
                            dbDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
                        }

                        var dbname = (feed[i]['document']['OnboardingState'].Name || "").toLowerCase();
                        var dbemail = (feed[i]['document']['OnboardingState'].Email || "").toLowerCase();
                        var dbphone = (feed[i]['document']['OnboardingState'].PhoneNumber || "").toLowerCase();
                       
                         if ( (dbDate >= startDate && dbDate <= endDate) && (!Name ||  dbname.indexOf(Name.toLowerCase()) !== -1  )
                                && (!Email || dbemail == Email )
                                && (!Phone ||  dbphone.indexOf(Phone.toLowerCase()) !== -1)
                           ) 
						{                            
                            result.push({
                                "Name": feed[i]['document']['OnboardingState'].Name || "",
                                "Email": feed[i]['document']['OnboardingState'].Email || "",
                                "Phone": feed[i]['document']['OnboardingState'].PhoneNumber || "",
                                "SendTrascriptEmail": feed[i]['document']['OnboardingState'].SendTranscriptEmail || "",
                                "ConversationId": feed[i]['document']['OnboardingState'].ConversationId || "",
                                "Date": feed[i]['document']['OnboardingState'].Date || "",
                                "Time": feed[i]['document']['OnboardingState'].Date || "",
                                "ChannelId": feed[i]['document']['OnboardingState'].ChannelId || ""
                               
                            })
                        }
                    }
                }
                var resp = JSON.stringify({"data":result});
                response.setBody(resp);
            }
        });

    if (!isAccepted) throw new Error('The query was not accepted by the server.');
}
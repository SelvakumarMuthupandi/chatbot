﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace PreludeSysDebi.ConversationHistory
{
    public class MessageLogEntity : TableEntity
    {
        public string Body
        {
            get;
            set;
        }
    }
}

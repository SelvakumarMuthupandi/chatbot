﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Luis;
using Microsoft.ApplicationInsights;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PreludeSysDebi.DataService;
using PreludeSysDebi.Dialogs.Escalate;
using PreludeSysDebi.Dialogs.Onboarding;
using PreludeSysDebi.Dialogs.ScheduleMeeting;
using PreludeSysDebi.Dialogs.Shared;
using PreludeSysDebi.Logging;
using PreludeSysDebi.SMTPEmailService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi.Dialogs.Main
{
    public class MainDialog : RouterDialog
    {
        private BotServices _services;
        private UserState _userState;
        private IStatePropertyAccessor<ScheduleMeetingState> _meetingAccessor;
        private ConversationState _conversationState;
        private MainResponses _responder = new MainResponses();
        private IPreludeLogger _logger;
        private AzureSQLDBService _sqlService;
        private CosmoDocumentService _cosmosDBService;
        private EmailService _emailService;
        private IConfiguration _config;
        private IStatePropertyAccessor<OnboardingState> _onboardingStateAccessor;
        private AzureBlobTranscriptStore _transcriptStore = null;

        public MainDialog(BotServices services, ConversationState conversationState, UserState userState, IStatePropertyAccessor<OnboardingState> accessor, IStatePropertyAccessor<ScheduleMeetingState> meetingAccessor, IBotTelemetryClient telemetryClient,
          IPreludeLogger logger, AzureBlobTranscriptStore transcriptStore, AzureSQLDBService sqlService, CosmoDocumentService cosmoService, SMTPEmailService.EmailService emailService, IConfiguration configuration)
           : base(nameof(MainDialog))
        {
            _services = services ?? throw new ArgumentNullException(nameof(services));
            _conversationState = conversationState;
            _userState = userState;
            _meetingAccessor = meetingAccessor;
            _logger = logger;
            _transcriptStore = transcriptStore;
            _sqlService = sqlService;
            _cosmosDBService = cosmoService;
            _emailService = emailService;
            TelemetryClient = telemetryClient;
            _config = configuration;

            _onboardingStateAccessor = accessor;
            AddDialog(new OnboardingDialog(_services, _userState.CreateProperty<OnboardingState>(nameof(OnboardingState)), telemetryClient, _logger, _sqlService));
            AddDialog(new ScheduleMeetingDialog(_services, _userState.CreateProperty<ScheduleMeetingState>(nameof(ScheduleMeetingState)), _userState.CreateProperty<OnboardingState>(nameof(OnboardingState)), telemetryClient, _logger));
            AddDialog(new EscalateDialog(_services));
        }

        protected override async Task OnStartAsync(DialogContext dc, CancellationToken cancellationToken = default(CancellationToken))
        {
            string userEmail = string.Empty;
            if (dc.Context.Activity.Value != null)
            {
                if (dc.Context.Activity.Type == ActivityTypes.Event && dc.Context.Activity.Name == "setUserName")
                {
                    userEmail = dc.Context.Activity.Value.ToString();
                }
            }
            var view = new MainResponses();

            if (string.IsNullOrWhiteSpace(userEmail))
            {
                if (dc.Context.Activity.Type == ActivityTypes.Event && dc.Context.Activity.Name == "setUserName")
                {
                    await view.ReplyWith(dc.Context, MainResponses.ResponseIds.Intro);
                    await dc.BeginDialogAsync(nameof(OnboardingDialog), cancellationToken: cancellationToken);
                }
            }
            else
            {
                DataService.CosmosDBService _cosmosService = new DataService.CosmosDBService(_config);
                var result = await _cosmosService.GetDocumentAsync(userEmail);
                OnboardingState _state = JsonConvert.DeserializeObject<OnboardingState>(result); // new OnboardingState();
                if (_state.Name == null || _state.Name.Trim() == string.Empty)
                {
                    await dc.BeginDialogAsync(nameof(OnboardingDialog), cancellationToken: cancellationToken);
                    return;
                };
                _state.ReVisiting = true;
                _state.ChannelId = dc.Context.Activity.ChannelId;
                _state.ConversationId = dc.Context.Activity.Conversation.Id;
                _state.Date = DateTime.UtcNow.ToShortDateString();
                _state.Time = DateTime.UtcNow.ToShortTimeString();
                await _onboardingStateAccessor.SetAsync(dc.Context, _state);
                ScheduleMeetingState meetingSchedule = null;
                var scheduleString = await _cosmosService.GetMeethingSheduleDocumentAsync(userEmail);
                if (!string.IsNullOrWhiteSpace(scheduleString))
                {
                    meetingSchedule = JsonConvert.DeserializeObject<ScheduleMeetingState>(scheduleString);
                }
                if (meetingSchedule != null && meetingSchedule.Time != null && meetingSchedule.Time.Length > 4)
                {
                    IMessageActivity replyAction = Activity.CreateMessageActivity();
                    await dc.EndDialogAsync();
                    var reply = dc.Context.Activity.CreateReply($"Hi {_state.Name}, \n \n Welcome back to PreludeSys!");
                    reply.Attachments = new List<Microsoft.Bot.Schema.Attachment>()
                    {
                            new HeroCard() { Title = "Meeting Remainder", Text = $"Your meeting with our representative is scheduled on {DateTime.Parse(meetingSchedule.MeetingDate).ToString("MM/dd/yyyy")  } at {meetingSchedule.Time}." }.ToAttachment()
                    };
                    await dc.Context.SendActivityAsync(reply, cancellationToken);
                    var emailActivity = dc.Context.Activity.CreateReply(userEmail);
                    emailActivity.Name = "getUserName";
                    emailActivity.Value = userEmail;
                    await dc.Context.SendActivityAsync(emailActivity, cancellationToken);
                    var conversationActivity = dc.Context.Activity.CreateReply(dc.Context.Activity.Conversation.Id);
                    conversationActivity.Name = "getConversationId";
                    conversationActivity.Value = dc.Context.Activity.Conversation.Id;
                    await dc.Context.SendActivityAsync(conversationActivity, cancellationToken);

                }
                else
                {
                    await dc.EndDialogAsync(); 
                    var reply = dc.Context.Activity.CreateReply($"Hi {_state.Name}, Welcome back to PreludeSys! \n \n  How can I help you today.");
                    await dc.Context.SendActivityAsync(reply, cancellationToken);
                    var emailActivity = dc.Context.Activity.CreateReply(userEmail);
                    emailActivity.Name = "getUserName";
                    emailActivity.Value = userEmail;
                    await dc.Context.SendActivityAsync(emailActivity, cancellationToken);
                    var conversationActivity = dc.Context.Activity.CreateReply(dc.Context.Activity.Conversation.Id);
                    conversationActivity.Name = "getConversationId";
                    conversationActivity.Value = dc.Context.Activity.Conversation.Id;
                    await dc.Context.SendActivityAsync(conversationActivity, cancellationToken);
                }

            }
        }
        protected override async Task RouteAsync(DialogContext dc, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Check dispatch result
            var dispatchResult = await _services.DispatchRecognizer.RecognizeAsync<Dispatch>(dc, true, CancellationToken.None);
            var intent = dispatchResult.TopIntent().intent;
            if (intent == Dispatch.Intent.l_general)
            {
                // If dispatch result is general luis model
                _services.LuisServices.TryGetValue("general", out var luisService);
                if (luisService == null)
                {
                    throw new Exception("The specified LUIS Model could not be found in your Bot Services configuration.");
                }
                else
                {
                    var result = await luisService.RecognizeAsync<General>(dc, true, CancellationToken.None);
                    var generalIntent = result?.TopIntent().intent;
                    // switch on general intents
                    switch (generalIntent)
                    {
                        case General.Intent.Cancel:
                            {
                                // send cancelled response
                                await _responder.ReplyWith(dc.Context, MainResponses.ResponseIds.Cancelled);

                                // Cancel any active dialogs on the stack
                                await dc.CancelAllDialogsAsync();
                                break;
                            }
                        case General.Intent.Escalate:
                            {
                                // start escalate dialog
                                await dc.BeginDialogAsync(nameof(EscalateDialog));
                                break;
                            }
                        case General.Intent.Help:
                            {
                                // send help response
                                await _responder.ReplyWith(dc.Context, MainResponses.ResponseIds.Help);
                                break;
                            }
                        case General.Intent.None:
                        default:
                            {
                                // No intent was identified, send confused message
                                await SendUnknownResponse(dc, cancellationToken);
                                break;
                            }
                    }
                }
            }
            else if (intent == Dispatch.Intent.q_faq)
            {
                _services.QnAServices.TryGetValue("faq", out var qnaService);

                if (qnaService == null)
                {
                    throw new Exception("The specified QnA Maker Service could not be found in your Bot Services configuration.");
                }
                else
                {
                    var answers = await qnaService.GetAnswersAsync(dc.Context);

                    if (answers != null && answers.Count() > 0)
                    {
                        await dc.Context.SendActivityAsync(answers[0].Answer);
                    }
                    else
                    {
                        await SendUnknownResponse(dc, cancellationToken);
                    }
                }
            }
            else if (intent == Dispatch.Intent.I_ThankYou)
            {
                await SendSuggestedActionsAsync(dc.Context, cancellationToken);
            }
            else if (intent == Dispatch.Intent.I_ScheduleMeeting)
            {
                await dc.BeginDialogAsync(nameof(ScheduleMeetingDialog));
            }
            else
            {
                if (!dc.Context.Activity.Text.Contains("command"))
                {
                    await SendUnknownResponse(dc, cancellationToken);
                }
            }
        }


        private async Task SendUnknownResponse(DialogContext dc, CancellationToken cancellationToken)
        {
            // If dispatch intent does not map to configured models, send "confused" response.
            await Task.Run(() =>
           {
               _services.QnAServices.TryGetValue("faq", out var qnaService);
               dc.Context.Activity.Text = MainResponses.ResponseIds.Confused;
           });
        }

        private async Task SendSuggestedActionsAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            _services.QnAServices.TryGetValue("faq", out var qnaService);
            var answers = await qnaService.GetAnswersAsync(turnContext);
            Activity reply = turnContext.Activity.CreateReply("Email Transcript");
            if (answers != null && answers.Count() > 0)
            {
                reply = turnContext.Activity.CreateReply(answers[0].Answer);
            }

            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction() { Title = "E-mail Transcript", Type = ActionTypes.ImBack, Value = "command SendTranscriptMail" },
                },
            };
            await turnContext.SendActivityAsync(reply, cancellationToken);
        }

        protected override async Task OnEventAsync(DialogContext dc, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Check if there was an action submitted from intro card
            if (dc.Context.Activity.Value != null)
            {
                dynamic value = dc.Context.Activity.Value;
                if (value.action == "startOnboarding")
                {
                    await dc.BeginDialogAsync(nameof(OnboardingDialog));
                    return;
                }
                else if (value.action == "processUserInfo")
                {
                    await ProcessUserInfo(dc, value, cancellationToken);
                }
                else if (value.action == "scheduleMeeting")
                {
                    dynamic data = JObject.Parse(Convert.ToString(dc.Context.Activity.Value));
                    ScheduleMeetingState meetingState = new ScheduleMeetingState()
                    {
                        MeetingDate = data.meetingDate,
                        TimeZone = data.TimezoneSelectVal,
                        Time = data.TimeSlotSelectVal
                    };
                    dynamic userData = dc.Context.Activity.Value;
                    string userName = userData.UserName;
                    if (!string.IsNullOrWhiteSpace(userName))
                    {
                        var userInfo = new OnboardingState()
                        {
                            Name = userData.UserName,
                            Email = userData.UserEmail,
                            PhoneNumber = userData.UserPhoneNumber,
                            ClientUserName = userData.UserEmail
                        };
                        var validationResult = await ValidateUserInfo(dc, userInfo, cancellationToken);
                        if (validationResult)
                        {
                            userInfo.ChannelId = dc.Context.Activity.ChannelId;
                            userInfo.ConversationId = dc.Context.Activity.Conversation.Id;
                            userInfo.Date = DateTime.UtcNow.ToShortDateString();
                            userInfo.Time = DateTime.UtcNow.ToShortTimeString();
                            await _onboardingStateAccessor.SetAsync(dc.Context, userInfo);
                        }
                    }
                    else
                    {
                        var _state = await _onboardingStateAccessor.GetAsync(dc.Context, () => new OnboardingState());
                        if (string.IsNullOrWhiteSpace(_state.Name))
                        {
                            var validationResult = await ValidateUserInfo(dc, _state, cancellationToken);
                            if (!validationResult) return;
                        }
                    }
                    await ScheduleMeeting(dc, meetingState, cancellationToken);
                }

            }
        }

        private async Task ProcessUserInfo(DialogContext dc, dynamic value, CancellationToken cancellationToken)
        {
            var userInfo = new OnboardingState()
            {
                Name = value.UserName,
                Email = value.UserEmail,
                PhoneNumber = value.UserPhoneNumber,
                ClientUserName = value.UserEmail
            };
            var validationResult = await ValidateUserInfo(dc, userInfo, cancellationToken);
            if (validationResult)
            {
                userInfo.ChannelId = dc.Context.Activity.ChannelId;
                userInfo.ConversationId = dc.Context.Activity.Conversation.Id;
                userInfo.Date = DateTime.UtcNow.ToShortDateString();
                userInfo.Time = DateTime.UtcNow.ToShortTimeString();
                await _onboardingStateAccessor.SetAsync(dc.Context, userInfo);
                await dc.EndDialogAsync();
                var reply = dc.Context.Activity.CreateReply($"Thanks for the information !  How can I help you?");
                await dc.Context.SendActivityAsync(reply, cancellationToken);
                var emailActivity = dc.Context.Activity.CreateReply(userInfo.Email);
                emailActivity.Name = "getUserName";
                emailActivity.Value = userInfo.Email;
                await dc.Context.SendActivityAsync(emailActivity, cancellationToken);
                var conversationActivity = dc.Context.Activity.CreateReply(dc.Context.Activity.Conversation.Id);
                conversationActivity.Name = "getConversationId";
                conversationActivity.Value = dc.Context.Activity.Conversation.Id;
                await dc.Context.SendActivityAsync(conversationActivity, cancellationToken);

            }
        }

        private async Task ScheduleMeeting(DialogContext dc, ScheduleMeetingState meetingState, CancellationToken cancellationToken)
        {
            await _meetingAccessor.SetAsync(dc.Context, meetingState);
            if (!string.IsNullOrEmpty(meetingState.MeetingDate) && !string.IsNullOrEmpty(meetingState.TimeZone) && !string.IsNullOrEmpty(meetingState.Time))
            {
                string[] startDate = meetingState.MeetingDate.Split(new string[] { @"/", @"-" }, StringSplitOptions.None);
                string[] timings = meetingState.Time.Split(new string[] { @" AM - ", @" PM - ", @" PM", @" AM" }, StringSplitOptions.None);
                if (startDate.Length == 3 && timings.Length == 3)
                {
                    var startTime = timings[0].Split(@":");
                    string startTimeAMorPM = string.Empty;
                    string endTimeAMorPM = string.Empty;
                    if (meetingState.Time.Contains(EndPMMarker))
                    {
                        endTimeAMorPM = "PM";
                    }
                    if (meetingState.Time.Contains(StartPMMarker))
                    {
                        startTimeAMorPM = "PM";
                    }
                    var endTime = timings[1].Split(@":");
                    DateTime meetingStartDate = new DateTime(Convert.ToInt32(startDate[0]), Convert.ToInt32(startDate[1]), Convert.ToInt32(startDate[2]), Convert.ToInt32(startTime[0]) + Convert.ToInt32((startTimeAMorPM == "PM" && startTime[0] != "12" ? 12 : 0)), Convert.ToInt32(startTime[1]), 0);
                    if (meetingStartDate.Date > DateTime.Today)
                    {
                        string adminEmail = _config.GetSection("AdminEmail").Value;
                        string orgainiserEmail = _config.GetSection("EmailFrom").Value;
                        ConversationData conversationDetails = await GetConversationData(dc.Context.Activity);
                        var onbState = await _onboardingStateAccessor.GetAsync(dc.Context, () => new OnboardingState());
                        var pstTimeInfor = TimeZoneInfo.FindSystemTimeZoneById(meetingState.TimeZone);
                        var istTimeInfor = TimeZoneInfo.Local;
                        var diff = istTimeInfor.GetUtcOffset(DateTime.Now).Ticks - pstTimeInfor.GetUtcOffset(DateTime.Now).Ticks;
                        var diffTimeSpan = new TimeSpan(diff);
                        meetingStartDate = TimeZoneInfo.ConvertTime(meetingStartDate.Add(diffTimeSpan), pstTimeInfor);
                        DateTime meetingEndDate = meetingStartDate.AddMinutes(15);
                        var reply = dc.Context.Activity.CreateReply(string.Format(Resources.MainStrings.SCHEDULE_MEETING_MSG, meetingStartDate.ToString("MM/dd/yyyy"), meetingStartDate.ToShortTimeString(), meetingEndDate.ToShortTimeString(), meetingState.TimeZone));
                        var formattedDateTime = DateTime.Parse( meetingState.MeetingDate).ToString("MM/dd/yyyy");
                        string meetingInfo = $"Hi {onbState.Name}, {Environment.NewLine}  {Environment.NewLine} Thank you for reaching out to us. {Environment.NewLine} {Environment.NewLine} Based on the live-chat discussion, we have scheduled a meeting for you with our agent on { formattedDateTime }. Time { meetingState.Time}  {meetingState.TimeZone}. {Environment.NewLine} Thank you, {Environment.NewLine} {Environment.NewLine} Team PreludeSys ";

                        string salesRepMessage = $"Hi {onbState.Name}, {Environment.NewLine} {Environment.NewLine} We recently had a prospect reaching out to us via DemandBlue Live Chat. {Environment.NewLine} {Environment.NewLine} Based on the live-chat discussion, we have scheduled a meeting for you with the prospect on {formattedDateTime}. Time { meetingState.Time} {meetingState.TimeZone}. {Environment.NewLine} {Environment.NewLine} Please find chat transcript attached. Thank you, {Environment.NewLine} {Environment.NewLine} Team PreludeSys";
                        var conversationText = (conversationDetails.Conversations != null && conversationDetails.Conversations.Count > 0) ? conversationDetails.Conversations[0].Text : string.Empty;
                        string currentDirectory = Environment.CurrentDirectory;
                        string folderPath = $"{currentDirectory}/TemporaryFolder";
                        string pathToFile = $"{folderPath}/{Guid.NewGuid().ToString()}_ConversationText.txt";
                        System.IO.Directory.CreateDirectory(folderPath);
                        System.IO.File.WriteAllText(pathToFile, conversationDetails.Conversations[0].Text);
                        var attachment = new System.Net.Mail.Attachment(pathToFile, "text/plain");
                        MailAddress adminEmailAddress = new MailAddress(adminEmail, Resources.MainStrings.MEETING_ADMIN);
                        SendMeetingInvitesForScheduleMeeting(new MailAddress(orgainiserEmail, Resources.MainStrings.MEETING_ORGANIZER), new MailAddress[] { adminEmailAddress, new MailAddress(onbState.Email, onbState.Name) }, Resources.MainStrings.MEETING_SUBJECT, true,
                            meetingInfo, attachment, "Through phone call", meetingStartDate,
                            meetingEndDate, pstTimeInfor, 1001, false, true, adminEmailAddress, salesRepMessage);
                        await dc.EndDialogAsync();
                        await dc.Context.SendActivityAsync(reply, cancellationToken);
                    }
                    else
                    {
                        var reply = dc.Context.Activity.CreateReply("Please select a date greater than today.");
                        await dc.Context.SendActivityAsync(reply, cancellationToken);
                    }
                }
                else
                {
                    var reply = dc.Context.Activity.CreateReply("Please select a date greater than today.");
                    await dc.Context.SendActivityAsync(reply, cancellationToken);
                }
            }
            else
            {
                var reply = dc.Context.Activity.CreateReply("Please select a date greater than today.");
                await dc.Context.SendActivityAsync(reply, cancellationToken);
            }
        }

        public void SendMeetingInvitesForScheduleMeeting(MailAddress emailFrom, MailAddress[] emailTos, string subject,
                  bool isMeeting, string bodyContent, System.Net.Mail.Attachment attachedContent, string location, DateTime? start, DateTime? end,
                  TimeZoneInfo timezone, int eventID, bool isCancel, bool htmlEnable, MailAddress adminEmail, string salesRepMessage)
        {
            EmailService _emailSerive = new EmailService(_logger, _config);
            _emailSerive.SendMeetingInvites(emailFrom, emailTos, subject,
            isMeeting, bodyContent, attachedContent, location, start, end,
            timezone, eventID, isCancel, htmlEnable, adminEmail, salesRepMessage);
        }

        private async Task<bool> ValidateUserInfo(DialogContext dc, OnboardingState userInfo, CancellationToken cancellationToken)
        {
            var isNameValid = Validation.CustomValidator.IsNameValid(userInfo.Name);
            var isEmailValid = Validation.CustomValidator.IsEmailValid(userInfo.Email);
            var isPhoneValid = Validation.CustomValidator.IsPhoneNumberValid(userInfo.PhoneNumber);
            if (!isNameValid || !isEmailValid || !isPhoneValid)
            {
                string fieldsRequired = string.Concat(!isNameValid ? "Name , " : "", !isEmailValid ? "Email, " : "", !isPhoneValid ? "Phone " : "");
                fieldsRequired = fieldsRequired.TrimEnd(',', ' ');
                var reply = dc.Context.Activity.CreateReply($"Please fill \"{fieldsRequired}\" field to continue.");
                await dc.Context.SendActivityAsync(reply, cancellationToken);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected override async Task CompleteAsync(DialogContext dc, CancellationToken cancellationToken = default(CancellationToken))
        {
            // The active dialog's stack ended with a complete status
            await _responder.ReplyWith(dc.Context, MainResponses.ResponseIds.Completed);
        }

        private static Microsoft.Bot.Schema.Attachment CreateAdaptiveCardAttachment(string filePath)
        {
            var adaptiveCardJson = File.ReadAllText(filePath);
            var adaptiveCardAttachment = new Microsoft.Bot.Schema.Attachment()
            {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = JsonConvert.DeserializeObject(adaptiveCardJson),
            };
            return adaptiveCardAttachment;
        }

        private async Task<ConversationData> GetConversationData(IActivity activity)
        {
            ChatBotConversation.ConversationTranscript _store = new ChatBotConversation.ConversationTranscript(_transcriptStore);
            ConversationData conversationDetails = await _store.GetConversationDetailsById(activity.Conversation.Id);
            return conversationDetails;
        }

        public const string StartPMMarker = @" PM - ";
        public const string EndPMMarker = @" PM";

    }
}

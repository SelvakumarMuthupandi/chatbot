﻿using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using PreludeSysDebi.Dialogs.Onboarding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreludeSysDebi.Dialogs
{
    public class Utility
    {
        public static string GetUserDetailsForEmail(OnboardingState onBoardingState)
        {
            string result = string.Empty;
            if (onBoardingState == null)
                return result;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(@"<br/>Hi Admin");
            builder.AppendLine(@"<br/>A chatbot user would like to schedule a meeting with you. The details are as under:-");
            builder.AppendLine(string.Format(@"<br/>UserName :{0}", onBoardingState.Name));
            builder.AppendLine(string.Format(@"<br/>Email :{0}", onBoardingState.Email));
            builder.AppendLine(string.Format(@"<br/>PhoneNumber :{0}", onBoardingState.PhoneNumber));
            builder.AppendLine(@"<br/>The transcript of conversation with the client is provided below:-");
            result = builder.ToString();
            return result;
        }
        public static Attachment CreateAdaptiveCardAttachment(string filePath)
        {

            var adaptiveCardJson = File.ReadAllText(filePath);
            var adaptiveCardAttachment = new Attachment()
            {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = JsonConvert.DeserializeObject(adaptiveCardJson),

            };
            return adaptiveCardAttachment;
        }
    }
}

﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Luis;
using Microsoft.ApplicationInsights;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using PreludeSysDebi.DataService;
using PreludeSysDebi.Dialogs.Shared;
using PreludeSysDebi.Logging;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi.Dialogs.Onboarding
{
    public class OnboardingDialog : EnterpriseDialog
    {
        private static OnboardingResponses _responder = new OnboardingResponses();
        private IStatePropertyAccessor<OnboardingState> _accessor;
        private OnboardingState _state;
        private BotServices _services;
        private IPreludeLogger _logger;
        private AzureSQLDBService _sqlService;

        public OnboardingDialog(BotServices botServices, IStatePropertyAccessor<OnboardingState> accessor, IBotTelemetryClient telemetryClient,
           IPreludeLogger preludeLogger, AzureSQLDBService sqlService)
            : base(botServices, nameof(OnboardingDialog))
        {
            _services = botServices ?? throw new ArgumentNullException(nameof(botServices));
            _accessor = accessor;
            _logger = preludeLogger;
            _sqlService = sqlService;
            InitialDialogId = nameof(OnboardingDialog);
            var onboarding = new WaterfallStep[]
            {
                AskUserInfo
            };
            // To capture built-in waterfall dialog telemetry, set the telemetry client 
            // to the new waterfall dialog and add it to the component dialog
            TelemetryClient = telemetryClient;
            AddDialog(new WaterfallDialog(InitialDialogId, onboarding) { TelemetryClient = telemetryClient });
            AddDialog(new TextPrompt(DialogIds.AskUserInfo));
        }

        public async Task<DialogTurnResult> AskUserInfo(WaterfallStepContext sc, CancellationToken cancellationToken)
        {
            _state = await _accessor.GetAsync(sc.Context, () => new OnboardingState());
            _state.ConversationId = sc.Context.Activity.Conversation.Id;
            _state.ChannelId = sc.Context.Activity.ChannelId;
            _state.Date = DateTime.UtcNow.ToShortDateString();
            _state.Time = DateTime.UtcNow.ToShortTimeString();
            await _accessor.SetAsync(sc.Context, _state);
            var activity = sc.Context.Activity;
            return await sc.PromptAsync(DialogIds.AskUserInfo, new PromptOptions()
            {
                Prompt = await _responder.RenderTemplate(sc.Context, sc.Context.Activity.Locale, OnboardingResponses.ResponseIds.AskUserInfo),
            });
        }

        public async Task<DialogTurnResult> FinishOnboardingDialog(WaterfallStepContext sc, CancellationToken cancellationToken)
        {
            _state = await _accessor.GetAsync(sc.Context);
            return await sc.EndDialogAsync();
        }


        public Task<bool> CustomPromptValidatorAsync(PromptValidatorContext<string> promptContext, CancellationToken cancellationToken)
        {
            var dispatchResult = _services.DispatchRecognizer.RecognizeAsync<Dispatch>(promptContext.Context, true, CancellationToken.None);
            var intent = dispatchResult.Result.TopIntent().intent;

            if (dispatchResult.Result.TopIntent().score > 0.5 && (intent == Dispatch.Intent.l_general || intent == Dispatch.Intent.q_faq))
            {
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }


        private class DialogIds
        {
            public const string AskUserInfo = "AskUserInfo";
        }


        private Task<bool> UserInfoValidatorAsync(
            PromptValidatorContext<string> promptContext,
            CancellationToken cancellationToken)
        {
            var customResult = CustomPromptValidatorAsync(promptContext, cancellationToken).Result;

            if (!customResult)
                return Task.FromResult(false);

            var testString = promptContext.Recognized.Value;
            var inputValid = Validation.CustomValidator.IsNameValid(testString);
            if (!inputValid)
            {
                promptContext.Context.SendActivitiesAsync(
                    new Activity[]
                    {
                          MessageFactory.Text("Invalid name! Please enter text only"),
                    },
                    cancellationToken: cancellationToken);
                return Task.FromResult(false);
            }

            var emailEntered = promptContext.Recognized.Value;
            var emailValid = Validation.CustomValidator.IsEmailValid(emailEntered);
            if (!emailValid)
            {
                promptContext.Context.SendActivitiesAsync(
                   new Activity[]
                   {
                    MessageFactory.Text("Invalid email ID! Please provide a valid email ID."),
                   },
                   cancellationToken: cancellationToken);
                return Task.FromResult(false);
            }

            var phoneNo = promptContext.Recognized.Value;
            var phoneValid = Validation.CustomValidator.IsPhoneNumberValid(phoneNo);
            if (!phoneValid)
            {
                promptContext.Context.SendActivitiesAsync(
                   new Activity[]
                   {
                           MessageFactory.Text("Invalid phone number! Please enter a valid 10 digit phone number without spaces or symbols."),
                   },
                   cancellationToken: cancellationToken);
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }

    }
}

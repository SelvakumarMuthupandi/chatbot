﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
using System;
namespace PreludeSysDebi.Dialogs.Onboarding
{
    public class OnboardingState
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string SendTranscriptEmail { get; set; }

        public string ConversationId { get; set; }

        public string Date {get; set;}

        public string Time { get; set; }

        public string ChannelId { get; set; }

        public bool IsAgent { get; set; }


        public bool ReVisiting { get; set; } = false;

        public string ClientUserName { get; set; }
    }
}

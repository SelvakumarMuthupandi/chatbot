﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.TemplateManager;
using Microsoft.Bot.Schema;
using PreludeSysDebi.Dialogs.Onboarding.Resources;

namespace PreludeSysDebi.Dialogs.Onboarding
{
    public class OnboardingResponses : TemplateManager
    {
        private static LanguageTemplateDictionary _responseTemplates = new LanguageTemplateDictionary
        {
            ["default"] = new TemplateIdMap
            {
                { ResponseIds.AskUserInfo,
                    (context, data) =>

                    MessageFactory.Attachment(
                        Utility.CreateAdaptiveCardAttachment(@".\Dialogs\Onboarding\Resources\OnboardingDialog.json"),
                        ssml: OnboardingStrings.AskUserInfo,
                        inputHint: InputHints.ExpectingInput  )
                }
            }
        };

        public OnboardingResponses()
        {
            Register(new DictionaryRenderer(_responseTemplates));
        }

        public class ResponseIds
        {
            public const string AskUserInfo = "AskUserInfo";
            public const string SendTranscriptEmail = "sendTranscriptEmail";
            public const string HaveSendTranscriptResponse = "haveSendTranscriptResponse";
        }
    }
}

﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using PreludeSysDebi.DataService;
using PreludeSysDebi.Dialogs.Onboarding;
using PreludeSysDebi.Dialogs.Shared;
using PreludeSysDebi.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi.Dialogs.ScheduleMeeting
{
    public class ScheduleMeetingDialog : EnterpriseDialog
    {
        private static ScheduleMeetingResponses _responder = new ScheduleMeetingResponses();
        private IStatePropertyAccessor<ScheduleMeetingState> _accessor;
        private ScheduleMeetingState _state;
        private OnboardingState _onboardingState;
        private IStatePropertyAccessor<OnboardingState> _onboardingStateAccessor;
        private BotServices _services;
        private IPreludeLogger _logger;

        public ScheduleMeetingDialog(BotServices botServices, IStatePropertyAccessor<ScheduleMeetingState> accessor, IStatePropertyAccessor<OnboardingState> onboardingAccessor, IBotTelemetryClient telemetryClient,
           IPreludeLogger preludeLogger)
            : base(botServices, nameof(ScheduleMeetingDialog))
        {
            _services = botServices ?? throw new ArgumentNullException(nameof(botServices));
            _accessor = accessor;
            _onboardingStateAccessor = onboardingAccessor;
            _logger = preludeLogger;
            InitialDialogId = nameof(ScheduleMeetingDialog);

            var scheduleMeeting = new WaterfallStep[]
            {
                AskForMeetingInfo,
                FinishScheduleMeetingDialog,
            };

            TelemetryClient = telemetryClient;
            AddDialog(new WaterfallDialog(InitialDialogId, scheduleMeeting) { TelemetryClient = telemetryClient });
            AddDialog(new TextPrompt(DialogIds.MeetingInfoPrompt, MeetingInfoValidatorAsync));
        }

        public async Task<DialogTurnResult> AskForMeetingInfo(WaterfallStepContext sc, CancellationToken cancellationToken)
        {
            _onboardingState = await _onboardingStateAccessor.GetAsync(sc.Context, () => new OnboardingState());
            _state = await _accessor.GetAsync(sc.Context, () => new ScheduleMeetingState());
            if (string.IsNullOrWhiteSpace(_onboardingState.Name))
            {
                return await sc.PromptAsync(DialogIds.MeetingInfoPrompt, new PromptOptions()
                {
                    Prompt = await _responder.RenderTemplate(sc.Context, sc.Context.Activity.Locale, ScheduleMeetingResponses.ResponseIds.AskUserInfoWithMeeting),
                    RetryPrompt = MessageFactory.Text("Please give your contact information alogin with meeting details."),
                });
            }
            else
            {
                return await sc.PromptAsync(DialogIds.MeetingInfoPrompt, new PromptOptions()
                {
                    Prompt = await _responder.RenderTemplate(sc.Context, sc.Context.Activity.Locale, ScheduleMeetingResponses.ResponseIds.MeetingPrompt),
                    RetryPrompt = MessageFactory.Text("Please input valid values and retry schedule meetings."),
                });
            }
        }

        public async Task<DialogTurnResult> FinishScheduleMeetingDialog(WaterfallStepContext sc, CancellationToken cancellationToken)
        {
            _state = await _accessor.GetAsync(sc.Context);
            return await sc.EndDialogAsync();
        }


        public Task<bool> CustomPromptValidatorAsync(PromptValidatorContext<string> promptContext, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }


        private class DialogIds
        {
            public const string MeetingInfoPrompt = "meetingInfoPrompt";
        }


        private async Task<bool> MeetingInfoValidatorAsync(
            PromptValidatorContext<string> promptContext,
            CancellationToken cancellationToken)
        {
            var customResult = await CustomPromptValidatorAsync(promptContext, cancellationToken);

            if (!customResult)
                return false;
            return true;
        }
    }
}

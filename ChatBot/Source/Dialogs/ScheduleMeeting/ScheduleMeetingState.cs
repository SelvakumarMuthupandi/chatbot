﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSysDebi.Dialogs.ScheduleMeeting
{
    public class ScheduleMeetingState
    {
        public string MeetingDate { get; set; }
        public string TimeZone { get; set; }
        public string Time { get; set; }
    }
}

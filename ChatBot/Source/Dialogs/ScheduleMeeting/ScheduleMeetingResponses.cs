﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using PreludeSysDebi.Dialogs.ScheduleMeeting.Resources;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.TemplateManager;
using Microsoft.Bot.Schema;

namespace PreludeSysDebi.Dialogs.ScheduleMeeting
{
    public class ScheduleMeetingResponses : TemplateManager
    {
        private static LanguageTemplateDictionary _responseTemplates = new LanguageTemplateDictionary
        {
            ["default"] = new TemplateIdMap
            {
                { ResponseIds.MeetingPrompt,
                    (context, data) =>
                    MessageFactory.Attachment(
                        Utility.CreateAdaptiveCardAttachment(@".\Dialogs\ScheduleMeeting\Resources\ScheduleMeeting.json"),
                        ssml: ScheduleMeetingStrings.MeetingPrompt,
                        inputHint: InputHints.ExpectingInput  )

                },
                {ResponseIds.AskUserInfoWithMeeting,
                    (context, data)=>
                    MessageFactory.Attachment(
                        Utility.CreateAdaptiveCardAttachment(@".\Dialogs\ScheduleMeeting\Resources\ScheduleMeetingWithUserInfo.json"),
                        ssml:ScheduleMeetingStrings.MeetingPrompt,
                        inputHint:InputHints.ExpectingInput)

                },
            }
        };

        public ScheduleMeetingResponses()
        {
            Register(new DictionaryRenderer(_responseTemplates));
        }

        public class ResponseIds
        {
            public const string MeetingPrompt = "meetingPrompt";
            public const string HaveMeetingInfoMessage = "haveMeetingInfo";
            public const string DateNotAvailableMessage = "dateNotAvailableMessage";
            public const string TimeNotAvailableMessage = "timeNotAvailableMessage";
            public const string Title = "title";
            public const string BodyText = "bodyText";

            public const string AskUserInfoWithMeeting = "AskUserInfoWithMeeting";
        }
    }
}

﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Azure.Documents;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using PreludeSysDebi.DataService;
using PreludeSysDebi.Dialogs.Main;
using PreludeSysDebi.Dialogs.Onboarding;
using PreludeSysDebi.Dialogs.ScheduleMeeting;
using PreludeSysDebi.Dialogs.Shared;
using PreludeSysDebi.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi
{
    /// <summary>
    /// Main entry point and orchestration for bot.
    /// </summary>
    public class PreludeSysDebi : IBot
    {
        private readonly BotServices _services;
        private readonly ConversationState _conversationState;
        private readonly UserState _userState;
        private readonly IBotTelemetryClient _telemetryClient;
        private readonly AzureBlobTranscriptStore _transcriptStore;
        private DialogSet _dialogs;
        private IPreludeLogger _appLogger;
        private CosmoDocumentService _cosmoDocService;
        private AzureSQLDBService _sqlDBService;
        private IConfiguration _config;
        private string _userEmail = string.Empty;
        private IStatePropertyAccessor<OnboardingState> _onboardingStateAccessor;
        // Create cancellation token (used by Async Write operation).
        public CancellationToken cancellationToken { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="PreludeSysDebi"/> class.
        /// </summary>
        /// <param name="botServices">Bot services.</param>
        /// <param name="conversationState">Bot conversation state.</param>
        /// <param name="userState">Bot user state.</param>
        public PreludeSysDebi(BotServices botServices, ConversationState conversationState, UserState userState, IBotTelemetryClient telemetryClient, IPreludeLogger cosmoLogger, AzureBlobTranscriptStore transcriptStore,
            AzureSQLDBService sqlDBService, CosmoDocumentService cosmoDocService, SMTPEmailService.EmailService emailServ, IConfiguration config)
        {
            _conversationState = conversationState ?? throw new ArgumentNullException(nameof(conversationState));
            _userState = userState ?? throw new ArgumentNullException(nameof(userState));
            _services = botServices ?? throw new ArgumentNullException(nameof(botServices));
            _telemetryClient = telemetryClient ?? throw new ArgumentNullException(nameof(telemetryClient));
            _transcriptStore = transcriptStore ?? throw new ArgumentNullException(nameof(transcriptStore));
            _appLogger = cosmoLogger;
            _cosmoDocService = cosmoDocService;
            _sqlDBService = sqlDBService;
            _config = config;
            _dialogs = new DialogSet(_conversationState.CreateProperty<DialogState>(nameof(PreludeSysDebi)));
            _onboardingStateAccessor = _userState.CreateProperty<OnboardingState>(nameof(OnboardingState));
            _dialogs.Add(new MainDialog(_services, _conversationState, _userState, _onboardingStateAccessor, _userState.CreateProperty<ScheduleMeetingState>(nameof(ScheduleMeetingState)), _telemetryClient, cosmoLogger, _transcriptStore, sqlDBService, _cosmoDocService, emailServ, config));
        }

        /// <summary>
        /// Run every turn of the conversation. Handles orchestration of messages.
        /// </summary>
        /// <param name="turnContext">Bot Turn Context.</param>
        /// <param name="cancellationToken">Task CancellationToken.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var act = turnContext.Activity;
            act.From.Name = "You";
            act.From.Role = "User";
            //if (act.From.Role == null || (act.From.Role.ToLower() == "user" ))
            //{
            //    var _state = await _onboardingStateAccessor.GetAsync(turnContext, () => new OnboardingState());
            //    if (string.IsNullOrWhiteSpace(_state.Name))
            //    {
            //        act.From.Name = "You";
            //    }
            //    else
            //    {
            //        act.From.Name = "You"; // _state.Name;
            //    }
            //    act.From.Role = "User";
            //}
            if (act.From.Role == null || (act.From.Role.ToLower() == "bot"))
            {
                act.From.Name = "P";
            }
            if (turnContext == null)
            {
                throw new ArgumentNullException(nameof(turnContext));
            }

            if (turnContext.Activity.Code == EndOfConversationCodes.BotTimedOut)
            {
                _services.TelemetryClient.TrackTrace($"Timeout in {turnContext.Activity.ChannelId} channel: Bot took too long to respond.");
                return;
            }

            var dc = await _dialogs.CreateContextAsync(turnContext);

            if (dc.ActiveDialog != null)
            {
                var result = await dc.ContinueDialogAsync();
            }
            else
            {
                await dc.BeginDialogAsync(nameof(MainDialog));
            }
            return;
        }
    }
}

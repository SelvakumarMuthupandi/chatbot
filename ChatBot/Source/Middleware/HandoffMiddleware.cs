﻿using Luis;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector.Authentication;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using PreludeSys.Bot.MessageRouting;
using PreludeSys.Bot.MessageRouting.DataStore;
using PreludeSys.Bot.MessageRouting.DataStore.Azure;
using PreludeSys.Bot.MessageRouting.DataStore.Local;
using PreludeSys.Bot.MessageRouting.Models;
using PreludeSys.Bot.MessageRouting.Results;
using PreludeSysDebi.CommandHandling;
using PreludeSysDebi.ConversationHistory;
using PreludeSysDebi.Dialogs.Onboarding;
using PreludeSysDebi.Dialogs.ScheduleMeeting;
using PreludeSysDebi.MessageRouting;
using PreludeSysDebi.Resources;
using PreludeSysDebi.SMTPEmailService;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSysDebi.Middleware
{
    public class HandoffMiddleware : IMiddleware
    {
        private const string KeyAzureTableStorageConnectionString = "AzureTableStorageConnectionString";
        private const string KeyRejectConnectionRequestIfNoAggregationChannel = "RejectConnectionRequestIfNoAggregationChannel";
        private const string KeyPermittedAggregationChannels = "PermittedAggregationChannels";
        private const string KeyNoDirectConversationsWithChannels = "NoDirectConversationsWithChannels";


        public IConfiguration Configuration
        {
            get;
            protected set;
        }

        public MessageRouter MessageRouter
        {
            get;
            protected set;
        }
        public MessageRouterResultHandler MessageRouterResultHandler
        {
            get;
            protected set;
        }
        public CommandHandler CommandHandler
        {
            get;
            protected set;
        }


        public MessageLogs MessageLogs
        {
            get;
            protected set;
        }
        private AzureBlobTranscriptStore _transcriptStore
        {
            get;
            set;
        }
        private IStatePropertyAccessor<OnboardingState> _onBoardingStateAccessor
        {
            get;
            set;
        }
        private IStatePropertyAccessor<ScheduleMeetingState> _scheduleMeetingAccessor
        {
            get;
            set;
        }
        private EmailService _emailService
        {
            get;
            set;
        }

        private readonly BotServices _services;
        public HandoffMiddleware(IConfiguration configuration, AzureBlobTranscriptStore transcriptStore, IStatePropertyAccessor<OnboardingState> accessor, IStatePropertyAccessor<ScheduleMeetingState> meetingAccessor, BotServices botservices,
            EmailService emailServ)
        {
            Configuration = configuration;
            string connectionString = Configuration[KeyAzureTableStorageConnectionString];
            IRoutingDataStore routingDataStore = null;
            _transcriptStore = transcriptStore;
            _onBoardingStateAccessor = accessor;
            _scheduleMeetingAccessor = meetingAccessor;
            _services = botservices;
            _emailService = emailServ;
            if (string.IsNullOrEmpty(connectionString))
            {
                System.Diagnostics.Debug.WriteLine($"WARNING!!! No connection string found - using {nameof(InMemoryRoutingDataStore)}");
                routingDataStore = new InMemoryRoutingDataStore();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"Found a connection string - using {nameof(AzureTableRoutingDataStore)}");
                routingDataStore = new AzureTableRoutingDataStore(connectionString);
            }

            MessageRouter = new MessageRouter(
                routingDataStore,
                new MicrosoftAppCredentials(Configuration["MicrosoftAppId"], Configuration["MicrosoftAppPassword"]));

            MessageRouterResultHandler = new MessageRouterResultHandler(MessageRouter);

            ConnectionRequestHandler connectionRequestHandler =
                new ConnectionRequestHandler(GetChannelList(KeyNoDirectConversationsWithChannels));

            CommandHandler = new CommandHandler(
                MessageRouter,
                MessageRouterResultHandler,
                connectionRequestHandler,
                _transcriptStore,
                _onBoardingStateAccessor,
                _scheduleMeetingAccessor,
                Configuration,
                _emailService,
                GetChannelList(KeyPermittedAggregationChannels)
                );

            MessageLogs = new MessageLogs(connectionString);
        }

        public async Task OnTurnAsync(ITurnContext context, NextDelegate next, CancellationToken cancellationToken = default(CancellationToken))
        {
            Activity activity = context.Activity;

            if (activity.Type == ActivityTypes.Event && activity.Name == "setUserName")
            {
                var userState = await _onBoardingStateAccessor.GetAsync(context, () => new OnboardingState());
                if (activity.Value != null)
                {
                    userState.ClientUserName = activity.Value.ToString();
                    await _onBoardingStateAccessor.SetAsync(context, userState);
                }
            }
            if (activity.From.Role == null || (activity.From.Role.ToLower() != "bot" && String.IsNullOrEmpty(activity.From.Name)))
            {
                var userState = await _onBoardingStateAccessor.GetAsync(context, () => new OnboardingState());
                activity.From.Name = "You"; // userState.Name;
                activity.From.Role = userState.IsAgent ? "Agent" : "User";
            }
            if (activity.Type != ActivityTypes.Message)
            {
                await next(cancellationToken).ConfigureAwait(false);
                return;
            }
            if (activity.Type is ActivityTypes.Message)
            {
                // For events activity.Value will have user provided inputs
                // that needs to be handle at next step.
                if (activity.Value != null)
                {
                    await next(cancellationToken).ConfigureAwait(false);
                    return;
                }
                var userState = await _onBoardingStateAccessor.GetAsync(context, () => new OnboardingState());
                var result = await ValidateUserInfo(context, userState, cancellationToken);
                if (!result)
                {
                    if (activity.Text != "schedule meeting")
                    {
                        var reply = context.Activity.CreateReply("I will get to that shortly. Meanwhile, may I have the above information for our future reference, please…");
                        await context.SendActivityAsync(reply, cancellationToken);
                        return;
                    }
                }
                AbstractMessageRouterResult messageRouterResult = null;
                bool.TryParse(
                    Configuration[KeyRejectConnectionRequestIfNoAggregationChannel],
                    out bool rejectConnectionRequestIfNoAggregationChannel);
                Activity commandActivity = context.Activity;
                Command command = Command.FromMessageActivity(commandActivity);

                if (command == null)
                {
                    // Check for back channel command
                    command = Command.FromChannelData(commandActivity);
                }
                if (command != null)
                {
                    bool.TryParse(
                        Configuration[KeyRejectConnectionRequestIfNoAggregationChannel],
                        out rejectConnectionRequestIfNoAggregationChannel);

                    // Store the conversation references (identities of the sender and the recipient [bot])
                    // in the activity
                    MessageRouter.StoreConversationReferences(context.Activity);

                    // Check the activity for commands
                    if (await CommandHandler.HandleCommandAsync(context))
                    {
                        await MessageRouterResultHandler.HandleResultAsync(messageRouterResult);
                        return;
                    }
                }


                // Let the message router route the activity, if the sender is connected with
                // another user/bot
                messageRouterResult = await MessageRouter.RouteMessageIfSenderIsConnectedAsync(activity);

                if (messageRouterResult is MessageRoutingResult
                    && (messageRouterResult as MessageRoutingResult).Type != MessageRoutingResultType.NoActionTaken)
                {
                    await MessageRouterResultHandler.HandleResultAsync(messageRouterResult);
                    return;
                }


                var dispatchResult = await _services.DispatchRecognizer.RecognizeAsync<Dispatch>(context, true, CancellationToken.None);
                var intent = dispatchResult.TopIntent().intent;
                if (dispatchResult.TopIntent().score > 0.5 &&
                    (intent == Dispatch.Intent.q_faq ||
                        intent == Dispatch.Intent.I_ThankYou ||
                        intent == Dispatch.Intent.l_general ||
                        intent == Dispatch.Intent.I_ScheduleMeeting))
                {
                    await next(cancellationToken).ConfigureAwait(false);
                    return;
                }

                if (dispatchResult?.TopIntent().score > 0.5 && intent == Dispatch.Intent.I_LiveAgent)
                {
                    // Store the conversation references (identities of the sender and the recipient [bot])
                    // in the activity
                    MessageRouter.StoreConversationReferences(context.Activity);

                    messageRouterResult = await HandleLiveAgentRequest(context, activity, rejectConnectionRequestIfNoAggregationChannel, messageRouterResult, cancellationToken);
                    await MessageRouterResultHandler.HandleResultAsync(messageRouterResult);
                }
                else
                {
                    // No action taken - this middleware did not consume the activity so let it propagate
                    await next(cancellationToken).ConfigureAwait(false);
                    // At next level when there is no match in LUIC activity text is set to confused
                    // need to handle confused here.
                    if (activity.Type is ActivityTypes.Message && activity.Text == "confused")
                    {
                        await HandleConfusedState(context, activity);
                    }
                }
            }
        }

        private async Task<AbstractMessageRouterResult> HandleLiveAgentRequest(ITurnContext context, Activity activity, bool rejectConnectionRequestIfNoAggregationChannel, AbstractMessageRouterResult messageRouterResult, CancellationToken cancellationToken)
        {
            PagedResult<IActivity> pagedResult = await _transcriptStore.GetTranscriptActivitiesAsync(activity.ChannelId, activity.Conversation.Id);
            ConversationData conversationDetails = new ConversationData();
            conversationDetails = BlobStorageUtils.Utility.GetConversationData(pagedResult);
            if (conversationDetails.Conversations?.Count > 0 && !string.IsNullOrEmpty(conversationDetails.Conversations[0].Text))
            {
                OnboardingState state = await _onBoardingStateAccessor.GetAsync(context, () => new OnboardingState());
                string toEmail = Configuration.GetSection("AgentRequestNotificationEmail")?.Value ?? "";
                string meetingInfo = string.Format(@"Customer {0} with email '{1}' and phone number '{2}' requested to talk to Agent  . <br/> <br/> Below are the conversation with him. <br/> <br/>",
                    state.Name, state.Email, state.PhoneNumber);
                string chatTranscripts = conversationDetails.Conversations[0].Text;
                _emailService.SendEmail(new MailAddress[] { new MailAddress(toEmail) }, "Agent request from customer ", meetingInfo + chatTranscripts);
            }

            // Create a connection request on behalf of the sender
            // Note that the returned result must be handled
            messageRouterResult = MessageRouter.CreateConnectionRequest(
                MessageRouter.CreateSenderConversationReference(activity),
                rejectConnectionRequestIfNoAggregationChannel);

            return messageRouterResult;
        }

        private async Task HandleConfusedState(ITurnContext context, Activity activity)
        {
            bool.TryParse(
               Configuration[KeyRejectConnectionRequestIfNoAggregationChannel],
               out bool rejectConnectionRequestIfNoAggregationChannel);
            AbstractMessageRouterResult messageRouterResult = null;
            messageRouterResult = await MessageRouter.RouteMessageIfSenderIsConnectedAsync(activity);
            // Create a connection request on behalf of the sender
            // Note that the returned result must be handled
            messageRouterResult = MessageRouter.CreateConnectionRequest(
                MessageRouter.CreateSenderConversationReference(activity),
                rejectConnectionRequestIfNoAggregationChannel);
            //bool result =  await MessageRouterResultHandler.HandleResultAsync(messageRouterResult);
            if (messageRouterResult is ConnectionRequestResult)
            {
                ConnectionRequestResult connectionRequestResult = messageRouterResult as ConnectionRequestResult;
                ConnectionRequest connectionRequest = connectionRequestResult?.ConnectionRequest;

                if (connectionRequest != null && connectionRequest.Requestor != null)
                {
                    IMessageActivity replyAction = Activity.CreateMessageActivity();

                    replyAction.ApplyConversationReference(connectionRequest.Requestor);

                    if (connectionRequestResult.Type == ConnectionRequestResultType.NotSetup)
                    {
                        replyAction.Text = @"Sorry, I don't have the information for your request.  I'll be glad to schedule a call with our agent, who can help you with your queries.";
                        replyAction.SuggestedActions = new SuggestedActions()
                        {
                            Actions = new List<CardAction>()
                                  {
                                          new CardAction() { Title = "Schedule a Call", Type = ActionTypes.ImBack, Value = "schedule meeting"  },
                                  },
                        };
                    }
                    else
                    {
                        replyAction.Text = @"Sorry, I don't have the information for your request.  I'll be glad to schedule a call with our agent, who can help you with your queries.";
                        replyAction.SuggestedActions = new SuggestedActions()
                        {
                            Actions = new List<CardAction>()
                                  {
                                        new CardAction() { Title = "Talk to Agent", Type = ActionTypes.ImBack, Value = "customer service"  },
                                        new CardAction() { Title = "Schedule a Call", Type = ActionTypes.ImBack, Value = "schedule meeting"  },
                                  },
                        };

                    }
                    await context.SendActivitiesAsync(new IActivity[] { replyAction });
                }
            }
        }

        private async Task<bool> ValidateUserInfo(ITurnContext context, OnboardingState userInfo, CancellationToken cancellationToken)
        {
            return await Task.Run<bool>(() =>
            {
                bool revisitingCustomer = userInfo.ReVisiting;
                var isNameValid = Validation.CustomValidator.IsNameValid(userInfo.Name);
                var isEmailValid = Validation.CustomValidator.IsEmailValid(userInfo.Email);
                var isPhoneValid = Validation.CustomValidator.IsPhoneNumberValid(userInfo.PhoneNumber);
                if (!isNameValid || !isEmailValid || !isPhoneValid)
                {

                    return false;
                }
                return true;
            });
            
        }

        /// <summary>
        /// Extracts the channel list from the settings matching the given key.
        /// </summary>
        /// <returns>The list of channels or null, if none found.</returns>
        private IList<string> GetChannelList(string key)
        {
            IList<string> channelList = null;

            string channels = Configuration[key];

            if (!string.IsNullOrWhiteSpace(channels))
            {
                System.Diagnostics.Debug.WriteLine($"Channels by key \"{key}\": {channels}");
                string[] channelArray = channels.Split(',');

                if (channelArray.Length > 0)
                {
                    channelList = new List<string>();

                    foreach (string channel in channelArray)
                    {
                        channelList.Add(channel.Trim());
                    }
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"No channels defined by key \"{key}\" in app settings");
            }

            return channelList;
        }
    }
}

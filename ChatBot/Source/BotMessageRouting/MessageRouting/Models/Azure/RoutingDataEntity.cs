﻿using Microsoft.WindowsAzure.Storage.Table;

namespace PreludeSys.Bot.MessageRouting.Models.Azure
{
    public class RoutingDataEntity : TableEntity
    {
        public string Body { get; set; }
    }
}

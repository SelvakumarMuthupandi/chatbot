﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public class ConversationData
    {
        public string ChannelId { get; set; }

        public DateTime? TimeStamp { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? StartDate { get; set; }

        public string Id { get; set; }

        public string Text { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<ConversationData> Conversations { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class OnboardingState
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string SendTranscriptEmail { get; set; }

        public string ConversationId { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string ChannelId { get; set; }
    }

    public class OnBoardingStateList
    {

        public List<OnboardingState> data { get; set; }
    }

}
﻿using Microsoft.Azure.Documents.Client;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Configuration;
using Microsoft.Bot.Schema;
using Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace ChatBotConversation
{
    public class GetConversationHistory
    {
        private readonly AzureBlobTranscriptStore _transcriptStore;
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationHistoryBot"/> class.
        /// </summary>
        /// <param name="transcriptStore">Injected via ASP.NET dependency injection.</param>
        public GetConversationHistory(AzureBlobTranscriptStore transcriptStore)
        {
            _transcriptStore = transcriptStore ?? throw new ArgumentNullException(nameof(transcriptStore));
        }

        public async Task<Models.ConversationData> GetConversationDetails(OnBoardingStateList onBoardingStates, double timezoneOffset)
        {
            Models.ConversationData conversationDetails = new Models.ConversationData();
            conversationDetails.Conversations = new List<Models.ConversationData>();
            var numTranscripts = onBoardingStates.data.Count();
            foreach (OnboardingState onbState in onBoardingStates.data)
            {
                if (!string.IsNullOrEmpty(onbState.Name) && (!string.IsNullOrEmpty(onbState.Email) || !string.IsNullOrEmpty(onbState.Phone)))
                {
                    ConversationData convData = await GetConversationDetailsById(onbState.ConversationId, timezoneOffset, onbState.ChannelId);
                    if (convData.Conversations != null && convData.Conversations.Count > 0)
                    {
                        var conData = convData.Conversations[0];
                        conData.Name = onbState.Name;
                        conData.Email = onbState.Email;
                        conData.Phone = onbState.Phone;
                        conversationDetails.Conversations.Add(conData);
                    }
                }
            }
            return conversationDetails;
        }


        public async Task<ConversationData> GetConversationDetailsById(string ConversationId, double timezoneOffset, string channelId)
        {
            ConversationData conversationDetails = new ConversationData();
            conversationDetails.Conversations = new List<ConversationData>();
            ConversationData conversationData = new ConversationData();
            string continuationToken = null;
            string currentConversationId = string.Empty;
            bool parentDataDone = false;
            PagedResult<IActivity> pagedActivities = null;
            do
            {
                string conversationIdChanged = "N";
                pagedActivities = await _transcriptStore.GetTranscriptActivitiesAsync(channelId, ConversationId, pagedActivities?.ContinuationToken);
                foreach (var item in pagedActivities.Items)
                {
                    if (item.Type.ToUpper() == "MESSAGE")
                    {
                        var thisMessage = item.AsMessageActivity();
                        if (currentConversationId != thisMessage.Conversation.Id)
                        {
                            if (parentDataDone)
                            {
                                conversationIdChanged = "Y";
                            }
                            parentDataDone = !parentDataDone;
                            currentConversationId = thisMessage.Conversation.Id;
                            conversationDetails.Conversations.Add(conversationData);
                            conversationData.Id = thisMessage.Conversation.Id;
                            conversationData.ChannelId = thisMessage.ChannelId;
                            conversationData.Text = thisMessage.Text;
                            conversationData.TimeStamp = UtcToLocal(thisMessage.Timestamp.Value.DateTime, timezoneOffset);
                            ConversationId = thisMessage.Conversation.Id;
                        }
                        else if (ConversationId == thisMessage.Conversation.Id)
                        {
                            conversationData.Text += @"<br />" + "<strong>" + item.From.Role + ":" + "</strong>" + thisMessage.Text;
                        }
                    }
                }

                if (conversationIdChanged == "N")
                {
                    continuationToken = pagedActivities.ContinuationToken;
                }

            } while (continuationToken != null);

            return conversationDetails;
        }

        public static DateTime UtcToLocal(DateTime UTCDT, double offset)
        {
            if (offset < 0)
            {
                return UTCDT.AddMinutes(offset * (-1));
            }
            else if (offset > 0)
            {
                return UTCDT.AddMinutes(offset);
            }
            else { return UTCDT; }
        }
    }
}
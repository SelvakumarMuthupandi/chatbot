﻿using Microsoft.Azure.Documents.Client;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Configuration;
using Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;

namespace ChatBotConversation.Controllers
{
    public class ConversationController : Controller
    {
        public ActionResult Index()
        {
            ConversationData conversationData = new ConversationData();
            conversationData.Conversations = new List<ConversationData>();
            return View("Conversation", conversationData);
        }

        public async Task<ActionResult> ConversationHistory(ConversationData conversationData)
        {
            double timezoneOffset = double.Parse(Request.Cookies["TimezoneOffset"].Value.ToString());
            SetConversationDateRange(conversationData, timezoneOffset);

            ViewBag.Message = "Conversation History";
            var conversation = await ConfigureBlob(conversationData, timezoneOffset);
            return View("Conversation", conversation);
        }

        [HttpPost]
        private void SetConversationDateRange(ConversationData conversationData, double timezoneOffset)
        {

            if (conversationData.StartDate != null)
            {
                TempData["startDate"] = conversationData.StartDate;
            }
            if (conversationData.EndDate != null)
            {
                TempData["endDate"] = conversationData.EndDate;
            }

            if (TempData["startDate"] != null)
            {
                conversationData.StartDate = LocalToUtc(Convert.ToDateTime(TempData["startDate"]), timezoneOffset);
                TempData.Keep("startDate");
            }
            if (TempData["endDate"] != null)
            {
                conversationData.EndDate = LocalToUtc(Convert.ToDateTime(TempData["endDate"]).AddDays(1).AddMinutes(-1), timezoneOffset);
                TempData.Keep("endDate");
            }

            if (!string.IsNullOrEmpty(conversationData.Email))
            {
                TempData["email"] = conversationData.Email;

            }

            if (!string.IsNullOrEmpty(conversationData.Name))
            {
                TempData["name"] = conversationData.Name;

            }

            if (!string.IsNullOrEmpty(conversationData.Phone))
            {
                TempData["phone"] = conversationData.Phone;
            }
        }


        public async Task<ConversationData> ConfigureBlob(ConversationData convData, double timezoneOffset)
        {
            List<ConversationData> userInput = new List<ConversationData>();
            var secretKey = WebConfigurationManager.AppSettings["botFileSecret"];
            var botFilePath = WebConfigurationManager.AppSettings["botFilePath"];
            string StorageConfigurationId = WebConfigurationManager.AppSettings["StorageConfigurationId"]?.ToString();
            string DefaultBotContainer = WebConfigurationManager.AppSettings["DefaultBotContainer"]?.ToString();
            string CosmosDBConfigurationId = WebConfigurationManager.AppSettings["CosmosDBConfigurationId"]?.ToString();
            AzureBlobTranscriptStore blobStore = null;
            var botConfig = BotConfiguration.Load(Server.MapPath("~/PreludeSysChatbotDev.bot") ?? @".\PreludeSysChatbotDev.bot", secretKey);

            var blobConfig = botConfig.FindServiceByNameOrId(StorageConfigurationId);

            if (!(blobConfig is BlobStorageService blobStorageConfig))
            {
                throw new InvalidOperationException($"The .bot file does not contain an blob storage with name '{StorageConfigurationId}'.");
            }
            // Default container name.
            var storageContainer = string.IsNullOrWhiteSpace(blobStorageConfig.Container) ? DefaultBotContainer : blobStorageConfig.Container;
            blobStore = new AzureBlobTranscriptStore(blobStorageConfig.ConnectionString, storageContainer);
            GetConversationHistory getConversationHistory = new GetConversationHistory(blobStore);
            var cosmoDBService = botConfig.FindServiceByNameOrId(CosmosDBConfigurationId);

            if (!(cosmoDBService is CosmosDbService))
            {
                throw new InvalidOperationException($"The .bot file does not contain an blob storage with name '{StorageConfigurationId}'.");
            }
            var cosmosDb = cosmoDBService as CosmosDbService;
            var endpoint = cosmosDb.Endpoint;
            var masterKey = cosmosDb.Key;
            if (!convData.StartDate.HasValue)
            {
                convData.StartDate = DateTime.Now.Date;
            }
            if (!convData.EndDate.HasValue)
            {
                convData.EndDate = DateTime.Now.Date;
            }
            Models.OnBoardingStateList onboardings = null;
            using (var client = new DocumentClient(new Uri(endpoint), masterKey))
            {
                //Execute Store Procedure  
                var result = await client.ExecuteStoredProcedureAsync<string>
                (UriFactory.CreateStoredProcedureUri(cosmosDb.Database, cosmosDb.Collection, "Sample1"), new[] { convData.StartDate.Value.ToShortDateString(), convData.EndDate.Value.ToShortDateString(), convData.Name, convData.Email, convData.Phone });

                onboardings = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OnBoardingStateList>(result.Response);
            }
            return await getConversationHistory.GetConversationDetails(onboardings, timezoneOffset);

        }

        public static DateTime LocalToUtc(DateTime LocalDT, double offset)
        {
            if (offset < 0)
            {
                return LocalDT.AddMinutes(offset);
            }
            else if (offset > 0)
            {
                return LocalDT.AddMinutes(offset * (-1));
            }
            else { return LocalDT; }
        }
    }
}
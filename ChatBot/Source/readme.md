﻿
# Enterprise Bot Template - Deploying your Bot

> [!NOTE]
> This topics applies to v4 version of the SDK. 

## Prerequisites

- Ensure the [Node Package manager](https://nodejs.org/en/) is installed.

- Install the Azure Bot Service command line (CLI) tools. It's important to do this even if you've used the tools before to ensure you have the latest versions.

```shell
npm install -g ludown luis-apis qnamaker botdispatch msbot chatdown
```

- Install the Azure Command Line Tools (CLI) from [here](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest)

- Install the AZ Extension for Bot Service
```shell
az extension add -n botservice
```

- Install the LUISGen tool

```shell
dotnet tool install -g luisgen
```

## Configuration

1. Retrieve your LUIS Authoring Key
   - Go to https://www.luis.ai and signin.
   - Once signed in click on your name in the top right hand corner.
   - Choose Settings and make a note of the Authoring Key for the next step.

## Deployment

>If you have multiple Azure subscriptions and want to ensure the deployment selects the correct one, run the following commands before continuing.

 Follow the browser login process into your Azure Account
```shell
az login
az account list
az account set --subscription "YOUR_SUBSCRIPTION_NAME"
```

Enterprise Template Bots require the following dependencies for end to end operation.
- Azure Web App
- Azure Storage Account (Transcripts)
- Azure Application Insights (Telemetry)
- Azure CosmosDb (State)
- Azure Cognitive Services - Language Understanding
- Azure Cognitive Services - QnAMaker (including Azure Search, Azure Web App)
- Azure Cognitive Services - Content Moderator (optional manual step)

Your new Bot project has a deployment recipe enabling the `msbot clone services` command to automate deployment of all the above services into your Azure subscription and ensure the .bot file in your project is updated with all of the services including keys enabling seamless operation of your Bot. It also has multiple configuration options for the following languages: Chinese, English, French, German, Italian, and Spanish.

Run the following command referencing the folder of the language you want to use (e.g. `DeploymentScripts\en`).

> Once deployed, review the Pricing Tiers for the created services and adjust to suit your scenario.

-- Start My Account

https://apps.dev.microsoft.com

Subscription ID - 81f4247a-a8d8-4912-99b2-8108247aee0f

App Name - VirtualDebi
App ID - c2883357-1d91-45a4-9e3e-a8efa63ece0f
pass   - gvbUHSL16*uznvBXU293[)|

Luis App - VirtualDebi
Luis Authoring Key - 2b50c1d8de38429d8719a5daad5fd0fa


***   For selvakumar_m2@preludesys.com ****
***       XiXion@2021                  ****

LUIS Authoring Key -  247911f852a746b2b14c08931784af76

az account set --subscription "e6140164-08da-495f-b400-c7866fde69f6"

msbot clone services --name "PreludeSysChatBotDev" --luisAuthoringKey "247911f852a746b2b14c08931784af76" --folder "DeploymentScripts\en"  --location "westus" --verbose  --appId "49fe02ba-022b-4f01-aad5-66d9a796afd4" --appSecret "xrkNN84[%:yogiWIPMS316["

az bot publish --resource-group PreludeSysChatBotDev -n PreludeSysChatBotDev --subscription e6140164-08da-495f-b400-c7866fde69f6 -v v4 --proj-name "PreludeSysDebi.csproj" --verbose 

az group delete -g PreludeSysChatBotDev --no-wait --subscription e6140164-08da-495f-b400-c7866fde69f6 --verbose


msbot clone services --name "PreludeSysChatBotQA" --luisAuthoringKey "247911f852a746b2b14c08931784af76" --folder "DeploymentScripts\enQA"  --location "westus" --verbose
az bot publish --resource-group PreludeSysChatBotQA -n PreludeSysChatBotQA --subscription e6140164-08da-495f-b400-c7866fde69f6 -v v4 --proj-name "PreludeSysDebi.csproj" --verbose
az group delete -g PreludeSysChatBotQA --no-wait --subscription e6140164-08da-495f-b400-c7866fde69f6 --verbose

msbot clone services --name "PreludeSysChatBotProd" --luisAuthoringKey "247911f852a746b2b14c08931784af76" --folder "DeploymentScripts\enProd"  --location "westus" --verbose
    az bot publish --resource-group PreludeSysChatBotProd -n PreludeSysChatBotProd --subscription e6140164-08da-495f-b400-c7866fde69f6 -v v4 --proj-name "PreludeSysDebi.csproj" --verbose

***   End selvakumar_m2@preludesys.com ****

msbot clone services --name "PreludeSysVDebi" --luisAuthoringKey "2b50c1d8de38429d8719a5daad5fd0fa" --folder "DeploymentScripts\en"  --location "westus" --appId "c2883357-1d91-45a4-9e3e-a8efa63ece0f" --appSecret "gvbUHSL16*uznvBXU293[)|"

Bot Secret key - mD+kCn4QoWsYriRBFO5aN4GmEOFIFG7tTLPi8pNLOgU=

az bot publish --resource-group PreludeSysVDebi -n PreludeSysVDebi --subscription 81f4247a-a8d8-4912-99b2-8108247aee0f -v v4 --proj-name "PreludeSysDebi.csproj" 

az group delete -g PreludeSysVDebi --no-wait --subscription 81f4247a-a8d8-4912-99b2-8108247aee0f


-- End My Account

```shell
msbot clone services --name "PreludeSysVDebi" --luisAuthoringKey "9fd221123b3f49c7b09bcefcafbce79d" --folder "DeploymentScripts\en" --location "westus" --appId "c66d685f-05a7-46ff-a182-3c7488cbcbdf" --appSecret "lubhVCP38{ssgNQMU898!:*"
```

Once this is complete ensure that you make a note of the .bot file secret provided as this will be required for later steps. At this time, take the secret and update the `botFileSecret` entry in your `appsettings.json` file. This will ensure your Bot can decrypt the secrets.

Update your appsettings.json file with the .bot file path, .bot file secret, and AppInsights intrumentation key (this can be found in the generated .bot file).
    
        {
          "botFilePath": ".\\YOUR_BOT_PATH.bot",
          "botFileSecret": "YOUR_BOT_KEY",
          "ApplicationInsights": {
            "InstrumentationKey": "YOUR_INSTRUMENTATION_KEY"
          }
        }
## Testing

Once complete, run your bot project within your development envrionment and open the Bot Framework Emulator. Within the Emulator, choose Open Bot from teh File menu and navigate to the .bot file in your directory.

Then type ```hi``` to verify everything is working.

## Deploy to Azure

Testing can be performed end to end locally, when your ready to deploy your Bot to Azure for additional testing you can use the following command to publish the source code

```shell
az bot publish --name "PreludeSysDebi" --resource-group "PreludeBot"
```

## Analytics
The Enterprise Template is configured to collect telemetry into Application Insights. This can be imported into a Power BI dashboard to view [advanced conversational analytics](https://aka.ms/botPowerBiTemplate).

## Next Steps
Find [additional documentation](http://aka.ms/ent_docs) for enabling more scenarios and further customization.

﻿using System;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using PreludeSysDebi.Logging;

namespace PreludeSysDebi.DataService
{
    public class AzureSQLDBService
    {
        private static string  _connectionString;
        private static IPreludeLogger _logger;

        public AzureSQLDBService(string connectionString, IPreludeLogger logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public SqlConnection GetSQLConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public DataTable GetQueryData(string comandQuery)
        {
            System.Data.DataTable dt = new DataTable();
            try
            {
                using (SqlConnection connection = GetSQLConnection())
                {
                    using (SqlCommand command = new SqlCommand(comandQuery, connection))
                    {

                        System.Data.SqlClient.SqlDataAdapter da = new SqlDataAdapter(comandQuery, connection);
                        da.Fill(dt);
                        return dt;
                    }
                }

            }
            catch (SqlException e)
            {
                _logger.Error( e.ToString());
            }
            return dt;
        }
    }
}

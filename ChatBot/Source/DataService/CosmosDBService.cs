﻿using Microsoft.Azure.Documents.Client;
using Microsoft.Bot.Configuration;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PreludeSysDebi.Dialogs.Onboarding;
using PreludeSysDebi.Dialogs.ScheduleMeeting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSysDebi.DataService
{
    public class CosmosDBService
    {
        private IConfiguration _config;

        public CosmosDBService(IConfiguration config)
        {
            _config = config;
        }
        public async Task<string> GetDocumentAsync(string emailAddress)
        {

            OnboardingState result = new OnboardingState();
         
            var cosmoDBService = GetCosmoDBConnectedService();

            if (!(cosmoDBService is CosmosDbService))
            {
                throw new InvalidOperationException($"The .bot file does not contain an cosmos db storage with the specified name.");
            }
            var cosmosDb = cosmoDBService as CosmosDbService;
            var endpoint = cosmosDb.Endpoint;
            var masterKey = cosmosDb.Key;

            using (var client = new DocumentClient(new Uri(endpoint), masterKey))
            {
                //Execute Store Procedure  
                var queryResult = await client.ExecuteStoredProcedureAsync<string>
               (UriFactory.CreateStoredProcedureUri(cosmosDb.Database, cosmosDb.Collection, "GetDocumentByEmailId"), new[] { emailAddress });
                if (queryResult.Response != "no docs found")
                    result = ((Newtonsoft.Json.JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(queryResult.Response)).First.ElementAt(0)["document"]["OnboardingState"]).ToObject<OnboardingState>();
                else
                    result = new OnboardingState();
            }
            return JsonConvert.SerializeObject(result);
        }

        public async Task<string> GetMeethingSheduleDocumentAsync(string emailAddress)
        {
            ScheduleMeetingState result = new ScheduleMeetingState();
            var cosmoDBService = GetCosmoDBConnectedService();
            if (!(cosmoDBService is CosmosDbService))
            {
                throw new InvalidOperationException($"The .bot file does not contain an cosmos db storage.");
            }
            var cosmosDb = cosmoDBService as CosmosDbService;
            var endpoint = cosmosDb.Endpoint;
            var masterKey = cosmosDb.Key;
            using (var client = new DocumentClient(new Uri(endpoint), masterKey))
            {
                //Execute Store Procedure  
                var queryResult = await client.ExecuteStoredProcedureAsync<string>
               (UriFactory.CreateStoredProcedureUri(cosmosDb.Database, cosmosDb.Collection, "GetScheduledMeetingDetails"), new[] { emailAddress });
                if (queryResult.Response == "no docs found" )
                {
                    return null;
                }
                try
                {
                    result = ((Newtonsoft.Json.JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(queryResult.Response)).First.ElementAt(0)["document"]["ScheduleMeetingState"]).ToObject<ScheduleMeetingState>();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return JsonConvert.SerializeObject(result);
        }


        public ConnectedService GetCosmoDBConnectedService()
        {
            OnboardingState result = new OnboardingState();
            List<ConversationData> userInput = new List<ConversationData>();
            var secretKey = _config.GetSection("botFileSecret").Value;
            var botFilePath = _config.GetSection("botFilePath").Value;
            var CosmosDBConfigurationId = _config.GetSection("CosmosDBConfigurationId").Value;
            var botConfig = BotConfiguration.Load(botFilePath, secretKey);
            return botConfig.FindServiceByNameOrId(CosmosDBConfigurationId);
        }

    }
}

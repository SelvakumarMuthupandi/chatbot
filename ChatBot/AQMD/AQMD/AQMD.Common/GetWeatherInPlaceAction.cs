﻿namespace AQMD
{
    using System;
    using System.Threading.Tasks;
    using AdaptiveCards;
    using AQMD.Dto;
    using System.Collections.Generic;
    using APIXULib;
    using Microsoft.Extensions.Configuration;
    using AQMD.Common;
    using System.Text;

    [Serializable]
    public class GetWeatherInPlaceAction
    {
        private readonly IConfiguration configuration;
        public GetWeatherInPlaceAction(IConfiguration appSettings)
        {
            configuration = appSettings;
        }
        public Task<object> FulfillAsync(string Place, string Date, string Type, string BaseApiPath, string AppKey)
        {
            var result = GetCard(Place, Date, Type, BaseApiPath, AppKey);
            return Task.FromResult((object)result);
        }
        public Task<object> FulfillAsyncAlexa(string Place, string Date, string BaseApiPath, string inputDate)
        {
            var result = GetAlexaCard(Place, Date, BaseApiPath, inputDate);
            return Task.FromResult((object)result);
        }

        private static AlexaCardContent GetAlexaCard(string place, string date, string baseApiPath, string inputDate)
        {
            string startDate = DateTime.Today.ToString();
            string endDate = DateTime.Today.ToString();

            if (date != null && date != "AQMD.WeatherDetails")
            {
                string[] inputText = date.Split(",");
                startDate = inputText[0];
                endDate = inputText[1];

            }
            var httpClient = new HttpClientApiHelper<Dto.ViewModel.AirQualityOutput>();
            //var baseApiPath = configuration.GetSection("BaseApiPath").Value;
            var endPoint = string.Format("/api/v1/airquality/search?location={0}&startDate={1}&endDate={2}&duration=None", System.Net.WebUtility.HtmlEncode(place), startDate, endDate);
            Dto.ViewModel.AirQualityOutput model = httpClient.GetAsync(baseApiPath + endPoint).Result;

            var card = new AlexaCardContent();
            if (model != null)
            {
                card.title = $"Current Weather";
                //card.smallImageUrl = GetIconUrl(model.current.condition.icon);
                //card.text = $"Today the temperature is {model.current.temp_f} farenheit at  { model.location.name} .Winds are {model.current.wind_mph} miles per hour from the {model.current.wind_dir}";
                card.text = $"Aqi value is {model.AQI} which is  { model.AQICategory}  at {model.AreaName} in {model.CityName} for {inputDate} . The major pollutant is {model.Pollutant}.";
                return card;
            }
            else
            {
                card.title = $"Current Weather";
                //card.smallImageUrl = GetIconUrl(model.current.condition.icon);
                //card.text = $"Today the temperature is {model.current.temp_f} farenheit at  { model.location.name} .Winds are {model.current.wind_mph} miles per hour from the {model.current.wind_dir}";
                card.text = $"We don't have air quality information for this place or time.";
                return card;

            }
        }
        private AdaptiveCard GetCard(string place, string date, string type, string baseApiPath, string AppKey)
        {
            string startDate = DateTime.Today.ToString();
            string endDate = DateTime.Today.ToString();

            if (date != null && date != "AQMD.WeatherDetails")
            {
                string[] inputText = date.Split(",");
                startDate = inputText[0];
                endDate = inputText[1];

            }
            var card = new AdaptiveCard("1.1");
            if (type == "Air Quality")
            {
                var httpClient = new HttpClientApiHelper<Dto.ViewModel.AirQualityOutput>();
                var endPoint = string.Format("/api/v1/airquality/search?location={0}&startDate={1}&endDate={2}&duration=None", System.Net.WebUtility.HtmlEncode(place), startDate, endDate);
                Dto.ViewModel.AirQualityOutput model = httpClient.GetAsync(baseApiPath + endPoint).Result;
                if (model != null)
                {
                    card.Speak = $"<s>Today the temperature is </s>";
                    AddCurrentAirQuality(model, card, startDate, endDate);
                    return card;
                }
            }
            else
            {
                double days = ((Convert.ToDateTime(endDate) - DateTime.Now.Date).TotalDays);
                days = days == 1 ? days + 1 : days;
                long number1 = 0;
                WeatherModel model = null;
                bool canConvert = long.TryParse(place, out number1);
                if (canConvert == true)
                {
                    model = new Repository().GetWeatherData(AppKey, GetBy.Zip, place, (Days)days);
                }
                else
                {
                    model = new Repository().GetWeatherData(AppKey, GetBy.CityName, place, (Days)days);
                }


                if (model != null)
                {
                    if (model.current != null)
                    {
                        card.Speak = $"<s>Today the temperature is {model.current.temperature}</s><s>Winds are {model.current.wind_speed} miles per hour from the {model.current.wind_dir}</s>";
                    }

                    if (model.forecast != null && model.forecast.forecastday != null)
                    {
                        if (days == 2 && Convert.ToDateTime(endDate) > DateTime.Now.Date)
                        {
                            AddForecast(place, model, card);
                        }
                        else
                        {
                            AddCurrentWeather(model, card);
                            AddForecast(place, model, card);
                        }

                        return card;
                    }
                }
            }

            return null;
        }

        public Task<string> GetOverviewAsync(string place, string actualdate, string date, string type, string baseApiPath, string AppKey)
        {
            string startDate = DateTime.Today.ToString();
            string endDate = DateTime.Today.ToString();

            if (date != null && date != "AQMD.WeatherDetails")
            {
                string[] inputText = date.Split(",");
                startDate = inputText[0];
                endDate = inputText[1];

            }
            var card = new AdaptiveCard("1.1");
            if (type == "Air Quality")
            {
                var httpClient = new HttpClientApiHelper<Dto.ViewModel.AirQualityOutput>();
                var endPoint = string.Format("/api/v1/airquality/search?location={0}&startDate={1}&endDate={2}&duration=None", System.Net.WebUtility.HtmlEncode(place), startDate, endDate);
                Dto.ViewModel.AirQualityOutput model = httpClient.GetAsync(baseApiPath + endPoint).Result;
                if (model != null)
                {
                    AddCurrentAirQuality(model, card, startDate, endDate);
                    actualdate = string.IsNullOrEmpty(actualdate) ? actualdate : "for " + actualdate;
                    string locationName = GetLocationByType(model);
                    var overviewText = $"Air Quality at {locationName}  {actualdate} is  {model.AQICategory}";

                    int x = 0;
                    if (Int32.TryParse(place, out x))
                    {
                        overviewText = $"Air Quality at Zipcode {place} - {model.CityName}, {model.StateName} for {actualdate} is  {model.AQICategory}.";
                    }
                    return Task.FromResult(overviewText);
                }
                else
                {
                    return Task.FromResult($"false");
                }
            }
            else
            {
                double days = ((Convert.ToDateTime(endDate) - DateTime.Now.Date).TotalDays);
                days = days == 1 ? days + 1 : days;
                long number1 = 0;
                WeatherModel model = null;
                bool canConvert = long.TryParse(place, out number1);
                if (canConvert == true)
                {
                    model = new Repository().GetWeatherData(AppKey, GetBy.Zip, place, (Days)days);
                }
                else
                {
                    model = new Repository().GetWeatherData(AppKey, GetBy.CityName, place, (Days)days);
                }
                if (model != null)
                {
                    if (model.current != null && Convert.ToDateTime(endDate).Date == DateTime.Now.Date)
                    {
                        var overviewText = $"Right now weather is {model.current.condition.text}  at {place}.";

                        int x = 0;
                        if (Int32.TryParse(place, out x))
                        {
                            overviewText = $"Right now weather is {model.current.condition.text} at zipcode {place} - {model.location.name}, {model.location.country}.";
                        }
                        return Task.FromResult(overviewText);
                    }

                    if (model.forecast != null && model.forecast.forecastday != null)
                    {
                        actualdate = string.IsNullOrEmpty(actualdate) ? actualdate : "for " + actualdate;

                        if (days == 2 && Convert.ToDateTime(endDate) > DateTime.Now.Date)
                        {
                            foreach (var day in model.forecast.forecastday)
                            {
                                if (DateTime.Parse(day.date).DayOfWeek != DateTime.Parse(model.current.last_updated).DayOfWeek)
                                {
                                    var overviewText = $"Weather forecast is {day.day.condition.text}  at {place}.";

                                    int x = 0;
                                    if (Int32.TryParse(place, out x))
                                    {
                                        overviewText = $"Weather forecast is {day.day.condition.text} at zipcode {place} - {model.location.name}, {model.location.country}.";
                                    }
                                    return Task.FromResult(overviewText);

                                }
                            }
                        }
                        else
                        {
                            string weatherforecast = null;
                            foreach (var day in model.forecast.forecastday)
                            {
                                if (DateTime.Parse(day.date).DayOfWeek != DateTime.Parse(model.current.last_updated).DayOfWeek)
                                {
                                    weatherforecast = $"Weather forecast is {day.day.condition.text}  at {place}.";

                                    int x = 0;
                                    if (Int32.TryParse(place, out x))
                                    {
                                        weatherforecast = $"Weather forecast is {day.day.condition.text} at zipcode {place} - {model.location.name}, {model.location.country}.";
                                    }
                                    break;

                                }
                            }
                            return Task.FromResult(weatherforecast);
                        }
                    }
                    else
                    {
                        return Task.FromResult($"false");
                    }

                }
                else
                {
                    return Task.FromResult($"false");
                }
            }

            return null;
        }

        private static string GetLocationByType(Dto.ViewModel.AirQualityOutput model)
        {
            var locationName = string.Empty;
            if (model.LocationType == LocationType.AreaGroup)
                locationName = model.AreaGroupName;
            else if (model.LocationType == LocationType.City)
                locationName = model.CityName;
            else if (model.LocationType == LocationType.Area)
                locationName = model.AreaName;
            else if (model.LocationType == LocationType.County)
                locationName = model.CountyName;
            else if (model.LocationType == LocationType.State)
                locationName = model.StateName;
            return locationName;
        }

        public Task<string> GetDetailsAsync(string place, string actualdate, string date, string type, string baseApiPath, string AppKey)
        {
            string startDate = DateTime.Today.ToString();
            string endDate = DateTime.Today.ToString();

            if (date != null && date != "AQMD.WeatherDetails")
            {
                string[] inputText = date.Split(",");
                startDate = inputText[0];
                endDate = inputText[1];

            }
            var card = new AdaptiveCard("1.1");
            if (type == "Air Quality")
            {
                var httpClient = new HttpClientApiHelper<Dto.ViewModel.AirQualityOutput>();
                var endPoint = string.Format("/api/v1/airquality/search?location={0}&startDate={1}&endDate={2}&duration=None", System.Net.WebUtility.HtmlEncode(place), startDate, endDate);
                Dto.ViewModel.AirQualityOutput model = httpClient.GetAsync(baseApiPath + endPoint).Result;
                if (model != null)
                {
                    AddCurrentAirQuality(model, card, startDate, endDate);
                    var locationName = GetLocationByType(model);
                    return Task.FromResult($"Major Pollutant  at {locationName} is  {model.Pollutant}. The Ozone levels is {Math.Round(model.Ozone, 2, MidpointRounding.ToEven)} and PM10 is {Math.Round(model.PMTen, 2, MidpointRounding.ToEven)}.");
                }
            }
            else
            {
                double days = ((Convert.ToDateTime(endDate) - DateTime.Now.Date).TotalDays);
                days = days == 1 ? days + 1 : days;
                long number1 = 0;
                WeatherModel model = null;
                bool canConvert = long.TryParse(place, out number1);
                if (canConvert == true)
                {
                    model = new Repository().GetWeatherData(AppKey, GetBy.Zip, place, (Days)days);
                }
                else
                {
                    model = new Repository().GetWeatherData(AppKey, GetBy.CityName, place, (Days)days);
                }


                if (model != null)
                {
                    if (model.current != null)
                    {
                        return Task.FromResult($"The  temperature   at {place} is  {model.current.temperature} degree fahrenheit and  Windspeed is {model.current.wind_speed} in {model.current.wind_dir}.");

                    }

                    if (model.forecast != null && model.forecast.forecastday != null)
                    {
                        if (days == 2 && Convert.ToDateTime(endDate) > DateTime.Now.Date)
                        {
                            foreach (var day in model.forecast.forecastday)
                            {
                                if (DateTime.Parse(day.date).DayOfWeek != DateTime.Parse(model.current.last_updated).DayOfWeek)
                                {

                                    return Task.FromResult($"The max temperature at {place} for tomorrow is  {day.day.maxtemp_f} degree fahrenheit and minimum is {day.day.mintemp_f} degree fahrenheit. indspeed is {day.day.maxwind_kph} in average.");

                                }
                            }
                            AddForecast(place, model, card);
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder($"The temprate at {place} ", 50);
                            foreach (var day in model.forecast.forecastday)
                            {
                                if (DateTime.Parse(day.date).DayOfWeek != DateTime.Parse(model.current.last_updated).DayOfWeek)
                                {
                                    sb.AppendLine($"The max temperature   at {place} for tomorrow is  {day.day.maxtemp_f} degree fahrenheit and minimum is {day.day.mintemp_f} degree fahrenheit. windspeed is {day.day.maxwind_kph} in average.");
                                    break;

                                }
                            }
                            return Task.FromResult(sb.ToString());
                        }


                    }
                }
            }

            return null;
        }

        private static void AddCurrentAirQuality(Dto.ViewModel.AirQualityOutput model, AdaptiveCard card, string startDate, string endDate)
        {

            var headerContainer = new AdaptiveContainer();
            var header = new AdaptiveColumnSet();
            card.Body.Add(headerContainer);
            var headerColumn = new AdaptiveColumn();
            var textHeader = new AdaptiveTextBlock();
            textHeader.Size = AdaptiveTextSize.Medium;
            textHeader.Weight = AdaptiveTextWeight.Bolder;
            textHeader.Text = "AQMD Air Quality";
            headerColumn.Width = AdaptiveColumnWidth.Auto;
            headerColumn.Items.Add(textHeader);
            header.Columns.Add(headerColumn);
            headerContainer.Bleed = true;
            headerContainer.Style = AdaptiveContainerStyle.Default;


            var statusImageColumn = new AdaptiveColumn();
            statusImageColumn.BackgroundImage = new AdaptiveBackgroundImage(GetAQICategoryImage(model));
            statusImageColumn.Width = AdaptiveColumnWidth.Stretch;
            var aqiValue = new AdaptiveTextBlock();
            aqiValue.HorizontalAlignment = AdaptiveHorizontalAlignment.Center;
            aqiValue.Text = string.Format("{0:N1}", model.AQI);
            statusImageColumn.Items.Add(aqiValue);
            header.Columns.Add(statusImageColumn);
            string areaName = GetAreaName(model);
            var zipCode = string.Empty;
            if (!string.IsNullOrEmpty(model.ZipCode))
                zipCode = $"{model.CityName} , {model.StateCode} {model.ZipCode}";
            headerContainer.Items.Add(header);
            var bodyContainer = new AdaptiveContainer();
            var data = new AdaptiveFactSet();
            data.Spacing = AdaptiveSpacing.ExtraLarge;
            if (model.AreaCode > 0)
                data.Facts.Add(new AdaptiveFact() { Title = "Area Number", Value = model.AreaCode.ToString() });
            if (!string.IsNullOrEmpty(areaName))
                data.Facts.Add(new AdaptiveFact() { Title = "Area Name", Value = areaName });
            if (!string.IsNullOrEmpty(model.CountyName))
                data.Facts.Add(new AdaptiveFact() { Title = "County", Value = model.CountyName?.ToString() ?? "N/A" });
            if (!string.IsNullOrEmpty(zipCode))
            {
                data.Facts.Add(new AdaptiveFact() { Title = "ZipCode", Value = zipCode });
            }
            else
            {
                if (!string.IsNullOrEmpty(model.CityName))
                    data.Facts.Add(new AdaptiveFact() { Title = "City", Value = model.CityName?.ToString() ?? "N/A" });
                if (!string.IsNullOrEmpty(model.StateName))
                    data.Facts.Add(new AdaptiveFact() { Title = "State", Value = model.StateName?.ToString() ?? "N/A" });
            }

            data.Facts.Add(new AdaptiveFact() { Title = "AQI  Value", Value = string.Format("{0:N1}", model.AQI) });
            data.Facts.Add(new AdaptiveFact() { Title = "Current Date", Value = DateTime.Now.ToString() });
            data.Facts.Add(new AdaptiveFact() { Title = "Foreccast Date Range", Value = startDate.Substring(0, 10) + " - " + endDate.Substring(0, 10) });
            data.Facts.Add(new AdaptiveFact() { Title = "AQI Category", Value = model.AQICategory?.ToString() ?? "" });
            data.Facts.Add(new AdaptiveFact() { Title = "Pollutants", Value = model.Pollutant?.ToString() ?? "" });
            data.Facts.Add(new AdaptiveFact() { Title = "Ozone Level", Value = string.Format("{0:N1}", model.Ozone) });
            data.Facts.Add(new AdaptiveFact() { Title = "PM 10 Level", Value = string.Format("{0:N1}", model.PMTen) });
            data.Facts.Add(new AdaptiveFact() { Title = "PM 2.5 Level", Value = string.Format("{0:N1}", model.PMTwoFive) });

            bodyContainer.Items.Add(data);
            card.Body.Add(bodyContainer);
        }

        private static string GetAreaName(Dto.ViewModel.AirQualityOutput model)
        {
            var areaName = string.Empty;
            if (string.IsNullOrEmpty(model.AreaGroupName) && !string.IsNullOrEmpty(model.AreaName))
                areaName = model.AreaName;
            if (!string.IsNullOrEmpty(model.AreaGroupName) && string.IsNullOrEmpty(model.AreaName))
                areaName = model.AreaGroupName;
            if (!string.IsNullOrEmpty(model.AreaGroupName) && !string.IsNullOrEmpty(model.AreaName))
                areaName = model.AreaGroupName + " - " + model.AreaName;
            return areaName;
        }

        private static string GetAQICategoryImage(Dto.ViewModel.AirQualityOutput model)
        {
            var imageUrl = AQMD.Dto.Constants.AQICategoryImage.Good;
            if (model.AQICategory == AQMD.Dto.AQICategory.Good.ToString())
                imageUrl = AQMD.Dto.Constants.AQICategoryImage.Good;
            if (model.AQICategory == AQMD.Dto.AQICategory.Moderate.ToString())
                imageUrl = AQMD.Dto.Constants.AQICategoryImage.Moderate;
            if (model.AQICategory == AQMD.Dto.AQICategory.UnhealthyForSensitiveGroup.ToString())
                imageUrl = AQMD.Dto.Constants.AQICategoryImage.UnhealthyForSensitiveGroup;
            if (model.AQICategory == AQMD.Dto.AQICategory.Unhealthy.ToString())
                imageUrl = AQMD.Dto.Constants.AQICategoryImage.Unhealthy;
            if (model.AQICategory == AQMD.Dto.AQICategory.VeryUnhealthy.ToString())
                imageUrl = AQMD.Dto.Constants.AQICategoryImage.VeryUnhealthy;
            if (model.AQICategory == AQMD.Dto.AQICategory.Hazardous.ToString())
                imageUrl = AQMD.Dto.Constants.AQICategoryImage.Hazardous;
            return imageUrl;
        }

        private static void AddCurrentWeather(WeatherModel model, AdaptiveCard card)
        {
            var current = new AdaptiveColumnSet();
            card.Body.Add(current);

            var currentColumn = new AdaptiveColumn();
            current.Columns.Add(currentColumn);
            currentColumn.Width = "35";

            var currentImage = new AdaptiveImage();
            currentColumn.Items.Add(currentImage);
            currentImage.Url = string.IsNullOrWhiteSpace(model.current.condition.icon) ? new Uri("") : new System.Uri(GetIconUrl(model.current.condition.icon));

            var currentColumn2 = new AdaptiveColumn();
            current.Columns.Add(currentColumn2);
            currentColumn2.Width = "65";

            string date = DateTime.Parse(model.current.last_updated).DayOfWeek.ToString();

            AddTextBlock(currentColumn2, $"{model.location.name} ({date})", AdaptiveTextSize.Large, false);
            AddTextBlock(currentColumn2, $"{model.current.temperature.ToString().Split('.')[0]}° F", AdaptiveTextSize.Large);
            AddTextBlock(currentColumn2, $"{model.current.condition.text}", AdaptiveTextSize.Medium);
            AddTextBlock(currentColumn2, $"Winds {model.current.wind_speed} mph {model.current.wind_dir}", AdaptiveTextSize.Medium);
        }
        private static void AddForecast(string place, WeatherModel model, AdaptiveCard card)
        {
            var forecast = new AdaptiveColumnSet();
            card.Body.Add(forecast);

            foreach (var day in model.forecast.forecastday)
            {
                if (DateTime.Parse(day.date).DayOfWeek != DateTime.Parse(model.current.last_updated).DayOfWeek)
                {
                    var column = new AdaptiveColumn();
                    AddForcastColumn(forecast, column, place);
                    AddTextBlock(column, DateTimeOffset.Parse(day.date).DayOfWeek.ToString().Substring(0, 3), AdaptiveTextSize.Medium);
                    AddImageColumn(day, column);
                    AddTextBlock(column, $"{day.day.condition.text}", AdaptiveTextSize.Medium);
                    AddTextBlock(column, $"{day.day.mintemp_f.ToString().Split('.')[0]}/{day.day.maxtemp_f.ToString().Split('.')[0]}", AdaptiveTextSize.Medium);
                }
            }
        }
        private static void AddImageColumn(Forecastday day, AdaptiveColumn column)
        {
            var image = new AdaptiveImage();
            image.Size = AdaptiveImageSize.Auto;
            image.Url = string.IsNullOrWhiteSpace(day.day.condition.icon) ? new Uri("") : new System.Uri(GetIconUrl(day.day.condition.icon));
            column.Items.Add(image);
        }
        private static string GetIconUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return string.Empty;

            if (url.StartsWith("http"))
                return url;
            //some clients do not accept \\
            return "https:" + url;
        }
        private static void AddForcastColumn(AdaptiveColumnSet forecast, AdaptiveColumn column, string place)
        {
            forecast.Columns.Add(column);
            column.Width = "20";
            var action = new AdaptiveOpenUrlAction();
            action.Url = new System.Uri($"https://www.bing.com/search?q=forecast in {place}");
            column.SelectAction = action;
        }

        private static void AddTextBlock(AdaptiveColumn column, string text, AdaptiveTextSize size, bool isSubTitle = true)
        {
            column.Items.Add(new AdaptiveTextBlock()
            {
                Text = text,
                Size = size,
                HorizontalAlignment = AdaptiveHorizontalAlignment.Center,
                IsSubtle = isSubTitle,
                //Separator = Adaptives.None
            });
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AQMD.Common
{
    /// <summary>   A HTTP client API helper. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    public class HttpClientApiHelper<T>
    {

        private readonly string _baseAddress;
        private readonly string _jsonMediaType = "application/json";

        /// <summary>   Default constructor. </summary>
        public HttpClientApiHelper() : this("") { }

        /// <summary>   Constructor. </summary>
        /// <param name="baseAddress">  The base address. </param>
        public HttpClientApiHelper(string baseAddress)
        {
            _baseAddress = !string.IsNullOrEmpty(baseAddress) ? baseAddress : "http://localhost:8080";
        }

        /// <summary>
        /// Used to setup the base address, that we want json, and authentication headers for the request.
        /// </summary>
        /// <param name="client">   . </param>
        private void SetupClient(HttpClient client)
        {
            client.BaseAddress = new Uri(_baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_jsonMediaType));
        }

        /// <summary>   For getting a single item from a web api uaing GET. </summary>
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        /// <param name="apiUrl"> Added to the base address to make the full url of the api get method,
        /// e.g. "products/1" to get a product with an id of 1. </param>
        /// <returns>   The item requested. </returns>
        public async Task<T> GetAsync(string apiUrl)
        {
            T result = default(T);
            using (var client = new HttpClient())
            {
                SetupClient(client);
                var response = await client.GetAsync(apiUrl).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;

                    result = JsonConvert.DeserializeObject<T>(x.Result);
                });
            }
            return result;
        }

        /// <summary>   For getting multiple (or all) items from a web api using GET. </summary>
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        /// <param name="apiUrl"> Added to the base address to make the full url of the api get method,
        /// e.g. "products?page=1" to get page 1 of the products. </param>
        /// <returns>   The items requested. </returns>
        public async Task<IEnumerable<T>> GetManyAsync(string apiUrl)
        {
            IEnumerable<T> result = null;
            using (var client = new HttpClient())
            {
                SetupClient(client);
                var response = await client.GetAsync(apiUrl).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                var settings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                };

                await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;

                    result = JsonConvert.DeserializeObject<IEnumerable<T>>(x.Result, settings);
                });
            }
            return result;
        }

        /// <summary>   For creating a new item over a web api using POST. </summary>
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        /// <param name="apiUrl">     Added to the base address to make the full url of the api post method,
        /// e.g. "products" to add products. </param>
        /// <param name="postObject">   The object to be created. </param>
        /// <returns>   The item created. </returns>
        public async Task<T> PostAsync(string apiUrl, T postObject)
        {
            T result = default(T);
            using (var client = new HttpClient())
            {
                SetupClient(client);
                var response = await client.PostAsync(apiUrl, postObject, new JsonMediaTypeFormatter()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();

                await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;
                    result = JsonConvert.DeserializeObject<T>(x.Result);
                });
            }
            return result;
        }

        /// <summary>   For updating an existing item over a web api using PUT. </summary>
        /// <param name="apiUrl">    Added to the base address to make the full url of the api put method,
        /// e.g. "products/3" to update product with id of 3. </param>
        /// <param name="putObject">    The object to be edited. </param>
        /// <returns>   The asynchronous result. </returns>
        public async Task PutAsync(string apiUrl, T putObject)
        {
            using (var client = new HttpClient())
            {
                SetupClient(client);
                var response = await client.PutAsync(apiUrl, putObject, new JsonMediaTypeFormatter()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
            }
        }

        /// <summary>   For deleting an existing item over a web api using DELETE. </summary>
        /// <param name="apiUrl"> Added to the base address to make the full url of the api delete method,
        /// e.g. "products/3" to delete product with id of 3. </param>
        /// <returns>   The asynchronous result. </returns>
        public async Task DeleteAsync(string apiUrl)
        {
            using (var client = new HttpClient())
            {
                SetupClient(client);
                var response = await client.DeleteAsync(apiUrl).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
            }
        }

        /// <summary>
        ///     To get data by sending paramters as object
        /// </summary>
        /// <typeparam name="TOutput"></typeparam>
        /// <typeparam name="TInput"></typeparam>
        /// <param name="apiUrl"></param>
        /// <param name="dataToPost"></param>
        /// <returns></returns>
        public async Task<TOutput> PostData<TOutput, TInput>(string apiUrl, TInput dataToPost)
        {
            var result = default(TOutput);
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, apiUrl)
            {
                Content = new ObjectContent<TInput>(dataToPost, new JsonMediaTypeFormatter(), (MediaTypeHeaderValue)null)
            };
            // To add header information use the following syntax.
            //var cdaRequestHeaderString = JsonConvert.SerializeObject(CDARequestHeader ?? new Data.DTO.CDARequestHeader());
            //httpRequestMessage.Headers.Add("CDARequestHeader", cdaRequestHeaderString);
            using (var client = new HttpClient())
            {
                SetupClient(client);
                using (HttpResponseMessage response = await client.SendAsync(httpRequestMessage))
                {
                    response.EnsureSuccessStatusCode();
                    await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                    {
                        if (x.IsFaulted)
                            throw x.Exception;
                        result = JsonConvert.DeserializeObject<TOutput>(x.Result);
                    });
                }
            }
            return result;
        }


    }

}

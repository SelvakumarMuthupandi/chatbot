using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIXULib
{
    public class Location
    {
        public string name { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string timezone_id { get; set; }
        public int localtime_epoch { get; set; }
        public string localtime { get; set; }
    }

    public class Condition
    {
        public string text { get; set; }
        public string icon { get; set; }
        public int code { get; set; }
    }

    public class Current
    {
        //public int last_updated_epoch { get; set; }
        public string last_updated { get; set; }
        public double temperature { get; set; }
        public int weather_code { get; set; }
        //public double temp_f { get; set; }
        // public Condition condition { get; set; }

        public string[] weather_icons { get; set; }
        public string[] weather_descriptions { get; set; }
        // public double wind_mph { get; set; }
        public double wind_speed { get; set; }
        public int wind_degree { get; set; }
        public string wind_dir { get; set; }
        // public double pressure_mb { get; set; }
        public double pressure { get; set; }
        // public double precip_mm { get; set; }
        public double precip { get; set; }
        public int humidity { get; set; }
        public int cloudcover { get; set; }
        //public double feelslike_c { get; set; }
        public double feelslike { get; set; }
        public Condition condition { get; set; }
    }



    public class Day
    {
        public double maxtemp_c { get; set; }
        public double maxtemp_f { get; set; }
        public double mintemp_c { get; set; }
        public double mintemp_f { get; set; }
        public double avgtemp_c { get; set; }
        public double avgtemp_f { get; set; }
        public double maxwind_mph { get; set; }
        public double maxwind_kph { get; set; }
        public double totalprecip_mm { get; set; }
        public double totalprecip_in { get; set; }
        public Condition condition { get; set; }
    }

    public class astro
    {
        public string sunrise { get; set; }
        public string sunset { get; set; }
        public string moonrise { get; set; }
        public string moonset { get; set; }
        public string moon_phase { get; set; }
        public string moon_illumination { get; set; }
    }



    public class Hour
    {
        public int time_epoch { get; set; }
        public string time { get; set; }
        public double temp_c { get; set; }
        public double temp_f { get; set; }
        public Condition condition { get; set; }
        public double wind_mph { get; set; }
        public double wind_kph { get; set; }
        public int wind_degree { get; set; }
        public string wind_dir { get; set; }
        public double pressure_mb { get; set; }
        public double pressure_in { get; set; }
        public double precip_mm { get; set; }
        public double precip_in { get; set; }
        public int humidity { get; set; }
        public int cloud { get; set; }
        public double feelslike_c { get; set; }
        public double feelslike_f { get; set; }
        public double windchill_c { get; set; }
        public double windchill_f { get; set; }
        public double heatindex_c { get; set; }
        public double heatindex_f { get; set; }
        public double dewpoint_c { get; set; }
        public double dewpoint_f { get; set; }
        public int will_it_rain { get; set; }
        public int will_it_snow { get; set; }
    }
    public class hourly
    {
        public string[] weather_icons { get; set; }
        public string[] weather_descriptions { get; set; }

    }

    public class Forecastday
    {
        public string date { get; set; }
        public int date_epoch { get; set; }
        public Day day { get; set; }
        public astro astro { get; set; }
        public List<Hour> hour { get; set; }
    }
    //public class Historicalday
    //{
    //    [JsonProperty(PropertyName = "1. open")]
    //    public double open { get; set; }
    //    [JsonProperty(PropertyName = "2. high")]
    //    public double high { get; set; }
    //    [JsonProperty(PropertyName = "3. low")]
    //    public double low { get; set; }
    //    [JsonProperty(PropertyName = "4. close")]
    //    public double close { get; set; }
    //    [JsonProperty(PropertyName = "5. volume")]
    //    public double volume { get; set; }
    //}
    public class Historicalday
    {
        public string date { get; set; }
        public int date_epoch { get; set; }
        public int mintemp { get; set; }
        public int maxtemp { get; set; }
        public int avgtemp { get; set; }

        public int totalsnow { get; set; }
        public double sunhour { get; set; }
        public int uv_index { get; set; }
        public astro astro { get; set; }
        public hourly[] hourly { get; set; }

    }

    public class Forecast
    {
        public List<Forecastday> forecastday { get; set; }
    }

    //public class Historical
    //{
    //    [JsonProperty(PropertyName = "Historicalday")] // You will need to change 15min to the interval you will use
    //    public Dictionary<string, Historicalday> Data { get; set; }
    //}

    public class WeatherModel
    {
        public Location location { get; set; }
        public Current current { get; set; }
        public Forecast forecast { get; set; }

        [JsonProperty(PropertyName = "Historical")] // You will need to change 15min to the interval you will use
        public Dictionary<string, Historicalday> Historical { get; set; }
    }
}

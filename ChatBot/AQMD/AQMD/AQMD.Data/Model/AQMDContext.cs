using AQMD.Dto;
using Microsoft.EntityFrameworkCore;
using System;

namespace AQMD.Data.Model
{
    public class AQMDContext : DbContext
    {
        public DbSet<AirQuality> AirQuality { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<County> County { get; set; }

        public AQMDContext(DbContextOptions options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //// configures one-to-many relationship
            //modelBuilder.Entity<City>()
            //    .HasOne<City,State>("StateID")
            //    .WithMany(g => g.Cities)
            //    .HasForeignKey<int>(s => s.StateId);
            base.OnModelCreating(modelBuilder);

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AQMD.Data.Commands;
using AQMD.Data.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AQMD.Data.Controllers
{
    public class WeatherController : Controller
    {
        private readonly IQueriesService _queries;
        private readonly ICommandService _commands;
        private readonly IConfiguration _settings;

        public WeatherController(IQueriesService queries, ICommandService commands, IConfiguration settings)
        {
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _commands = commands ?? throw new ArgumentNullException(nameof(commands));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
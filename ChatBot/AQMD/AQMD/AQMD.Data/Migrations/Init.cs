using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AQMD.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //return;

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    StateId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StateCode = table.Column<string>(nullable:false),
                    StateName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.StateId);
                });
            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    AreaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AreaNumber = table.Column<int>(nullable: false),
                    AreaName = table.Column<string>(nullable: false),
                    StateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.AreaId);
                    table.ForeignKey("FK_Area_State_StateId", x => x.StateId, "State", "StateId", onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "County",
                columns: table => new
                {
                    CountyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CountyName = table.Column<string>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    AreaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_County", x => x.CountyId);
                    table.ForeignKey("FK_County_State_StateId", x => x.StateId, "State", "StateId", onDelete: ReferentialAction.Cascade);
                    table.ForeignKey("FK_County_Area_AreaId", x => x.AreaId, "Area", "AreaId", onDelete: ReferentialAction.NoAction);
                });


            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    CityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CityName = table.Column<string>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    AreaId = table.Column<int>(nullable: false),
                    CountyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.CityId);
                    table.ForeignKey("FK_City_State_StateId", x => x.StateId, "State", "StateId", onDelete: ReferentialAction.Cascade);
                    table.ForeignKey("FK_City_Area_AreaId", x => x.AreaId, "Area", "AreaId", onDelete: ReferentialAction.NoAction);
                    table.ForeignKey("FK_City_County_CountyId", x => x.CountyId, "County", "CountyId", onDelete: ReferentialAction.NoAction);
                });


            migrationBuilder.CreateTable(
                name: "AirQuality",
                columns: table => new
                {
                    AirQualityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AreaId = table.Column<int>(nullable: false),
                    AQI = table.Column<double>(nullable: true),
                    ForecastDateTime = table.Column<DateTime>(nullable: false, defaultValue: DateTime.UtcNow),
                    AQICategory = table.Column<string>(nullable: false, defaultValue: "Good"),
                    Pollutant = table.Column<string>(nullable: true),
                    PMTwoFive = table.Column<double>(nullable: true),
                    PMTen = table.Column<double>(nullable: true),
                    Ozone = table.Column<double>(nullable: true),
                    NitorgenDiOxide = table.Column<double>(nullable: true),
                    CarbonMonoOxide = table.Column<double>(nullable: true),
                    SulfurDiOxide = table.Column<double>(nullable: true),
                    ZipCode = table.Column<string>(nullable: false),
                    CityId = table.Column<int>(nullable: false),
                    CountyId = table.Column<int>(nullable: false),
                    StateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AirQuality", x => x.AirQualityId);
                    table.ForeignKey("FK_AirQuality_State_StateId", x => x.StateId, "State", "StateId", onDelete: ReferentialAction.NoAction);
                    table.ForeignKey("FK_AirQuality_Area_AreaId", x => x.AreaId, "Area", "AreaId", onDelete: ReferentialAction.NoAction);
                    table.ForeignKey("FK_AirQuality_County_CountyId", x => x.CountyId, "County", "CountyId", onDelete: ReferentialAction.NoAction);
                    table.ForeignKey("FK_AirQuality_City_CityId", x => x.CityId, "City", "CityId", onDelete: ReferentialAction.NoAction);

                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AirQuality");
            migrationBuilder.DropTable(
                name: "City");
            migrationBuilder.DropTable(
                name: "Area");
            migrationBuilder.DropTable(
                name: "County");
            migrationBuilder.DropTable(
                name: "State");

        }
    }
}

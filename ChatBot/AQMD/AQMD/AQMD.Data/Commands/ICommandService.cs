using AQMD.Data.Controllers;
using AQMD.Data.Model;
using AQMD.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace AQMD.Data.Commands
{
    public interface ICommandService
    {
        Task<AirQuality> SaveAirQualityAsync(AirQuality airQualityDto);
    }
}

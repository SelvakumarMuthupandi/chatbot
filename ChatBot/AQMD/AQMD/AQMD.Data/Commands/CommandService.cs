using AQMD.Data.Controllers;
using AQMD.Data.Model;
using AQMD.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AQMD.Data.Commands
{
    public class CommandService : ICommandService
    {
        private readonly AQMDContext _context;

        public CommandService(AQMDContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<AirQuality> SaveAirQualityAsync(AirQuality  airQualityDto)
        {
            await _context.AirQuality.AddAsync(airQualityDto);
            await _context.SaveChangesAsync();

            return airQualityDto;
        }
    }
}

using AQMD.Data.Model;
using AQMD.Dto;
using AQMD.Dto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AQMD.Data.Queries
{
    public interface IQueriesService
    {
        Task<AirQualityOutput> GetAirQuality(string location, DateTime startDate, DateTime endDate,string duration);
        Task<IEnumerable<AirQualityOutput>> GetAirQualityByZipCodeAsync(string zipCode, DateTime startDate, DateTime endDate, string duration);
        Task<IEnumerable<AirQualityOutput>> GetAirQualityByCityAsync(string city, DateTime startDate, DateTime endDate, string duration);
        Task<IEnumerable<AirQualityOutput>> GetAirQualityByAreaAsync(string areaName, DateTime startDate, DateTime endDate, string duration);
        Task<IEnumerable<AirQualityOutput>> GetAirQualityByCountyAsync(string county, DateTime startDate, DateTime endDate, string duration);

        Task<IEnumerable<AirQualityOutput>> GetAirQualityByStateAsync(string state, DateTime startDate, DateTime endDate, string duration);
    }
}

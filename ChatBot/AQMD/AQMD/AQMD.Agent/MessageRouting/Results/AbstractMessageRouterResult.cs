using System;

namespace AQMD.Agent.MessageRouting.Results
{
    [Serializable]
    public abstract class AbstractMessageRouterResult
    {
        public string ErrorMessage
        {
            get;
            set;
        }

        public AbstractMessageRouterResult()
        {
            ErrorMessage = string.Empty;
        }

        public abstract string ToJson();
    }
}

using Microsoft.WindowsAzure.Storage.Table;

namespace AQMD.Agent.MessageRouting.Models.Azure
{
    public class RoutingDataEntity : TableEntity
    {
        public string Body { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AQMD.Dto
{
    public class State
    {
        public int StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }

    }
}

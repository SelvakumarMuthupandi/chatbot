﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AQMD.Dto
{
    public class City
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int AreaId { get; set; }
        public int StateId { get; set; }
        public int CountyId { get; set; }

    }
}

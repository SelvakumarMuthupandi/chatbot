﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AQMD.Dto
{
    public class County
    {
        public int CountyId { get; set; }
        public string CountyName { get; set; }

        public int AreaId { get; set; }
        public int StateId { get; set; }

    }
}

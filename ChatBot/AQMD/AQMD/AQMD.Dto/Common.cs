﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AQMD.Dto
{
    public enum AQICategory
    {
        Good,
        Moderate,
        UnhealthyForSensitiveGroup,
        Unhealthy,
        VeryUnhealthy,
        Hazardous
    }

    public enum LocationType
    {
        City,
        Area,
        AreaGroup,
        State,
        County,
        ZipCode
    }

    public enum Duration
    {
        Hour,
        Day,
        None,

    }

    public static class Constants
    {
        public static class PollutantDetails
        {
            public const string Ozone = "Ozone(O3) - 8 Hr Average";
            public const string PM10 = "Fine Particle Matters (PM10) - 24 Hr Average";
            public const string PM25 = "Fine Particle Matters(PM2.5) - 24 Hr Average";
            public const string CO = "Carbon Mono Oxide (CO) - 8 Hr Average";
            public const string No2 = "Nitrogen Di Oxide (NO2) - 8 Hr Average";

        }

        public static class AQICategoryImage
        {
            public const string Good = "http://demo.preludesys.com/AQMD/images/6.png";
            public const string Moderate = "http://demo.preludesys.com/AQMD/images/5.png";
            public const string UnhealthyForSensitiveGroup = "http://demo.preludesys.com/AQMD/images/4.png";
            public const string Unhealthy = "http://demo.preludesys.com/AQMD/images/3.png";
            public const string VeryUnhealthy = "http://demo.preludesys.com/AQMD/images/2.png";
            public const string Hazardous = "http://demo.preludesys.com/AQMD/images/1.png";
        }

        public static class Description
        {
            public const string Good = "Air quality is considered satisfactory, and air pollution poses little or no risk.";
            public const string Moderate = "Air quality is acceptable; however, for some pollutants there may be a moderate health concern for a very small number of people";
            public const string UnhealthyForSensitiveGroup = "Members of sensitive groups may experience health effects. This means they are likely to be affected at lower levels than the general public.";
            public const string Unhealthy = "Everyone may begin to experience health effects when AQI values are between 151 and 200. Members of sensitive groups may experience more serious health effects.";
            public const string VeryUnhealthy = "Very Unhealthy";
            public const string Hazardous = "Hazardous";

        }

        public static class PollutantThresold
        {
            public const int Good = 50;
            public const int Moderate = 100;
            public const int UnhealthyForSensitiveGroup = 150;
            public const int Unhealthy = 200;
            public const int VeryUnhealthy = 300;
            public const int Hazardous = 300;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AQMD.Dto
{
    public class Area
    {
        public int AreaId { get; set; }
        public int AreaNumber { get; set; }
        public string AreaName { get; set; }
        public int StateId { get; set; }
        public int AreaGroupId { get; set; }


    }
}

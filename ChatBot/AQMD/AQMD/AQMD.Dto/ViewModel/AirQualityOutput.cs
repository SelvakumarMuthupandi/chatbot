﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AQMD.Dto.ViewModel
{
    public class AirQualityOutput
    {
        public int AreaCode { get; set; }
        public string AreaName { get; set; }
        public double AQI { get; set; }
        public DateTime ForecastDateTime { get; set; }
        public string AQICategory { get; set; }
        public string Pollutant { get; set; }
        public double PMTwoFive { get; set; }
        public double PMTen { get; set; }
        public double Ozone { get; set; }
        public double NitorgenDiOxide { get; set; }
        public double CarbonMonoOxide { get; set; }
        public double SulfurDiOxide { get; set; }
        public string ZipCode { get; set; }

        public string CityName { get; set; }
        public string CountyName { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string AreaGroupName { get; set; }
        public string Description { get; set; }

        public int CityId { get; set; }
        public int AreaId { get; set; }
        public int CountyId { get; set; }
        public int StateId { get; set; }
        public int AreaGroupId { get; set; }
        public LocationType LocationType { get; set; }

    }
}

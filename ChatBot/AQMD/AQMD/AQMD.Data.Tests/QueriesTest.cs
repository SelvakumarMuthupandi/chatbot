﻿using AQMD.Data.Commands;
using AQMD.Data.Model;
using AQMD.Data.Queries;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Xunit;
using AQMD.Dto;
using System.Linq;

namespace AQMD.Data.Tests
{
    public class QueriesTest
    {
        [Fact]
        public async Task TestGetAirQuality()
        {
            //Assemble

            var options = new DbContextOptionsBuilder<AQMDContext>()
                .UseInMemoryDatabase(databaseName: "mem-test")
                .Options;

            AirQuality weather;
            var forcastDateTime = DateTime.UtcNow.AddDays(1);
            using (var context = new AQMDContext(options))
            {
                var commands = new CommandService(context);
                //act
                weather = await commands.SaveAirQualityAsync(new AirQuality()
                {
                    AreaId = 20,
                    AQI = 20,
                    PMTwoFive = 20,
                    Ozone = 20,
                    AQICategory = "Good",
                    ForecastDateTime = forcastDateTime
                });
            }
            using (var context = new AQMDContext(options))
            {
                var connectionString = context.Database.GetDbConnection().ConnectionString;
                //var connectionString = "Server =.\\server2012; Initial Catalog = AQMDWeather; Persist Security Info = False; User ID = PreludeSys; Password = Prelude@123; MultipleActiveResultSets = False; Connection Timeout = 30;";
                var queryService = new QueriesService(connectionString);

                var airQualityResult = await queryService.GetAirQualityByAreaAsync("test", forcastDateTime, forcastDateTime.AddDays(1),Dto.Duration.Day.ToString());
                var airQuality = airQualityResult.First();
                //Assert
                Assert.True(airQuality.AQI == 20);
            }

        }

    }
}

// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using AQMD.Common;
using Bot.Builder.Community.Adapters.Alexa;
using AQMD.CognitiveModels;
using AdaptiveCards;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace AQMD.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        private readonly IConfiguration configuration;
        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;
        private const string HelpMsgText = "Please drop an email to webinquiry@aqmd.gov or reach out to us on 909 3962 000.";
        int count = 0;

        // Dependency injection uses this constructor to instantiate MainDialog
        public MainDialog(WeatherRecognizer luisRecognizer, WeatherDialog weatherDialog, ILogger<MainDialog> logger)
            : base(nameof(MainDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(weatherDialog);
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                IntroStepAsync,
                ActStepAsync,
                FinalStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> IntroStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("LUIS or QnA is not configured.", inputHint: InputHints.IgnoringInput), cancellationToken);
                Logger.LogInformation("LUIS or QnA is not configured.Update the Luis and QNA setting in Appsetting.json");
                return await stepContext.NextAsync(null, cancellationToken);
            }
            if (stepContext.Context.Activity.ChannelId != "alexa")
            {
                // Use the text provided in FinalStepAsync or the default if it is the first time.
                var messageText = stepContext.Options?.ToString() ?? "How can I help you today?\n To know air quality, type  \"How is air quality in Diamond Bar?\"";
                var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            }
            return await stepContext.NextAsync(null, cancellationToken);
        }

        private async Task<DialogTurnResult> ActStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                // LUIS is not configured, we just run the WeatherDialog path with an empty BookingDetailsInstance.
                return await stepContext.BeginDialogAsync(nameof(WeatherDialog), new WeatherDetails(), cancellationToken);
            }
            // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
            var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(stepContext.Context, cancellationToken);
            switch (luisResult.TopIntent().intent)
            {
                case WeatherInfo.Intent.I_CustomerService:
                    var customerServiceHelpTest = "All our customer service executives are busy, please drop an email to webinquiry@aqmd.gov or reach out to us on 909 3962 000.";
                    var customerServiceMessage = MessageFactory.Text(customerServiceHelpTest, customerServiceHelpTest, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = customerServiceMessage }, cancellationToken);

                case WeatherInfo.Intent.l_Weather:
                    count = 0;
                    var weatherDetails = new WeatherDetails()
                    {
                        // Get Location and Date from the composite entities arrays.
                        Location = !string.IsNullOrEmpty(luisResult.Location) ? luisResult.Location : "Diamond Bar",
                        ActualDate = luisResult.Date,
                        Type = "Weather"
                    };
                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(WeatherDialog), weatherDetails, cancellationToken);
                case WeatherInfo.Intent.l_AirQuality:
                    count = 0;
                    var airQuality = new WeatherDetails()
                    {
                        // Get Location and Date from the composite entities arrays.
                        Location = !string.IsNullOrEmpty(luisResult.Location) ? luisResult.Location : "Diamond Bar",
                        ActualDate = luisResult.Date,
                        Type = "Air Quality"
                    };
                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(WeatherDialog), airQuality, cancellationToken);
                case WeatherInfo.Intent.q_qna:
                    if (stepContext.Context.Activity.ChannelId == "alexa")
                        return await ProcessSampleQnAForAlexaAsync(stepContext, cancellationToken, count);
                    else
                        await ProcessSampleQnAAsync(stepContext, cancellationToken,count);
                    break;
                default:
                    count++;
                    if (stepContext.Context.Activity.ChannelId == "alexa")
                    {
                        if (count > 3)
                        {
                            var help = "I can't understand you .Please drop an email to webinquiry@aqmd.gov or reach out to us on 9 0 9 3 9 6 2 0 0 0.";
                            var message = MessageFactory.Text(help, help, InputHints.ExpectingInput);
                            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                        }
                        else
                        {
                            Logger.LogInformation($"Unrecognized intent: {luisResult.TopIntent().intent}.");
                            var defaultMsg = $"Sorry, I didn't get that. Please try asking in a different way. For Example Ask air quality in Diamond Bar.";
                            var message = MessageFactory.Text(defaultMsg, defaultMsg, InputHints.ExpectingInput);
                            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                        }
                    }
                    else
                    {
                        if (count > 3)
                        {
                            await stepContext.Context.SendActivityAsync(MessageFactory.Text($"I can't understand you. Please drop an email to webinquiry@aqmd.gov or reach out to us on 909-3962-000."), cancellationToken);
                        }
                        else
                        { 
                            await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Sorry, I didn't get that. Please try asking in a different way. For Example Ask air quality in Diamond Bar."), cancellationToken);
                        }
                    }
                    break;
            }

            return await stepContext.NextAsync(null, cancellationToken);
        }
        private async Task ProcessSampleQnAAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken,int count)
        {
            Logger.LogInformation("ProcessSampleQnAAsync");
            var results = await _luisRecognizer.SampleQnA.GetAnswersAsync(stepContext.Context);
            if (results.Any())
            {
                switch (results.First().Answer.ToLower())
                {
                    case "help":
                        count = 0;
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text(HelpMsgText), cancellationToken);
                        new DialogTurnResult(DialogTurnStatus.Waiting);
                        break;
                    case "cancel":
                        count = 0;
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text("Cancelling."), cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        break;
                    case "quit":
                        count = 0;
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text("Thank you for using AQMD Bot. Please share your feedback at webquery@aqmd.gov"), cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        break;
                    default:
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text(results.First().Answer), cancellationToken);
                        break;
                }

            }
            else
            {
                if (count > 2)
                {
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("I can't understand you. Please drop an email to webinquiry@aqmd.gov or reach out to us on 909-3962-000."), cancellationToken);
                }
                else
                {
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("Sorry, could not find an answer in the Q and A system."), cancellationToken);
                }
            }
        }
        private async Task<DialogTurnResult> ProcessSampleQnAForAlexaAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken , int count )
        {
            Logger.LogInformation("ProcessSampleQnAForAlexaAsync");

            var results = await _luisRecognizer.SampleQnA.GetAnswersAsync(stepContext.Context);
            if (results.Any())
            {
                switch (results.First().Answer.ToLower())
                {
                    case "help":
                        count = 0;
                        var promptMessage = MessageFactory.Text(HelpMsgText, HelpMsgText, InputHints.ExpectingInput);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
                    default:
                        count = 0;
                        var defaultMessage = results.First().Answer;
                        var message = MessageFactory.Text(defaultMessage, defaultMessage, InputHints.ExpectingInput);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                }
            }
            else
            {
                if (count > 2)
                {
                    var cancelMessage = "I can't understand you. Please drop an email to webinquiry@aqmd.gov or reach out to us on 9 0 9 3 9 6 2 0 0 0.";
                    var quitMessage = MessageFactory.Text(cancelMessage, cancelMessage, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = quitMessage }, cancellationToken);
               
                }
                else
                {
                    var cancelMessage = "Could not find answer for your questions in our QnA system.";
                    var quitMessage = MessageFactory.Text(cancelMessage, cancelMessage, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = quitMessage }, cancellationToken);
                }
            }
        }

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Restart the main dialog with a different message the second time around
            //var promptMessage = "Is there anything else I can help you with?";
            var promptMessage = string.Empty;
            return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage, cancellationToken);
        }
    }
}

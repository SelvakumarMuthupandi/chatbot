﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using AQMD;
using AQMD.Dialogs;
using Bot.Builder.Community.Adapters.Alexa.Integration.AspNet.Core;
using Bot.Builder.Community.Adapters.Alexa.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.BotFramework;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Connector.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging;

namespace Microsoft.BotBuilderSamples
{
    /// <summary>
    /// The Startup class configures services and the request pipeline.
    /// </summary>
    public class Startup
    {
        private ILogger<DialogAndWelcomeBot<MainDialog>> _logger;
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Create the Bot Framework Adapter with error handling enabled.
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            // Create the storage we'll be using for User and Conversation state. (Memory is great for testing purposes.)
            services.AddSingleton<IStorage, MemoryStorage>();

            // Create the User state. (Used in this bot's Dialog implementation.)
            services.AddSingleton<UserState>();

            // Create the Conversation state. (Used by the Dialog system itself.)
            services.AddSingleton<ConversationState>();

            // Register LUIS recognizer
            services.AddSingleton<WeatherRecognizer>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            //services.AddTransient<IBot, DialogAndWelcomeBot<MainDialog>>();
            services.AddBot<DialogAndWelcomeBot<MainDialog>>(options =>
            {
                options.Middleware.Add(new ShowTypingMiddleware(100,1000));
            });
            // Register the BookingDialog.
            services.AddSingleton<WeatherDialog>();

            // The Dialog that will be run by the bot.
            services.AddSingleton<MainDialog>();
            

            services.AddAlexaBot<DialogAndWelcomeBot<MainDialog>>(options =>
            {
                // Set this to true to validate that a request has come from the Alexa
                // service - this is a requirement for skill certification
                // disable this if you want to debug using other tools like Postman.
                options.AlexaOptions.ValidateIncomingAlexaRequests = false;

                // Determine if we should end a session after each turn
                // If set to true, you can choose to keep the session open
                // by using the ExpectingInput InputHint in your outgoing activity
                options.AlexaOptions.ShouldEndSessionByDefault = false;
                // Catches any errors that occur during a conversation turn and logs them.
                options.AlexaOptions.OnTurnError = async (context, exception) =>
                {
                    _logger.LogError($"Exception caught : {exception}");
                    await context.SendActivityAsync("Sorry, it looks like something went wrong.");
                };

                // This middleware will look for a known slot called 'Phrase'
                // and transform an incoming IntentRequest with this slot into 
                // a MessageActivity, using the value of the Phrase slot as the 
                // Text property for the activity. See the readme for more details
                // on configuring your Alexa skill for this.
                options.Middleware.Add(new AlexaIntentRequestToMessageActivityMiddleware());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
            app.UseAlexa();
        }
    }
}

import React from "react";
import $ from "jquery";
import ReactTooltip from "react-tooltip";
import { createStore, createStyleSet } from "botframework-webchat";

import WebChat from "./WebChat";

import "./fabric-icons-inline.css";
import "./MinimizableWebChat.css";
import sideMenu from "./menu.png";
import maximizeButton from "./icon2.svg";
import chatLogo from "./chatlogo.png";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.handleFetchToken = this.handleFetchToken.bind(this);
    this.handleMaximizeButtonClick = this.handleMaximizeButtonClick.bind(this);
    this.handleMinimizeButtonClick = this.handleMinimizeButtonClick.bind(this);
    this.handleSwitchButtonClick = this.handleSwitchButtonClick.bind(this);
    this.handleExpandMenuClick = this.handleExpandMenuClick.bind(this);
    this.handleAgentButtonClick = this.handleAgentButtonClick.bind(this);
    this.handleScheduleButtonClick = this.handleScheduleButtonClick.bind(this);

    const store = createStore({}, ({ dispatch }) => next => action => {
      if (action.type === "DIRECT_LINE/CONNECT_FULFILLED") {
        setTimeout(() => {
          dispatch({
            type: "WEB_CHAT/SEND_EVENT",
            payload: {
              name: "webchat/join",
              value: {
                language: window.navigator.language
              }
            }
          });
        }, 1000);
      } else if (action.type === "DIRECT_LINE/INCOMING_ACTIVITY") {
        if (action.payload.activity.from.role === "bot") {
          this.setState(() => ({ newMessage: true }));
        }
      }
      return next(action);
    });

    this.state = {
      minimized: true,
      newMessage: false,
      userInitials: "You",
      side: "right",
      store,
      styleSet: createStyleSet({
        backgroundColor: "Transparent",
        bubbleBackground: "rgba(0, 0, 255, .1)",
        bubbleFromUserBackground: "rgba(0, 255, 0, .1)",
        botAvatarInitials: "A"
      }),
      token: null
    };
  }

  async handleFetchToken() {
    if (!this.state.token) {
      const token = "cdQuqDMl1BU.qppdhk_ERTpdg3G6qyOabcujw6d0eyzzQWw1U7qoTbo";
      this.setState(() => ({ token }));
    }
  }

  handleMaximizeButtonClick() {
    this.setState(() => ({
      minimized: false,
      newMessage: false
    }));
  }

  handleMinimizeButtonClick() {
    this.setState(() => ({
      minimized: true,
      newMessage: false
    }));
  }

  handleSwitchButtonClick() {
    this.setState(({ side }) => ({
      side: side === "left" ? "right" : "left"
    }));
  }

  handleExpandMenuClick() {
    $(".wc-call-wrapper").slideToggle(500);
    $(".wc-console").toggleClass("active");
  }

  handleAgentButtonClick() {
    this.state.store.dispatch({
      type: "WEB_CHAT/SEND_MESSAGE",
      payload: { text: "customer service" }
    });
  }

  handleScheduleButtonClick() {
    this.state.store.dispatch({
      type: "WEB_CHAT/SEND_MESSAGE",
      payload: { text: "schedule meeting" }
    });
  }

  render() {
    const {
      state: { minimized, newMessage, side, store, styleSet, token }
    } = this;

    return (
      <div className="minimizable-web-chat">
        {minimized ? (
          <button className="maximize" onClick={this.handleMaximizeButtonClick}>
            <img alt="Maximize" src={maximizeButton} />
          </button>
        ) : (
          <div className={side === "left" ? "chat-box left" : "chat-box right"}>
            <header>
              <div className="chat-title">
                <img alt="PreludeSys" src={chatLogo} /> 
              </div>
              <button
                className="wc-menu"
                data-tip="Customer Service"
                onClick={this.handleAgentButtonClick}
              >
                <img
                  data-tip="Customer Service"
                  alt=""
                  src={sideMenu}
                  className="menu-svg"
                />
              </button>

              <div className="filler" />
              <button
                className="switch"
                data-tip="switch"
                onClick={this.handleSwitchButtonClick}
              >
                <span className="ms-Icon ms-Icon--Switch" />
              </button>
              <button
                className="minimize"
                data-tip="minimize"
                onClick={this.handleMinimizeButtonClick}
              >
                <span className="ms-Icon ms-Icon--ChromeMinimize" />
              </button>
              <ReactTooltip type="light" />
            </header>
            <WebChat
              className="react-web-chat"
              onFetchToken={this.handleFetchToken}
              store={store}
              styleSet={styleSet}
              token={token}
            />
          </div>
        )}
      </div>
    );
  }
}

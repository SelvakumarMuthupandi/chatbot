using System.Reflection;
using PreludeSys.Api.AirQuality.Commands;
using PreludeSys.Api.AirQuality.Model;
using PreludeSys.Api.AirQuality.Queries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using PreludeSys.Interface;

namespace PreludeSys.Api.AirQuality
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped< IAirQualityQueries , AirQualityQueries>(ctor => new AirQualityQueries(Configuration["ConnectionString"]));
            services.AddScoped<ILeadPredictionReportQueries, LeadPredictionReportQueries>(ctor => new LeadPredictionReportQueries(Configuration["ConnectionString"]));

            services.AddScoped<IAirQualityCommand, AirQualityCommand>();


           

            services.AddDbContext<LeadReportDBContext>(optionsAction: optionsBuilder =>
                optionsBuilder.UseSqlServer(Configuration["ConnectionString"],
                optionsAction => optionsAction.MigrationsAssembly(typeof(LeadReportDBContext).GetTypeInfo().Assembly.GetName().Name)));


            services.AddDbContext<PreludeSysContext>(optionsAction: optionsBuilder =>
               optionsBuilder.UseSqlServer(Configuration["ConnectionString"],
               optionsAction => optionsAction.MigrationsAssembly(typeof(PreludeSysContext).GetTypeInfo().Assembly.GetName().Name)));

            //services.AddDbContext<LocalDBContext>(optionsAction: optionsBuilder =>
            // optionsBuilder.UseSqlServer(Configuration["DefaultdbConnection"],
            // optionsAction => optionsAction.MigrationsAssembly(typeof(LocalDBContext).GetTypeInfo().Assembly.GetName().Name)));


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "PreludeSys Bot API",
                    Description = "PreludeSys Bot API",
                    TermsOfService = "None",
                    Contact = new Contact()
                    {
                        Name = "PreludeSys",
                        Email = "info@PreludeSys.com",
                        Url = "https://PreludeSys.com/"
                    }
                });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            //using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            //{
            //    var context = serviceScope.ServiceProvider.GetRequiredService<PreludeSysContext>();
            //    context.Database.Migrate();
            //}

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PreludeSys API V1");
            });

            app.UseMvc();

        }
    }
}

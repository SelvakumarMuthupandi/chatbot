﻿using PreludeSys.Dto;
using PreludeSys.Dto.ViewModel;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using PreludeSys.Interface;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace PreludeSys.Api.AirQuality.Queries
{
    public class LeadPredictionReportQueries : ILeadPredictionReportQueries
    {
        private static readonly IConfigurationRoot _iconfiguration;
        private string _connectionString;
        public object listLeadPredictModal;

        public IEnumerable<LeadPredictionOutPut> LReport { get; private set; }

        // public IEnumerable<LeadPredictionOutPut> LReport { get; private set; }

        public LeadPredictionReportQueries(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("message", nameof(connectionString));
            }
            _connectionString = connectionString;
            
        }


       
        public async Task<IEnumerable<LeadPredictionOutPut>> GetLeadInfo(int count_of_Records_Fetched, DateTime startDate, DateTime endDate, double annualRevenueRange, string status)
        {
            return await GetReport(count_of_Records_Fetched,startDate,endDate, annualRevenueRange, status);
        }

        public async Task<IEnumerable<LeadSourceOutPut>> GetBestLeadSourceInfo()
        {
            return await GetLeadSourceReport();
        }


        private async Task<IEnumerable<LeadSourceOutPut>> GetLeadSourceReport()
        {

            //string sqlString = $"select top '{Count_of_Records_Fetched}' Company, AnnualRevenue,Force#comStatus,CreatedbyRep,CreatedDate from LeadConversion where CreatedDate = '{createdDate}'";
            string sqlstr = "select top 5 leadscount,validationlink,SuccessLeads,InTop3,Highauthorities,validphonecount,validemailcount,phonevalidpercent,emailvalidpercent from vwBestLeadSource order by leadscount desc";



            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var Lead = await conn.QueryAsync<LeadSourceOutPut>(sqlstr);
                return Lead;
            }
        }
        //private async Task<IEnumerable<LeadPredictionOutPut>> GetReport( int maxValue, int Count_of_Records_Fetched)
        //{

        //    //string sqlString = $"select top '{Count_of_Records_Fetched}' Company, AnnualRevenue,Force#comStatus,CreatedbyRep,CreatedDate from LeadConversion where CreatedDate = '{createdDate}'";
        //    string sqlstr = $"SELECT top {Count_of_Records_Fetched} Company,sum(AnnualRevenue)as AnnualRevenue,CreatedbyRep,ForceStatus as status FROM LeadConversion  group by Company,CreatedDate,CreatedbyRep,ForceStatus order by AnnualRevenue desc;";
        //    sqlstr = string.Format("select distinct top {0}", Count_of_Records_Fetched);


        //    sqlstr = sqlstr + @"  company, AnnualRevenue,forcestatus,
        //        STUFF(
        //        (
        //         SELECT  ', ' + CreatedbyRep FROM 
        //         (
        //           SELECT distinct CreatedbyRep FROM [tblLeadReport] 
        //           where Company = A.Company
        //           --Your query here
        //         ) AS T FOR XML PATH('')
        //        )
        //        ,1,1,'') AS CreatedByReplist
        //        ,
        //        sum(Follow_upCount) as FollowUpCount

        //         from
        //          (select distinct
        //          Company
        //          ,Count(Company) 'Follow_upCount'
        //          ,max(AnnualRevenue) 'AnnualRevenue'
        //          ,CreatedbyRep
        //          ,forcestatus
        //          from [tblLeadReport]
        //           group by Company,AnnualRevenue,CreatedbyRep,forcestatus
        //          ) A

        //          group by Company, AnnualRevenue,forcestatus
        //          order by AnnualRevenue desc
        //        ";



        //    using (var conn = new SqlConnection(_connectionString))
        //    {
        //        conn.Open();
        //        var Lead = await conn.QueryAsync<LeadPredictionOutPut>(sqlstr);
        //        return Lead;
        //    }
        //}


        private async Task<IEnumerable<LeadPredictionOutPut>> GetReport( int count_of_Records_Fetched, DateTime startDate, DateTime endDate, double annualRevenueRange, string status)
        {
            string whereclause_greater = string.Empty;
            string sqlstr = string.Empty;
            string whereclause = string.Empty;
            string wheredateclause = string.Empty;
            if (count_of_Records_Fetched == 0)
            {
                count_of_Records_Fetched = 3;
            }
            sqlstr = $"SELECT top {count_of_Records_Fetched} Company,sum(AnnualRevenue)as AnnualRevenue,CreatedbyRep,ForceStatus as status FROM LeadConversion  group by Company,CreatedDate,CreatedbyRep,ForceStatus order by AnnualRevenue desc;";
            sqlstr = $"select distinct top {count_of_Records_Fetched}";
            if (annualRevenueRange != 0)
            {
                whereclause_greater = $" where a.annualrevenue > { annualRevenueRange}";
            }
            else if ((annualRevenueRange == 0))
            {
                whereclause_greater = $"";
            }

            var strStatus = string.IsNullOrEmpty( status)? "": status.ToString().ToUpperInvariant();
            if (strStatus == "SUCCESS" || strStatus == "SUCCESSFULL" || strStatus == "SUCCESSFUL")
            {
                if (!string.IsNullOrEmpty(whereclause_greater))
                    whereclause_greater = whereclause_greater + $" and a.forcestatus ='3: Meeting Set'";
                else
                    whereclause_greater = whereclause_greater + $" where a.forcestatus ='3: Meeting Set'";

            }

           // var strStatus = startDate != DateTime.MinValue : status.ToString().ToUpperInvariant();
            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                
                    wheredateclause =  $" where CreatedDate between '" + startDate + "' and '" + endDate + "'";
               
            }
            if (startDate != DateTime.MinValue && endDate == DateTime.MinValue)
            {
              
                
                    wheredateclause =  $" where CreatedDate > '" + startDate + "'";

            }

            sqlstr = sqlstr + @"  company, AnnualRevenue,forcestatus,
                STUFF(
                (
                       SELECT  ', ' + CreatedbyRep FROM 
                       (
                         SELECT distinct CreatedbyRep FROM [tblLeadReport] 
                         where Company = A.Company
                         --Your query here
                       ) AS T FOR XML PATH('')
                )
                ,1,1,'') AS CreatedByReplist
                ,
                sum(Follow_upCount) as FollowUpCount
                ,forcestatus

                 from
                  (select distinct
                  Company
                  ,Count(Company) 'Follow_upCount'
                  ,max(AnnualRevenue) 'AnnualRevenue'
                  ,CreatedbyRep
                  ,forcestatus
                  from [tblLeadReport] "
            + wheredateclause + @"
                    group by Company,AnnualRevenue,CreatedbyRep,forcestatus
                  ) A " + whereclause_greater + @"
                  group by Company, AnnualRevenue,forcestatus
                  order by AnnualRevenue desc
                ";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var Lead = await conn.QueryAsync<LeadPredictionOutPut>(sqlstr);
                return Lead;
            }
        }

    }
}

    


using PreludeSys.Dto;
using PreludeSys.Dto.ViewModel;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using PreludeSys.Interface;

namespace PreludeSys.Api.AirQuality.Queries
{
    public class AirQualityQueries : IAirQualityQueries
    {
        private readonly string _connectionString;

        public AirQualityQueries(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("message", nameof(connectionString));
            }
            _connectionString = connectionString;
        }


        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByAreaAsync(string areaName, DateTime startDate, DateTime endDate, string duration)
        {
            return await GetAirQuality(string.Empty, string.Empty, string.Empty, string.Empty, areaName, startDate, endDate, duration);
        }

        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByZipCodeAsync(string zipCode, DateTime startDate, DateTime endDate, string duration)
        {
            return await GetAirQuality(string.Empty, string.Empty, zipCode, string.Empty, string.Empty, startDate, endDate, duration);
        }

        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByCityAsync(string city, DateTime startDate, DateTime endDate, string duration)
        {
            return await GetAirQuality(city, string.Empty, string.Empty, string.Empty, string.Empty, startDate, endDate, duration);
        }

        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByCountyAsync(string county, DateTime startDate, DateTime endDate, string duration)
        {
            return await GetAirQuality(string.Empty, string.Empty, string.Empty, county, string.Empty, startDate, endDate, duration);
        }

        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByStateAsync(string state, DateTime startDate, DateTime endDate, string duration)
        {
            return await GetAirQuality(string.Empty, state, string.Empty, string.Empty, string.Empty, startDate, endDate, duration);
        }


        private async Task<State> GetState(string stateName)
        {
            string sqlString = $"select * from state where statename = '{stateName}'";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var states = await conn.QueryAsync<State>(sqlString);

                return states.FirstOrDefault();
            }
        }
        private async Task<State> GetStateById(int stateId)
        {
            string sqlString = $"select * from state where stateid = {stateId}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var states = await conn.QueryAsync<State>(sqlString);

                return states.FirstOrDefault();
            }
        }

        private async Task<County> GetCounty(string countyName)
        {
            string sqlString = $"select * from county where countyname = '{countyName}'";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var counties = await conn.QueryAsync<County>(sqlString);

                return counties.FirstOrDefault();
            }
        }

        private async Task<County> GetCountyById(int countyId)
        {
            string sqlString = $"select * from county where countyid = {countyId}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var counties = await conn.QueryAsync<County>(sqlString);

                return counties.FirstOrDefault();
            }
        }


        private async Task<Area> GetArea(string areaName)
        {
            string sqlString = $"select * from area where areaname = '{areaName}'";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var areas = await conn.QueryAsync<Area>(sqlString);

                return areas.FirstOrDefault();
            }
        }

        private async Task<Area> GetAreaById(int areaId)
        {
            string sqlString = $"select * from area where areaid = {areaId}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var areas = await conn.QueryAsync<Area>(sqlString);

                return areas.FirstOrDefault();
            }
        }

        private async Task<AreaGroup> GetAreaGroup(string areaGroupName)
        {
            string sqlString = $"select * from areagroup where areagroupname = '{areaGroupName}'";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var areagroups = await conn.QueryAsync<AreaGroup>(sqlString);

                return areagroups.FirstOrDefault();
            }
        }

        private async Task<AreaGroup> GetAreaGroupById(int areagroupId)
        {
            string sqlString = $"select * from areagroup where areagroupid = {areagroupId}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var areas = await conn.QueryAsync<AreaGroup>(sqlString);

                return areas.FirstOrDefault();
            }
        }

        private async Task<City> GetCity(string cityName)
        {
            string sqlString = $"select * from city where cityname = '{cityName}'";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var cities = await conn.QueryAsync<City>(sqlString);

                return cities.FirstOrDefault();
            }
        }

        private async Task<City> GetCityById(int cityId)
        {
            string sqlString = $"select * from city where cityId = {cityId}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var cities = await conn.QueryAsync<City>(sqlString);

                return cities.FirstOrDefault();
            }
        }

        private async Task<IEnumerable<AirQualityOutput>> GetAirQuality(string city, string state, string zipCode, string county, string areaName, DateTime? startDate, DateTime? endDate, string duration)
        {
            string sqlString = GetQuery(city, state, zipCode, county, areaName, startDate, endDate, duration);
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var airQualityList = await conn.QueryAsync<AirQualityOutput>(sqlString);
                foreach (var airQuality in airQualityList)
                {
                    SetAQICategory(airQuality);
                    SetPolutant(airQuality);
                }
                return airQualityList;
            }
        }

        private static void SetPolutant(AirQualityOutput airQuality)
        {
            if (airQuality.Ozone >= airQuality.AQI)
            {
                airQuality.Pollutant = Constants.PollutantDetails.Ozone;
            }
            else if (airQuality.PMTen >= airQuality.AQI)
            {
                airQuality.Pollutant = Constants.PollutantDetails.PM10;
            }
            else if (airQuality.PMTwoFive >= airQuality.AQI)
            {
                airQuality.Pollutant = Constants.PollutantDetails.PM25;
            }
            else if (airQuality.CarbonMonoOxide >= airQuality.AQI)
            {
                airQuality.Pollutant = Constants.PollutantDetails.CO;
            }
            else if (airQuality.NitorgenDiOxide >= airQuality.AQI)
            {
                airQuality.Pollutant = Constants.PollutantDetails.No2;
            }
        }

        private static void SetAQICategory(AirQualityOutput airQuality)
        {
            if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.Good)
            {
                airQuality.AQICategory = PreludeSys.Dto.AQICategory.Good.ToString();
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.Moderate)
            {
                airQuality.AQICategory = PreludeSys.Dto.AQICategory.Moderate.ToString();
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.UnhealthyForSensitiveGroup)
            {
                airQuality.AQICategory = PreludeSys.Dto.AQICategory.UnhealthyForSensitiveGroup.ToString();
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.Unhealthy)
            {
                airQuality.AQICategory = PreludeSys.Dto.AQICategory.Unhealthy.ToString();
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.VeryUnhealthy)
            {
                airQuality.AQICategory = PreludeSys.Dto.AQICategory.VeryUnhealthy.ToString();
            }
            else if (airQuality.AQI > PreludeSys.Dto.Constants.PollutantThresold.Hazardous)
            {
                airQuality.AQICategory = PreludeSys.Dto.AQICategory.Hazardous.ToString();
            }
        }

        private static string GetQuery(string city, string state, string zipCode, string county, string areaName, DateTime? startDate, DateTime? endDate, string duration)
        {
            string whereClause = string.Empty;
            string groupByClause = string.Empty;
            string selectClause = "select 1";
            if (!string.IsNullOrEmpty(state))
            {
                whereClause = whereClause + $" and State like '%{state}%'";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by state " : " , state";
                selectClause = selectClause + ", state";
            }
            if (!string.IsNullOrEmpty(areaName))
            {
                whereClause = whereClause + $" and AreaName like '%{areaName}%'";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by AreaName , AreaNumber ,county,city,state" : " , AreaName , AreaNumber";
                selectClause = selectClause + ", AreaName , AreaNumber , county,city,state";
            }
            if (!string.IsNullOrEmpty(county))
            {
                whereClause = whereClause + $" and county like '%{county}%'";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by  state , city,  county" : " , county";
                selectClause = selectClause + ", state, city, county";
            }
            if (!string.IsNullOrEmpty(city))
            {
                whereClause = whereClause + $" and City like '%{city}%'";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by city , state " : " , City";
                selectClause = selectClause + ", state, city";
            }

            if (!string.IsNullOrEmpty(zipCode))
            {
                whereClause = whereClause + $" and ZipCode like '%{zipCode}%'";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by zipcode, areanumber, areaname , county,city,state   " : " , zipcode";
                selectClause = selectClause + ", zipcode, areanumber, areaname , county,city,state";
            }
            if (startDate.HasValue && endDate.HasValue)
            {
                if (string.IsNullOrEmpty(groupByClause))
                {
                    selectClause = "select Zipcode, AreaNumber, AreaName, county,city,state";
                    groupByClause = " Group by ZipCode, AreaNumber, AreaName, county,city,state";
                }
                whereClause = whereClause + $" and CONVERT(DATE, ForecastDateTime)  between '{startDate.Value.ToString("yyyy-MM-dd")}' and '{endDate.Value.ToString("yyyy-MM-dd")}'";
            }
            if (duration != PreludeSys.Dto.Duration.None.ToString())
            {
                if (duration == PreludeSys.Dto.Duration.Hour.ToString())
                {
                    selectClause = selectClause + " ,  CAST(ForecastDateTime as date)  as ForecastDateTime ";
                    groupByClause = groupByClause + " , CAST(ForecastDateTime as date) , DATEPART(hour,ForecastDateTime) ";
                }
                else if (duration == PreludeSys.Dto.Duration.Day.ToString())
                {
                    selectClause = selectClause + " , CAST(ForecastDateTime as date)  as ForecastDateTime";
                    groupByClause = groupByClause + " , CAST(ForecastDateTime as date) , DATEPART(day,ForecastDateTime) ";
                }
            }

            string sqlString = $" {selectClause }, " +
                $"AVG(AQI) as AQI, AVG(PMTwoFive) as PMTwoFive, AVG(PMTen) as PMTen , AVG(Ozone) as Ozone , AVG(NitorgenDiOxide) as NitorgenDiOxide, AVG(CarbonMonoOxide) as CarbonMonoOxide ,AVG(SulfurDiOxide) as SulfurDiOxide from dbo.Weather " +
                $"where 1=1 { whereClause } { groupByClause}";

            return sqlString;
        }

        public async Task<AirQualityOutput> GetAirQuality(string location, DateTime startDate, DateTime endDate, string duration = "None")
        {
            var locationType = LocationType.City;
            var state = await GetState(location);
            var sqlQuery = string.Empty;
            if (state != null && state.StateId > 0)
            {
                locationType = LocationType.State;
                sqlQuery = GetQueryById(0, state.StateId, 0, 0, 0,0,startDate, endDate, duration);
            }
            var county = await GetCounty(location);
            var area = new Area();
            if (county != null && county.CountyId > 0)
            {
                locationType = LocationType.County;
                sqlQuery = GetQueryById(0, 0, 0, county.CountyId, 0,0, startDate, endDate, duration);
                state = await GetStateById(county.StateId);
                area = await GetAreaById(county.AreaId);
            }
            var areaGroup = new AreaGroup();
            areaGroup = await GetAreaGroup(location);
            if ( areaGroup != null && areaGroup.AreaGroupId > 0)
            {
                locationType = LocationType.AreaGroup;
                sqlQuery = GetQueryById(0, 0, 0, 0, 0, areaGroup.AreaGroupId, startDate, endDate, duration);
                state = await GetStateById(areaGroup.StateId);
            }
            area = await GetArea(location);
            if (area != null && area.AreaNumber > 0)
            {
                locationType = LocationType.Area;
                sqlQuery = GetQueryById(0, 0, 0, 0, area.AreaNumber,0, startDate, endDate, duration);
                state = await GetStateById(area.StateId);
            }
            var city = await GetCity(location);
            if (city != null && city.CityId > 0)
            {
                locationType = LocationType.City;
                sqlQuery = GetQueryById(city.CityId, 0, 0, 0, 0,0, startDate, endDate, duration);
                state = await GetStateById(city.StateId);
                area = await GetAreaById(city.AreaId);
                county = await GetCountyById(city.CountyId);
            }
            int zipCode = 0;
            if (int.TryParse(location, out zipCode))
            {
                locationType = LocationType.ZipCode;
                sqlQuery = GetQueryById(0, 0, zipCode, 0, 0,0, startDate, endDate, duration);
            }
            if ( string.IsNullOrEmpty( sqlQuery))
            {
                return Task.FromResult<AirQualityOutput>(null).Result;
            }
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var airQualityList = await conn.QueryAsync<AirQualityOutput>(sqlQuery);
                foreach (var airQuality in airQualityList)
                {
                    SetAQILevel(airQuality);
                    SetAQICategory(airQuality);
                    SetPolutant(airQuality);
                    SetDescription(airQuality);
                    airQuality.ForecastDateTime = startDate;
                    airQuality.LocationType = locationType;
                    if (locationType == LocationType.ZipCode)
                    {
                        areaGroup = await GetAreaGroupById(airQuality.AreaGroupId);
                        area = await GetAreaById(airQuality.AreaId);
                        city = await GetCityById(airQuality.CityId);
                        county = await GetCountyById(airQuality.CountyId);
                        state = await GetStateById(airQuality.StateId);
                    }
                    if (state != null)
                    {
                        airQuality.StateName = state.StateName;
                        airQuality.StateCode = state.StateCode;
                    }
                    if ( city != null)
                        airQuality.CityName = city.CityName;
                    if ( areaGroup != null)
                        airQuality.AreaGroupName = areaGroup.AreaGroupName;
                    if (area != null)
                    {
                        airQuality.AreaCode = area.AreaNumber;
                        airQuality.AreaName = area.AreaName;                        
                    }
                    if (county != null)
                        airQuality.CountyName = county.CountyName;

                }
                return airQualityList.FirstOrDefault();
            }
        }

        private void SetAQILevel(AirQualityOutput airQuality)
        {
            var aqiLevel = airQuality.Ozone;
            if (airQuality.PMTen > aqiLevel)
                aqiLevel = airQuality.PMTen;
            if (airQuality.PMTwoFive > aqiLevel)
                aqiLevel = airQuality.PMTwoFive;
            if (airQuality.CarbonMonoOxide > aqiLevel)
                aqiLevel = airQuality.CarbonMonoOxide;
            if (airQuality.NitorgenDiOxide > aqiLevel)
                aqiLevel = airQuality.NitorgenDiOxide;
            airQuality.AQI = aqiLevel;

        }

        private void SetDescription(AirQualityOutput airQuality)
        {
            if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.Good)
            {
                airQuality.Description = PreludeSys.Dto.Constants.Description.Good;
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.Moderate)
            {
                airQuality.Description = PreludeSys.Dto.Constants.Description.Moderate;
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.UnhealthyForSensitiveGroup)
            {
                airQuality.Description = PreludeSys.Dto.Constants.Description.UnhealthyForSensitiveGroup;
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.Unhealthy)
            {
                airQuality.Description = PreludeSys.Dto.Constants.Description.Unhealthy;
            }
            else if (airQuality.AQI < PreludeSys.Dto.Constants.PollutantThresold.VeryUnhealthy)
            {
                airQuality.Description = PreludeSys.Dto.Constants.Description.VeryUnhealthy;
            }
            else if (airQuality.AQI > PreludeSys.Dto.Constants.PollutantThresold.Hazardous)
            {
                airQuality.Description = PreludeSys.Dto.Constants.Description.Hazardous;
            }
        }

        private static string GetQueryById(int cityId, int stateId, int zipCode, int countyId, int areaCode, int areaGroupId, DateTime? startDate, DateTime? endDate, string duration)
        {
            string whereClause = string.Empty;
            string groupByClause = string.Empty;
            string selectClause = "select 1";
            string innerJoin = string.Empty;
            if (stateId > 0)
            {
                whereClause = whereClause + $" and StateId = {stateId}";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by stateId " : " , stateId";
                selectClause = selectClause + ", stateId";
            }
            if (areaGroupId > 0)
            {
                whereClause = whereClause + $" and AreaGroupId = {areaGroupId}";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by AreaGroupId ,stateid" : " , AreaGroupId";
                selectClause = selectClause + ", AreaGroupId ,stateId";
            }
            if (areaCode > 0)
            {
                whereClause = whereClause + $" and AreaId = {areaCode}";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by AreaId ,AreaGroupId,stateid" : " , AreaId,AreaGroupId";
                selectClause = selectClause + ", AreaId ,AreaGroupId, stateId";
            }
            if (countyId > 0)
            {
                whereClause = whereClause + $" and countyId = {countyId}";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by  stateId ,  countyId" : " , countyId";
                selectClause = selectClause + ", stateId,  countyId";
            }
            if (cityId > 0)
            {
                whereClause = whereClause + $" and CityId = {cityId}";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by cityId , areaid, countyid, stateId " : " , CityId";
                selectClause = selectClause + ", cityId , areaid, countyid, stateId";
            }

            if (zipCode > 0)
            {
                whereClause = whereClause + $" and ZipCode = '{zipCode}'";
                groupByClause = string.IsNullOrEmpty(groupByClause) ? " group by zipcode, cityid,areaid, countyId,stateId   " : " , zipcode";
                selectClause = selectClause + ", zipcode,cityId, areaid,countyId,stateId";
            }
            if (startDate.HasValue && endDate.HasValue)
            {
                if (string.IsNullOrEmpty(groupByClause))
                {
                    selectClause = "select Zipcode, areaid, countyId,cityId,stateId";
                    groupByClause = " Group by ZipCode, areaid, countyId,cityId,stateId";
                }
                whereClause = whereClause + $" and CONVERT(DATE, ForecastDateTime)  between '{startDate.Value.ToString("yyyy-MM-dd")}' and '{endDate.Value.ToString("yyyy-MM-dd")}'";
            }
            if (duration != PreludeSys.Dto.Duration.None.ToString())
            {
                if (duration == PreludeSys.Dto.Duration.Hour.ToString())
                {
                    selectClause = selectClause + " ,  CAST(ForecastDateTime as date)  as ForecastDateTime ";
                    groupByClause = groupByClause + " , CAST(ForecastDateTime as date) , DATEPART(hour,ForecastDateTime) ";
                }
                else if (duration == PreludeSys.Dto.Duration.Day.ToString())
                {
                    selectClause = selectClause + " , CAST(ForecastDateTime as date)  as ForecastDateTime";
                    groupByClause = groupByClause + " , CAST(ForecastDateTime as date) , DATEPART(day,ForecastDateTime) ";
                }
            }
            string sqlString = $" {selectClause }, " +
                $"AVG(AQI) as AQI, AVG(PMTwoFive) as PMTwoFive, AVG(PMTen) as PMTen , AVG(Ozone) as Ozone , AVG(NitorgenDiOxide) as NitorgenDiOxide, AVG(CarbonMonoOxide) as CarbonMonoOxide ,AVG(SulfurDiOxide) as SulfurDiOxide from dbo.AirQuality " +
                $"where 1=1 { whereClause } { groupByClause}";

            return sqlString;
        }


    }
}

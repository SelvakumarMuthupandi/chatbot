using PreludeSys.Api.AirQuality.Model;
using PreludeSys.Interface;
using System;
using System.Threading.Tasks;

namespace PreludeSys.Api.AirQuality.Commands
{
    public class AirQualityCommand : IAirQualityCommand
    {
        private readonly PreludeSysContext _context;

        public AirQualityCommand(PreludeSysContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<PreludeSys.Dto.AirQuality> SaveAirQualityAsync(PreludeSys.Dto.AirQuality airQualityDto)
        {
            await _context.AirQuality.AddAsync(airQualityDto);
            await _context.SaveChangesAsync();

            return airQualityDto;
        }
    }
}

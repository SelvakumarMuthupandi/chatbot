using PreludeSys.Dto.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PreludeSys.Interface;

namespace PreludeSys.Api.AirQuality.Controllers
{
    [ApiController]
    
    public class AirQualityController : Controller
    {
        private readonly IAirQualityQueries _queries;
        private readonly IAirQualityCommand _commands;
        private readonly IConfiguration _settings;


        public AirQualityController(IAirQualityQueries queries, IAirQualityCommand commands , IConfiguration settings)
        {
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _commands = commands ?? throw new ArgumentNullException(nameof(commands));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        [HttpGet]
        [Route("api/v1/airquality/search")]
        public async Task<AirQualityOutput> GetAirQuality(string location  , DateTime? startDate, DateTime? endDate, string duration = "None")
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQuality(location, startDate.Value, endDate.Value,duration));
        }

        [HttpGet]
        [Route("api/v1/airquality/area")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityForAllAreas( DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate,  ref endDate,ref  duration);
            return (await _queries.GetAirQualityByAreaAsync(string.Empty,startDate.Value,endDate.Value,duration)).ToList();
        }


        [HttpGet]
        [Route("api/v1/airquality/area/{areaName}")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByArea(string areaName, DateTime? startDate , DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByAreaAsync(areaName, startDate.Value, endDate.Value,duration));
        }


        [HttpGet]
        [Route("api/v1/airquality/county")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityFoAllCounties( DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput( ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByCountyAsync(string.Empty, startDate.Value, endDate.Value, duration));
        }


        [HttpGet]
        [Route("api/v1/airquality/county/{county}")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByCounty(string county, DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByCountyAsync(county, startDate.Value, endDate.Value, duration));
        }

        [HttpGet]
        [Route("api/v1/airquality/city")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityForAllCities( DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByCityAsync(string.Empty, startDate.Value, endDate.Value, duration));
        }

        [HttpGet]
        [Route("api/v1/airquality/city/{city}")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByCity(string city, DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByCityAsync(city, startDate.Value, endDate.Value,duration));
        }

        [HttpGet]
        [Route("api/v1/airquality/zipcode")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityForAllZipCodes( DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByZipCodeAsync(string.Empty, startDate.Value, endDate.Value, duration));
        }

        [HttpGet]
        [Route("api/v1/airquality/zipcode/{zipCode}")]
        public async Task<IEnumerable<AirQualityOutput>> GetAirQualityByZipCode(string zipCode, DateTime? startDate, DateTime? endDate, string duration)
        {
            ValidateInput(ref startDate, ref endDate, ref duration);
            return (await _queries.GetAirQualityByZipCodeAsync (zipCode, startDate.Value, endDate.Value,duration));
        }

        [HttpPost]
        [Route("api/v1/airquality")]
        public void SaveWeather([FromBody] PreludeSys.Dto.AirQuality airQualityDto)
        {
            _commands.SaveAirQualityAsync(airQualityDto);
        }

        private static void ValidateInput(ref DateTime? startDate, ref DateTime? endDate, ref string duration)
        {
            var currentDateTime = DateTime.UtcNow;
            if (!startDate.HasValue)
                startDate = currentDateTime.AddDays(-1);
            if (!endDate.HasValue)
                endDate = currentDateTime;
            if (string.IsNullOrEmpty(duration))
                duration = PreludeSys.Dto.Duration.Day.ToString();
        }

    }
}

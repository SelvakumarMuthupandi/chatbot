﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PreludeSys.Dto.ViewModel;
using PreludeSys.Interface;
using PreludeSys.Dto;
using System.Text.RegularExpressions;

namespace PreludeSys.Api.AirQuality.Controllers
{
    [ApiController]

    public class LeadPredictionReportController : Controller
    {
        private readonly  ILeadPredictionReportQueries _queries;
        private readonly IConfiguration _settings;

       
        public LeadPredictionReportController(ILeadPredictionReportQueries queries, IAirQualityCommand commands, IConfiguration settings)
        {
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        [HttpGet]
        [Route("api/v1/leadReport/RevenueReport")]
        public async Task<IEnumerable<LeadPredictionOutPut>> GetLeadRevenueInfo(int Recordscount,DateTime startDate, DateTime endDate,string annualrangevalue, string status)
        {
            double annualval;
            annualval= Double.Parse(Regex.Replace(annualrangevalue, @"[^\d.]", ""));
           // Double.TryParse(annualrangevalue, out annualval);
             return (await _queries.GetLeadInfo(Recordscount,startDate,endDate, annualval, status));


        }


        [HttpGet]
        [Route("api/v1/leadReport/BestSourceReport")]
        public async Task<IEnumerable<LeadSourceOutPut>> GetBestLeadSourceInfo()
        {
            //ValidateInput(ref startDate, ref endDate);
            return (await _queries.GetBestLeadSourceInfo());

        }



    }
}
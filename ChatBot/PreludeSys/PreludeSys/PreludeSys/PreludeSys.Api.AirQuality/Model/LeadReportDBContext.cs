﻿using Microsoft.EntityFrameworkCore;
using PreludeSys.Dto;
using System;


namespace PreludeSys.Api.AirQuality.Model
{
    public class LeadReportDBContext : DbContext
    {
        public DbSet<PreludeSys.Dto.LeadInfo> LeadPredInfo { get; set; }
        public DbSet<State> Company { get; set; }
        public DbSet<City> AnnualRevenue { get; set; }
        public DbSet<City> Status { get; set; }
        public DbSet<City> CreatedByRep { get; set; }
        public DbSet<City> CreatedDate { get; set; }
    
        public LeadReportDBContext(DbContextOptions<LeadReportDBContext> options) : base()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }


}

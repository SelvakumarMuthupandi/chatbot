using Microsoft.EntityFrameworkCore;
using PreludeSys.Dto;
using System;

namespace PreludeSys.Api.AirQuality.Model
{
    public class PreludeSysContext : DbContext
    {
        public DbSet<PreludeSys.Dto.AirQuality> AirQuality { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<County> County { get; set; }

        public PreludeSysContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

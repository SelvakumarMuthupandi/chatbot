using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSys
{
    public class DialogAndWelcomeBot<T> : DialogBot<T>
        where T : Dialog
    {

        private BotState _conversationState;
        private BotState _userState;

        public DialogAndWelcomeBot(ConversationState conversationState, UserState userState, T dialog, ILogger<DialogBot<T>> logger)
            : base(conversationState, userState, dialog, logger)
        {
            _conversationState = conversationState;
            _userState = userState;
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                // Greet anyone that was not the target (recipient) of this message.
                // To learn more about Adaptive Cards, see https://aka.ms/msbot-adaptivecards for more details.
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    if (turnContext.Activity.ChannelId != "alexa")
                    {
                        var welcomeCard = CreateAdaptiveCardAttachment();
                        var response = MessageFactory.Attachment(welcomeCard);
                        await turnContext.SendActivityAsync(response, cancellationToken);

                        //var messageText = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Reports - Top 3 Revenue Leads \n 5. Reports - Best Lead Sources\n";
                        //await turnContext.SendActivityAsync(messageText);

                        var capabilityCard = CreateCapabilityCardAttachment();
                        var capabilityresponse = MessageFactory.Attachment(capabilityCard);
                        await turnContext.SendActivityAsync(capabilityresponse, cancellationToken);

                        await Dialog.RunAsync(turnContext, ConversationState.CreateProperty<DialogState>("DialogState"), cancellationToken);
                    }
                }
            }
        }

        private Attachment CreateCapabilityCardAttachment()
        {
            var cardResourcePath = "PreludeSys.Cards.capability.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }

        }

        private Attachment CreateAdaptiveCardAttachment()
        {
            var cardResourcePath = "PreludeSys.Cards.welcomeCard.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }
        }
    }
}


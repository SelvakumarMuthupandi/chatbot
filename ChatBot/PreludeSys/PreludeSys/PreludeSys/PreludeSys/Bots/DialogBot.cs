using Bot.Builder.Community.Adapters.Alexa;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PreludeSys.Dto;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSys
{

    public class ConversationFlow
    {
        public enum Question
        {
            Name,
            Age,
            Date,
            None, // Our last action did not involve a question.
        }

        // The last question asked.
        public Question LastQuestionAsked { get; set; } = Question.None;
    }
    // This IBot implementation can run any type of Dialog. The use of type parameterization is to allows multiple different bots
    // to be run at different endpoints within the same project. This can be achieved by defining distinct Controller types
    // each with dependency on distinct IBot types, this way ASP Dependency Injection can glue everything together without ambiguity.
    // The ConversationState is used by the Dialog system. The UserState isn't, however, it might have been used in a Dialog implementation,
    // and the requirement is that all BotState objects are saved at the end of a turn.
    public class DialogBot<T> : ActivityHandler
    where T : Dialog
    {
        protected readonly Dialog Dialog;
        protected readonly BotState ConversationState;
        protected readonly BotState UserState;
        protected readonly ILogger Logger;

        public DialogBot(ConversationState conversationState, UserState userState, T dialog, ILogger<DialogBot<T>> logger)
        {
            ConversationState = conversationState;
            UserState = userState;
            Dialog = dialog;
            Logger = logger;
        }

        private async Task ProcessLeadInformation(ITurnContext turnContext, LeadInfo lead, CancellationToken cancellationToken)
        {
            if (!(await LeadService.IsValidLead(turnContext, lead))) return;


            var scoreLabel = string.Empty;
            var scorePercentage = string.Empty;
            JObject joResponse = JObject.Parse(await LeadService.GetLeadPrediction(lead));
            foreach (KeyValuePair<string, JToken> app in joResponse)
            {
                var appName = app.Key;
                foreach (JToken scorePrediction in app.Value["output1"].First)
                {
                    if (string.IsNullOrEmpty(scoreLabel))
                    {
                        var scoreKey = scorePrediction;
                        scoreLabel = scorePrediction.First.Value<string>();
                        scorePercentage = scorePrediction.Next.First.Value<string>();
                    }
                }
            }
            await turnContext.SendActivityAsync(MessageFactory.Text("Scored Labels : " + scoreLabel));
            await turnContext.SendActivityAsync(MessageFactory.Text("Probability for Scored Label : " + scorePercentage));
            return;
        }

        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {

            var conversationStateAccessors = ConversationState.CreateProperty<ConversationFlow>(nameof(ConversationFlow));
            var flow = await conversationStateAccessors.GetAsync(turnContext, () => new ConversationFlow());

            var userStateAccessors = UserState.CreateProperty<LeadInfo>(nameof(LeadInfo));
            var leadInfo = await userStateAccessors.GetAsync(turnContext, () => new LeadInfo());

            switch (turnContext.Activity.Type)
            {
                case AlexaRequestTypes.LaunchRequest:
                    var responseMessage = $"Hi there! You are talking to PreludeSys Bot. How can I help you today?\nSay something like \"How is air quality in Diamond Bar?\"";
                    await turnContext.SendActivityAsync(responseMessage);
                    break;
                case AlexaRequestTypes.SessionEndedRequest:
                    var endMessages = $"Thank for using PreludeSys Bot.";
                    await turnContext.SendActivityAsync(endMessages);
                    break;
            }
            await base.OnTurnAsync(turnContext, cancellationToken);
            // Save any state changes that might have occured during the turn.
            await ConversationState.SaveChangesAsync(turnContext, false, cancellationToken);
            await UserState.SaveChangesAsync(turnContext, false, cancellationToken);
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {

            Logger.LogInformation("Running dialog with Message Activity.");

            // Run the Dialog with the new message Activity.
            await Dialog.RunAsync(turnContext, ConversationState.CreateProperty<DialogState>("DialogState"), cancellationToken);
        }
    }
}

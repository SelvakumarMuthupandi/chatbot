﻿using Microsoft.Bot.Builder.AI.Luis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreludeSys.CognitiveModels
{
    public partial class LeadReportInfo
    {
        public string Income
        {
            get
            {
                var income = Entities?._instance?.income?.FirstOrDefault()?.Text;
                return income;
            }
        }

        public string IncomeValue
        {
            get
            {
                var income = Entities?._instance?.revenuevalue?.FirstOrDefault()?.Text;
                return income;
            }
        }
        public string RecordCount
        {
            get
            {
                var records = Entities?._instance?.recordcount?.FirstOrDefault()?.Text;
                return records;
            }
        }

        public string Expression
        {
            get
            {
                var exp = Entities?._instance?.expression?.FirstOrDefault()?.Text;
                return exp;
            }
        }

        public string Leads
        {
            get
            {
                var leads = Entities?._instance?.marketingrecord?.FirstOrDefault()?.Text;
                return leads;
            }
        }
        public string Source
        {
            get
            {
                var src = Entities?._instance?.leadSourceType?.FirstOrDefault()?.Text;
                return src;
            }
        }

        public DateTime StartDate
        {
            get
            {
                var dt = Entities.startDate;
                return dt;


            }
        }

        public DateTime EndDate
        {
            get
            {
                var dt = Entities.endDate;
                return dt;


            }
        }

        public string Status
        {
            get
            {
                var dt = Entities?._instance?.Status?.FirstOrDefault()?.Text;
                return dt;
            }
        }
    }
}

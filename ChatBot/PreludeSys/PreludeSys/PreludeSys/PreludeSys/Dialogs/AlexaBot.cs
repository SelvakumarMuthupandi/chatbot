// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using PreludeSys.Service;
using PreludeSys.Dto;
using Bot.Builder.Community.Adapters.Alexa;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.Recognizers.Text;
using Microsoft.Recognizers.Text.DateTime;
using System;
using PreludeSys.CognitiveModels;

namespace PreludeSys.Dialogs
{
    public class AlexaBot : ActivityHandler, IBot
    {
        private readonly IConfiguration _configuration;
        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;
        private const string HelpMsgText = "Please drop an email to info@preludesys.com or Contact on +1(123)456-7890";

        public AlexaBot(WeatherRecognizer luisRecognizer, WeatherDialog weatherDialog, ILogger<MainDialog> logger)
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;
        }
        public static bool IsDateRange(string distinctTimexExpressions)
        {
            bool hasDate = false;
            string[] inputText = distinctTimexExpressions.Split(",");//split on a whitespace

            foreach (string text in inputText)
            {
                if (DateTime.TryParse(text, out DateTime Temp) == true)
                {
                    hasDate = true;
                }
                else
                {
                    hasDate = false;
                    break;
                }

            }
            if (hasDate)
            {
                return ((Convert.ToDateTime(inputText[1]) - Convert.ToDateTime(inputText[0])).TotalDays > 7) ? false : true;
            }
            else
            {
                return hasDate;
            }
        }

        public static string DateRange(string Date)
        {
            // Run the recognizer.
            var results = DateTimeRecognizer.RecognizeDateTime(Date, Culture.English);
            var distinctTimexExpressions = new HashSet<string>();
            // We should find a single result in this example.
            foreach (var result in results)
            {
                // The resolution includes a single value because there is no ambiguity.
                var values = (List<Dictionary<string, string>>)result.Resolution["values"];
                foreach (var value in values)
                {
                    if (value.TryGetValue("type", out var type))
                    {
                        if (type == "date" && value.TryGetValue("value", out var val))
                        {
                            distinctTimexExpressions.Add(val + ',' + val);
                        }
                        if (type == "daterange" && value.TryGetValue("start", out var start) && value.TryGetValue("end", out var end))
                        {

                            distinctTimexExpressions.Add(start + ',' + end);
                        }
                    }
                    break;
                }
            }
            return string.Join(',', distinctTimexExpressions);
        }
        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            switch (turnContext.Activity.Type)
            {
                case ActivityTypes.Message:
                    if (turnContext.Activity.Text == "AMAZON.CancelIntent")
                    {
                        await turnContext.SendActivityAsync("You asked to cancel!");
                    }
                    else
                    {
                        await OnMessageActivityAsync(new DelegatingTurnContext<IMessageActivity>(turnContext), cancellationToken);
                    }

                    break;
                case AlexaRequestTypes.LaunchRequest:
                    var responseMessage = $"You launched the PreludeSys Alexa Bot! What can I help you with today?\nSay something like \"How is Air Quality in Diamond Bar?\"";
                    await turnContext.SendActivityAsync(responseMessage);
                    break;
            }
        }

        private async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                // LUIS is not configured, we just run the WeatherDialog path with an empty BookingDetailsInstance.
                //return await turnContext.BeginDialogAsync(nameof(WeatherDialog), new WeatherDetails(), cancellationToken);
                await turnContext.SendActivityAsync("Luis and QnA is not Configured!");
            }
            // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
            var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(turnContext, cancellationToken);
            switch (luisResult.TopIntent().intent)
            {
                case Intent.l_Weather:
                    var weatherDetails = new WeatherDetails()
                    {
                        // Get Location and Date from the composite entities arrays.
                        Location = luisResult.Location,
                        Date = luisResult.Date
                    };
                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    await ProcessWeatherAsyncForAlexa(turnContext, weatherDetails, cancellationToken);
                    break;
                case Intent.l_AirQuality:
                    var airQuality = new WeatherDetails()
                    {
                        // Get Location and Date from the composite entities arrays.
                        Location = luisResult.Location,
                        Date = luisResult.Date
                    };
                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    await ProcessWeatherAsyncForAlexa(turnContext, airQuality, cancellationToken);
                    break;
                case Intent.q_qna:
                    await ProcessSampleQnAForAlexaAsync(turnContext, cancellationToken);
                    break;
                default:
                    Logger.LogInformation($"Unrecognized intent: {luisResult.TopIntent().intent}.");
                    await turnContext.SendActivityAsync(MessageFactory.Text($"Sorry, I didn't get that. Please try asking in a different way. For Example Ask Air Quality in Diamond Bar."), cancellationToken);
                    break;
            }
        }

        private async Task ProcessWeatherAsyncForAlexa(ITurnContext context, WeatherDetails weatherDetails, CancellationToken cancellationToken)
        {
            Logger.LogInformation("ProcessWeatherAsyncForAlexa");

            // Retrieve LUIS results for Weather.
            if (weatherDetails != null)
            {
                // await turnContext.SendActivityAsync(MessageFactory.Text($"ProcessWeather entities were found in the message:\n\n{string.Join("\n\n", result.Entities.Select(i => i.Entity))}"), cancellationToken);
                GetWeatherInPlaceAction action = new GetWeatherInPlaceAction(_configuration);
                var place = weatherDetails.Location;
                var date = weatherDetails.Date != null ? DateRange(weatherDetails.Date) : null;
                if (date != null && !IsDateRange(date))
                {
                    await context.SendActivityAsync(MessageFactory.Text($"Sorry, We don't have records for this time period. We can only provide you next 7 days data."), cancellationToken);
                }
                else
                {
                    var alexaWeatherCard = (AlexaCardContent)await action.FulfillAsyncAlexa(place, date, _luisRecognizer.BaseApiPath, weatherDetails.Date);
                    if (alexaWeatherCard == null)
                    {
                        context.AlexaSetCard(new AlexaCard()
                        {
                            Type = AlexaCardType.Standard,
                            Title = "PreludeSys Weather",
                            Content = $"We don't find any Air Quality Data for {place} . Try for other city or time period.",
                        });
                    }

                    else
                    {
                        await context.SendActivityAsync(alexaWeatherCard.text);
                    }
                }
            }
            else
            {

                await context.SendActivityAsync(MessageFactory.Text($"Please specify a city or location. For Example Ask Air Quality in Diamond Bar."), cancellationToken);
                context.AlexaSetCard(new AlexaCard()
                {
                    Type = AlexaCardType.Standard,
                    Title = $"Weather App",
                    Content = $"Please specify a city or location. For Example Ask Weather in Diamond Bar.",
                });

            }

        }


        private async Task ProcessSampleQnAForAlexaAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            Logger.LogInformation("ProcessSampleQnAAsync");

            var results = await _luisRecognizer.SampleQnA.GetAnswersAsync(turnContext);
            if (results.Any())
            {
                await turnContext.SendActivityAsync(MessageFactory.Text(results.First().Answer), cancellationToken);
            }
            else
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Sorry, could not find an answer in the Q and A system."), cancellationToken);
            }
        }
    }
}

// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using PreludeSys.Weather;
using PreludeSys.Dto;
using Bot.Builder.Community.Adapters.Alexa;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Recognizers.Text.DataTypes.TimexExpression;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using Constants = Microsoft.Recognizers.Text.DataTypes.TimexExpression.Constants;
using PreludeSys.CognitiveModels;
using System.IO;

namespace PreludeSys.Dialogs
{
    public class WeatherDialog : CancelAndHelpDialog
    {
        private const string LocationStepMsgText = "For which location you want to get information?";
        Dto.ViewModel.AirQualityOutput model = new Dto.ViewModel.AirQualityOutput();
        WeatherModel modelx = new WeatherModel();
        private readonly IConfiguration configuration;
        private const string MoreDetailMsgText = "Do you want more details?";
        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;

        public WeatherDialog(WeatherRecognizer luisRecognizer, ILogger<MainDialog> logger)
            : base(nameof(WeatherDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new DateResolverDialog());
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                LocationStepAsync,
                DateStepAsync,
                InitialInformtionAsync,
                FinalStepAsync,
            }));
            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> LocationStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var weatherDetails = (WeatherDetails)stepContext.Options;
            if (weatherDetails.Location == null)
            {
                var promptMessage = MessageFactory.Text(LocationStepMsgText, LocationStepMsgText, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            }
            return await stepContext.NextAsync(weatherDetails.Location, cancellationToken);
        }

        private async Task<DialogTurnResult> DateStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var weatherDetails = (WeatherDetails)stepContext.Options;
            weatherDetails.Location = (string)stepContext.Result;
            weatherDetails.ActualDate = weatherDetails.ActualDate == null ? "today" : weatherDetails.ActualDate;
            if (weatherDetails.ActualDate != null && IsAmbiguous(weatherDetails.ActualDate))
            {
                return await stepContext.BeginDialogAsync(nameof(DateResolverDialog), weatherDetails, cancellationToken);
            }
            return await stepContext.NextAsync(weatherDetails, cancellationToken);
        }

        private async Task<DialogTurnResult> InitialInformtionAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var weatherDetails = (WeatherDetails)stepContext.Options;
            var weatherResults = (WeatherDetails)stepContext.Result;
            weatherDetails.Date = stepContext.Result == null ? null : weatherResults.Date.ToString();
            weatherDetails.ActualDate = weatherResults.ActualDate == null ? null : weatherDetails.ActualDate;
            if (stepContext.Options is WeatherDetails result)
            {
                DataService weatherService = new DataService(configuration);
                string prompt = await weatherService.GetOverviewAsync(weatherDetails.Location, weatherDetails.ActualDate, weatherDetails.Date, weatherDetails.Type, _luisRecognizer.BaseApiPath, _luisRecognizer.APIXUKey);
                if (stepContext.Context.Activity.ChannelId != "alexa" && stepContext.Context.Activity.ChannelId != "email")  
                {
                    if (prompt != "false")
                    {
                        await stepContext.Context.SendActivityAsync(prompt );
                        var promptMessage = MessageFactory.Text(MoreDetailMsgText, MoreDetailMsgText, InputHints.ExpectingInput);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
                    }
                    else
                    {
                        await stepContext.Context.SendActivityAsync($"We don't find any {weatherDetails.Type} Data for {weatherDetails.Location}. Ask  for other city or time period.");
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    }
                }
                if (stepContext.Context.Activity.ChannelId == "alexa" || stepContext.Context.Activity.ChannelId == "email")
                {
                    if (prompt != "false")
                    {
                        prompt = prompt + ". \n" + MoreDetailMsgText;
                        var promptMessage = MessageFactory.Text(prompt, prompt, InputHints.ExpectingInput);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
                    }
                    else
                    {
                        stepContext.ActiveDialog.State["stepIndex"] = 2;
                    }
                }
            }
            return await stepContext.NextAsync(weatherDetails, cancellationToken);
        }

        private Attachment CreateCapabilityCardAttachment()
        {
            var cardResourcePath = "PreludeSys.Cards.capability.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }
        }

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var weatherDetails = (WeatherDetails)stepContext.Options;
            if (stepContext.Context.Activity.ChannelId == "alexa" || stepContext.Context.Activity.ChannelId == "email")
            {
                // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
                var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(stepContext.Context, cancellationToken);
                if (luisResult.TopIntent().intent == "I_Yes")
                {
                    DataService action = new DataService(configuration);
                    var prompt = await action.GetDetailsAsync(weatherDetails.Location, weatherDetails.ActualDate, weatherDetails.Date, weatherDetails.Type, _luisRecognizer.BaseApiPath, _luisRecognizer.APIXUKey);
                    var message = MessageFactory.Text(prompt, prompt, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                }
                else if (luisResult.TopIntent().intent == "I_Yes")
                {
                    var prompt = "What else can I do for you?";
                    var message = MessageFactory.Text(prompt, prompt, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);

                }
                else
                {
                    var notFountMsg = $"No information found for your query. Ask for other city or time period.";
                    var promptMessage = MessageFactory.Text(notFountMsg, notFountMsg, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
                }
            }
            else
            {
                // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
                var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(stepContext.Context, cancellationToken);
                if (luisResult.TopIntent().intent == "I_Yes")
                {
                    await ProcessWeatherAsync(stepContext.Context, weatherDetails, cancellationToken);
                    //var messageText = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Reports - Top 3 Revenue Leads \n 5. Reports - Best Lead Sources\n";
                    //await stepContext.Context.SendActivityAsync(messageText);

                    var capabilityCard = CreateCapabilityCardAttachment();
                    var capabilityresponse = MessageFactory.Attachment(capabilityCard);
                    await stepContext.Context.SendActivityAsync(capabilityresponse, cancellationToken);


                }
                else if (luisResult.TopIntent().intent == "I_No")
                {
                    await stepContext.Context.SendActivityAsync($"What else can I do for you?");

                    //var messageText = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Reports - Top 3 Revenue Leads \n 5. Reports - Best Lead Sources\n";
                    //await stepContext.Context.SendActivityAsync(messageText);
                    var capabilityCard = CreateCapabilityCardAttachment();
                    var capabilityresponse = MessageFactory.Attachment(capabilityCard);
                    await stepContext.Context.SendActivityAsync(capabilityresponse, cancellationToken);


                }
                else
                {
                    await stepContext.Context.SendActivityAsync($"No information found for your query. Ask for other city or time period.");
                }
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
        }
        private static bool IsAmbiguous(string timex)
        {
            var timexProperty = new TimexProperty(timex);
            return !timexProperty.Types.Contains(Constants.TimexTypes.Definite);
        }

        private async Task ProcessWeatherAsync(ITurnContext context, WeatherDetails weatherDetails, CancellationToken cancellationToken)
        {
            Logger.LogInformation("ProcessWeatherAsync");
            if (weatherDetails != null)
            {
                DataService dataService = new DataService(configuration);
                var place = weatherDetails.Location;
                var date = weatherDetails.Date;
                var type = weatherDetails.Type;

                var weatherCard = (AdaptiveCards.AdaptiveCard)await dataService.FulfillAsync(place, date, type, _luisRecognizer.BaseApiPath, _luisRecognizer.APIXUKey);

                if (weatherCard == null)
                {
                    await context.SendActivityAsync(MessageFactory.Text($"We don't find any Air Quality Data for {place}. Ask for other city or time period."), cancellationToken);
                }
                else
                {
                    var adaptiveCardAttachment = new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(weatherCard)),
                    };
                    await context.SendActivityAsync(MessageFactory.Attachment(adaptiveCardAttachment), cancellationToken);
                }
            }
            else
            {
                await context.SendActivityAsync(MessageFactory.Text($"Please specify a city or location. For example ask weather in Diamond Bar."), cancellationToken);
                context.AlexaSetCard(new AlexaCard()
                {
                    Type = AlexaCardType.Simple,
                    Title = $"Weather App",
                    Content = $"Please specify a city or location. For example ask air quality in Diamond Bar.",
                });
            }
        }
    }
}

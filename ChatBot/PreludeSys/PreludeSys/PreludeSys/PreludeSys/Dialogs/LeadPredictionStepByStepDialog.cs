using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using PreludeSys.Dto;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSys.Dialogs
{
    public class LeadPredictionStepByStepDialog : CancelAndHelpDialog
    {

        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;

        public LeadPredictionStepByStepDialog(WeatherRecognizer luisRecognizer, ILogger<MainDialog> logger) : base(nameof(LeadPredictionStepByStepDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new WaterfallDialog(nameof(LeadPredictionStepByStepDialog), new WaterfallStep[]
            {
                GetCompanyAsync,
                GetAnnualRevenueAsync,
                GetCreatedDateAsync,
                GetTitleAsync,
                GetSubjectAsync,
                GetTimeZoneAsync,
                GetNumberValidationAsync,
                GetValidationLinkAsync,
                ProcessLeadInformationAsync
            }));
            // The initial child Dialog to run.
            InitialDialogId = nameof(LeadPredictionStepByStepDialog);
        }

        private async Task<DialogTurnResult> ProcessLeadInformationAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var lead = (LeadInfo)stepContext.Options;
            lead.ValidationLink = (string)stepContext.Result;
            if ( await LeadService.IsValidLead(stepContext.Context , lead))
            {
                var scoreLabel = string.Empty;
                var scorePercentage = string.Empty;
                JObject joResponse = JObject.Parse(await LeadService.GetLeadPrediction(lead));
                foreach (KeyValuePair<string, JToken> app in joResponse)
                {
                    var appName = app.Key;
                    foreach (JToken scorePrediction in app.Value["output1"].First)
                    {
                        if (string.IsNullOrEmpty(scoreLabel))
                        {
                            var scoreKey = scorePrediction;
                            scoreLabel = scorePrediction.First.Value<string>();
                            scorePercentage = scorePrediction.Next.First.Value<string>();
                        }
                    }
                }
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("Scored Labels : " + scoreLabel));
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("Probability for Scored Label : " + scorePercentage));
            }
            else
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("Not able to provide lead prediction now. Try it again later."));
            }
            return await stepContext.EndDialogAsync();
        }

        private async Task<DialogTurnResult> GetValidationLinkAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.NumberValidation = (string)stepContext.Result;
            if (string.IsNullOrEmpty(leadInfo.NumberValidation))
            {
                var numberValidationMessage = $"Please provide valid contact number validation details for '{ leadInfo.Company }'";
                var numberValidationActivity = MessageFactory.Text(numberValidationMessage, numberValidationMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(numberValidationActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if (string.IsNullOrEmpty(leadInfo.ValidationLink))
            {
                var validationLinkMessage = $"Please provide source of the lead for '{leadInfo.Company}' ?";
                var validationLinkActivity = MessageFactory.Text(validationLinkMessage, validationLinkMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = validationLinkActivity }, cancellationToken);
            }

            return await stepContext.NextAsync(leadInfo.ValidationLink, cancellationToken);
        }

        private async Task<DialogTurnResult> GetNumberValidationAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.TimeZone = (string)stepContext.Result;
            if (string.IsNullOrEmpty(leadInfo.TimeZone))
            {
                var timezoneMessage = $"Please provide valid time zone for '{ leadInfo.Company }'";
                var timezoneActivity = MessageFactory.Text(timezoneMessage, timezoneMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(timezoneActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if (string.IsNullOrEmpty(leadInfo.NumberValidation))
            {
                var numberValidationMessage = $"Please provide contact number validation details for '{leadInfo.Company}' ?";
                var numberValidationActivity = MessageFactory.Text(numberValidationMessage, numberValidationMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = numberValidationActivity }, cancellationToken);
            }

            return await stepContext.NextAsync(leadInfo.NumberValidation, cancellationToken);
        }

        private async Task<DialogTurnResult> GetTimeZoneAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.Subject = (string)stepContext.Result;
            if (string.IsNullOrEmpty(leadInfo.Subject))
            {
                var subjectMessage = $"Please provide valid subject for  '{ leadInfo.Company }'";
                var subjectActivity = MessageFactory.Text(subjectMessage, subjectMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(subjectActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if (string.IsNullOrEmpty(leadInfo.TimeZone))
            {
                var timezoneMessage = $"Please provide timezone for '{leadInfo.Company}' ?";
                var timeZoneActivity = MessageFactory.Text(timezoneMessage, timezoneMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = timeZoneActivity }, cancellationToken);
            }

            return await stepContext.NextAsync(leadInfo.TimeZone, cancellationToken);
        }

        private async Task<DialogTurnResult> GetSubjectAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.Title = (string)stepContext.Result;
            if (string.IsNullOrEmpty(leadInfo.Title))
            {
                var titleMessage = $"Please provide contact person designation for '{ leadInfo.Company }'";
                var titleActivity = MessageFactory.Text(titleMessage, titleMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(titleActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if (string.IsNullOrEmpty(leadInfo.Subject))
            {
                var subjectMessage = $"Please provide subject for '{leadInfo.Company}' ?";
                var subjectActivity = MessageFactory.Text(subjectMessage, subjectMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = subjectActivity }, cancellationToken);
            }

            return await stepContext.NextAsync(leadInfo.Subject, cancellationToken);
        }

        private async Task<DialogTurnResult> GetTitleAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.CreatedDate = (string)stepContext.Result;
            DateTime createdDate;
            if (!DateTime.TryParse(leadInfo.CreatedDate, out createdDate))
            {
                var createdDateMessage = $"Created date is not valid. Please provide valid created date for '{ leadInfo.Company }'";
                var createdDateActivity = MessageFactory.Text(createdDateMessage, createdDateMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(createdDateActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if (string.IsNullOrEmpty(leadInfo.Title))
            {
                var titleMessage = $"Please provide contact person designation for '{leadInfo.Company}' ?";
                var titleActivity = MessageFactory.Text(titleMessage, titleMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = titleActivity }, cancellationToken);
            }

            return await stepContext.NextAsync(leadInfo.Title, cancellationToken);
        }

        private async Task<DialogTurnResult> GetCreatedDateAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.AnnualRevenue = (string)stepContext.Result;
            double annualRevenue;
            if (!double.TryParse(leadInfo.AnnualRevenue,out annualRevenue))
            {
                var annualRevenueMessage = $"Sorry, annual revenue should be integer. Please provide valid annual revenue for '{ leadInfo.Company }'";
                var annualRevenueActivity = MessageFactory.Text(annualRevenueMessage, annualRevenueMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(annualRevenueActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if (string.IsNullOrEmpty(leadInfo.CreatedDate))
            {
                var createdDateMessage = $"Please provide created date for '{leadInfo.Company}' ?";
                var createdDateActivity = MessageFactory.Text(createdDateMessage, createdDateMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = createdDateActivity }, cancellationToken);
            }

            return await stepContext.NextAsync(leadInfo.CreatedDate, cancellationToken);
        }

        private async Task<DialogTurnResult> GetAnnualRevenueAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            leadInfo.Company = (string)stepContext.Result;

            if (leadInfo.Company.Length > 100)
            {
                var companyTooLongMessage = $"Sorry, length of company in too long. Please provide company with length less than 100";
                var companyTooLongActivity = MessageFactory.Text(companyTooLongMessage, companyTooLongMessage, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(companyTooLongActivity, cancellationToken);
                return await Task.FromResult<DialogTurnResult>(null);
            }
            if ( string.IsNullOrEmpty(leadInfo.AnnualRevenue))
            {
                var annualRevenueMessage = $"Please provide annual revenue for '{leadInfo.Company}' ?";
                var annualRevenueActivity = MessageFactory.Text(annualRevenueMessage, annualRevenueMessage, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = annualRevenueActivity }, cancellationToken);

            }

            return await stepContext.NextAsync(leadInfo.AnnualRevenue, cancellationToken);
        }

        private async Task<DialogTurnResult> GetCompanyAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadInfo = (LeadInfo)stepContext.Options;
            if (string.IsNullOrEmpty(leadInfo.Company))
            {
                var companyMessageText = "For which company you want to predict ?";
                var promptMessage = MessageFactory.Text(companyMessageText, companyMessageText, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            }
            return await stepContext.NextAsync(leadInfo.Company, cancellationToken);
        }
    }
}

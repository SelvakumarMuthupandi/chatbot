// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PreludeSys.CognitiveModels;
using PreludeSys.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSys.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;
        private const string HelpMsgText = "Please drop an email to info@preludesys.com or reach out to us on 949-208-7126.";
        int count = 0;


        public static List<string> Intents = new List<string>();

        public static List<string> LoadJson()
        {
            using (StreamReader r = new StreamReader("AQMD_TestDispatch.json"))
            {

                string json = r.ReadToEnd();
                Intents = JsonConvert.DeserializeObject<List<string>>(json);
                return Intents;
            }
        }





        // Dependency injection uses this constructor to instantiate MainDialog
        public MainDialog(WeatherRecognizer luisRecognizer, WeatherDialog weatherDialog, ILogger<MainDialog> logger, LeadPredictionDialog leadPredictionDialog, LeadPredictionStepByStepDialog leadPredictionStepByStepDialog, LeadRevenueDialog leadRevenueDialog)
            : base(nameof(MainDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(weatherDialog);
            AddDialog(leadPredictionDialog);
            AddDialog(leadPredictionStepByStepDialog);
            AddDialog(leadRevenueDialog);
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                IntroStepAsync,
                ActStepAsync,
                FinalStepAsync
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private Attachment CreateCapabilityCardAttachment()
        {
            var cardResourcePath = "PreludeSys.Cards.capability.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }

        }



        private async Task<DialogTurnResult> IntroStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("LUIS or QnA is not configured.", inputHint: InputHints.IgnoringInput), cancellationToken);
                Logger.LogInformation("LUIS or QnA is not configured.Update the Luis and QNA setting in Appsetting.json");
                return await stepContext.NextAsync(null, cancellationToken);
            }

            if (stepContext.Context.Activity.ChannelId != "alexa")
            {
                // Use the text provided in FinalStepAsync or the default if it is the first time.
                //var messageText = stepContext.Options?.ToString() ?? "How can I help you today?\n To know air quality, type  \"How is air quality in Diamond Bar?\"";
                var messageText = string.Empty; // "To know more about our capability type one of the following \n\n 1. air quality \n 2. weather \n 3. lead prediction \n 4. Reports - top 3 revenue leads \n 5. Reports - best lead sources\"";
                if (stepContext.Context.Activity.ChannelId == "email")
                {
                    messageText = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Top 3 Revenue Leads \n 5. Best Lead Sources\"";
                }
                var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            }
            return await stepContext.NextAsync(null, cancellationToken);
        }

        private async Task<DialogTurnResult> ActStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                // LUIS is not configured, we just run the WeatherDialog path with an empty BookingDetailsInstance.
                return await stepContext.BeginDialogAsync(nameof(WeatherDialog), new WeatherDetails(), cancellationToken);
            }
            // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
            var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(stepContext.Context, cancellationToken);
            var luisLRResult = await _luisRecognizer.Dispatch.RecognizeAsync<LeadReportInfo>(stepContext.Context, cancellationToken);

            switch (luisResult.TopIntent().intent)
            {
                case "Revenue":
                case "LeadSource":
                case "Lead":
                //case Intent.Revenue:
                //case Intent.LeadSource:
                //case Intent.Lead:
                    count = 0;
                    var leadReportParam = new LeadInfo()
                    {
                        // Get Location and Date from the composite entities arrays.
                        AnnualRevenue = !string.IsNullOrEmpty(luisLRResult.IncomeValue) ? luisLRResult.IncomeValue : "0.00",
                        Datacount = !string.IsNullOrEmpty(luisLRResult.RecordCount) ? luisLRResult.RecordCount : "0",
                        Marketingrecords = !string.IsNullOrEmpty(luisLRResult.Leads) ? luisLRResult.Leads : "0",
                        Expression = !string.IsNullOrEmpty(luisLRResult.Expression) ? luisLRResult.Expression : "0",
                        status = !string.IsNullOrEmpty(luisLRResult.Status) ? luisLRResult.Status : "",
                        StartDate = luisLRResult.StartDate.Date,
                        End_Date = luisLRResult.EndDate.Date,
                        AnnualRevenueLabel = !string.IsNullOrEmpty(luisLRResult.Income) ? luisLRResult.Income : "0.00"

                    };
                    if (!string.IsNullOrEmpty(luisLRResult.Source) || (luisResult.TopIntent().intent == "LeadSource"))
                        leadReportParam.Type = "Best Lead Source";
                    else
                        leadReportParam.Type = "Top Revenue";

                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(LeadRevenueDialog), leadReportParam, cancellationToken);
                //  case Intent.I_LeadPrediction:
                case "I_LeadPrediction":
                    var leadInfo = new LeadInfo();
                    if (stepContext.Context.Activity.ChannelId != "alexa")
                    {
                        //return await stepContext.BeginDialogAsync(nameof(LeadPredictionStepByStepDialog), leadInfo, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(LeadPredictionDialog), leadInfo, cancellationToken);
                    }
                    else
                    {
                        return await stepContext.BeginDialogAsync(nameof(LeadPredictionStepByStepDialog), leadInfo, cancellationToken);
                    }

                //case Intent.I_CustomerService:
                case "I_CustomerService":
                    var customerServiceHelpTest = "All our customer service executives are busy, please drop an email to info@preludesys.com or reach out to us on 949-208-7126.";
                    var customerServiceMessage = MessageFactory.Text(customerServiceHelpTest, customerServiceHelpTest, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = customerServiceMessage }, cancellationToken);

                // case Intent.l_Weather:
                case "l_Weather":
                    count = 0;
                    var weatherDetails = new WeatherDetails()
                    {
                        // Get Location and Date from the composite entities arrays.
                        Location = !string.IsNullOrEmpty(luisResult.Location) ? luisResult.Location : "Diamond Bar",
                        ActualDate = luisResult.Date,
                        Type = "Weather"
                    };
                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(WeatherDialog), weatherDetails, cancellationToken);

                //case Intent.l_AirQuality:
                case "l_AirQuality":
                    count = 0;
                    var airQuality = new WeatherDetails()
                    {
                        // Get Location and Date from the composite entities arrays.
                        Location = !string.IsNullOrEmpty(luisResult.Location) ? luisResult.Location : "Diamond Bar",
                        ActualDate = luisResult.Date,
                        Type = "Air Quality"
                    };
                    // Run the Weather Dialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(WeatherDialog), airQuality, cancellationToken);
                // case Intent.q_qna:
                case "q_qna":
                    if (stepContext.Context.Activity.ChannelId == "alexa")
                        return await ProcessSampleQnAForAlexaAsync(stepContext, cancellationToken, count);
                    else
                        await ProcessSampleQnAAsync(stepContext, cancellationToken, count);
                    break;
                default:
                    count++;
                    if (stepContext.Context.Activity.ChannelId == "alexa")
                    {
                        if (count > 3)
                        {
                            var help = "I can't understand you .Please drop an email to info@preludesys.com or reach out to us on 9 0 9 3 9 6 2 0 0 0.";
                            var message = MessageFactory.Text(help, help, InputHints.ExpectingInput);
                            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                        }
                        else
                        {
                            Logger.LogInformation($"Unrecognized intent: {luisResult.TopIntent().intent}.");
                            var defaultMsg = $"Sorry, I didn't get that. Please try asking in a different way. For Example Ask air quality in Diamond Bar.";
                            var message = MessageFactory.Text(defaultMsg, defaultMsg, InputHints.ExpectingInput);
                            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                        }
                    }
                    else
                    {
                        if (count > 3)
                        {
                            await stepContext.Context.SendActivityAsync(MessageFactory.Text($"I can't understand you. Please drop an email to info@preludesys.com or reach out to us on 909-3962-000."), cancellationToken);
                        }
                        else
                        {
                            await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Sorry, I didn't get that. Please try asking in a different way. For Example Ask air quality in Diamond Bar."), cancellationToken);
                        }
                    }
                    break;
            }

            return await stepContext.NextAsync(null, cancellationToken);
        }
        private async Task ProcessSampleQnAAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken, int count)
        {
            Logger.LogInformation("ProcessSampleQnAAsync");
            var results = await _luisRecognizer.SampleQnA.GetAnswersAsync(stepContext.Context);
            if (results.Any())
            {
                switch (results.First().Answer.ToLower())
                {
                    case "help":
                        count = 0;
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text(HelpMsgText), cancellationToken);
                        new DialogTurnResult(DialogTurnStatus.Waiting);
                        break;
                    case "cancel":
                        count = 0;
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text("Cancelling."), cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        break;
                    case "quit":
                        count = 0;
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text("Thank you for using PreludeSys Bot. Please share your feedback at webquery@PreludeSys.gov"), cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        break;
                    default:
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text(results.First().Answer), cancellationToken);
                        break;
                }

            }
            else
            {
                if (count > 2)
                {
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("I can't understand you. Please drop an email to info@preludesys.com or reach out to us on 909-3962-000."), cancellationToken);
                }
                else
                {
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("Sorry, could not find an answer in the Q and A system."), cancellationToken);
                }
            }
        }
        private async Task<DialogTurnResult> ProcessSampleQnAForAlexaAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken, int count)
        {
            Logger.LogInformation("ProcessSampleQnAForAlexaAsync");

            var results = await _luisRecognizer.SampleQnA.GetAnswersAsync(stepContext.Context);
            if (results.Any())
            {
                switch (results.First().Answer.ToLower())
                {
                    case "help":
                        count = 0;
                        var promptMessage = MessageFactory.Text(HelpMsgText, HelpMsgText, InputHints.ExpectingInput);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
                    default:
                        count = 0;
                        var defaultMessage = results.First().Answer;
                        var message = MessageFactory.Text(defaultMessage, defaultMessage, InputHints.ExpectingInput);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
                }
            }
            else
            {
                if (count > 2)
                {
                    var cancelMessage = "I can't understand you. Please drop an email to info@preludesys.com or reach out to us on 949-208-7126.";
                    var quitMessage = MessageFactory.Text(cancelMessage, cancelMessage, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = quitMessage }, cancellationToken);

                }
                else
                {
                    var cancelMessage = "Could not find answer for your questions in our QnA system.";
                    var quitMessage = MessageFactory.Text(cancelMessage, cancelMessage, InputHints.ExpectingInput);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = quitMessage }, cancellationToken);
                }
            }
        }

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Restart the main dialog with a different message the second time around
            //var promptMessage = "Is there anything else I can help you with?";

            //var messageText =  "To know more about our capability type one of the following \n\n 1. air quality \n 2. weather \n 3. lead prediction \n 4. Reports - top 3 revenue leads \n 5. Reports - best lead sources\"";
            //var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
            //await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            //await stepContext.Context.SendActivityAsync(messageText);

            var promptMessage1 = string.Empty;
            return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage1, cancellationToken);
        }

    }
}

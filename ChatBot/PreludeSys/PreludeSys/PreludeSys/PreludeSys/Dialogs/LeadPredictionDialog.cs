using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PreludeSys.Cards;
using PreludeSys.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PreludeSys.Dialogs
{
    public class LeadPredictionDialog : CancelAndHelpDialog
    {
        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;

        static string AdaptivePromptId = "LeadPredictionDialogCard";
        public LeadPredictionDialog(WeatherRecognizer luisRecognizer, ILogger<MainDialog> logger) : base(nameof(LeadPredictionDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;
            AddDialog(new AdaptiveCardPrompt(AdaptivePromptId));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new WaterfallDialog(nameof(LeadPredictionDialog), new WaterfallStep[]
            {
                GetLeadInformationAsync,
                ValidateLeadAsync,
                ProcessLeadInformationAsync
            }));
            // The initial child Dialog to run.
            InitialDialogId = nameof(LeadPredictionDialog);
        }

        private async Task<DialogTurnResult> ValidateLeadAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var leadString = stepContext.Result.ToString();
            var lead = JsonConvert.DeserializeObject<LeadInfo>(leadString);

            if (string.IsNullOrEmpty(lead.Company))
            {
                
                var companyMessageText = "For which company you want to predict ?";
                //await stepContext.Context.SendActivityAsync(MessageFactory.Text(companyMessageText));
                //return Dialog.EndOfTurn;
                var promptMessage = MessageFactory.Text(companyMessageText, companyMessageText, InputHints.IgnoringInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            }
            if (string.IsNullOrEmpty(lead.AnnualRevenue))
            {
                var annualRevenueMessage = $"Please provide annual revenue for '{lead.Company}' ?";
                var annualRevenueActivity = MessageFactory.Text(annualRevenueMessage, annualRevenueMessage, InputHints.IgnoringInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = annualRevenueActivity }, cancellationToken);
            }
            if (string.IsNullOrEmpty(lead.CreatedDate))
            {
                var createdDateMessage = $"Please provide created date for '{lead.Company}' ?";
                var createdDateActivity = MessageFactory.Text(createdDateMessage, createdDateMessage, InputHints.IgnoringInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = createdDateActivity }, cancellationToken);
            }

            if (string.IsNullOrEmpty(lead.Subject))
            {
                var subjectMessage = $"Please provide calling mode for '{lead.Company}' ?";
                var subjectActivity = MessageFactory.Text(subjectMessage, subjectMessage, InputHints.IgnoringInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = subjectActivity }, cancellationToken);
            }


            return await stepContext.NextAsync(lead, cancellationToken);
        }

        private async Task<DialogTurnResult> ProcessLeadInformationAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var lead = (LeadInfo) stepContext.Result;

            var scoreLabel = string.Empty;
            var scorePercentage = string.Empty;
            JObject jsonResponse = JObject.Parse(await LeadService.GetLeadPrediction(lead));
            foreach (KeyValuePair<string, JToken> app in jsonResponse)
            {
                var appName = app.Key;
                foreach (JToken scorePrediction in app.Value["output1"].First)
                {
                    if (string.IsNullOrEmpty(scoreLabel))
                    {
                        var scoreKey = scorePrediction;
                        scoreLabel = scorePrediction.First.Value<string>();
                        scorePercentage = scorePrediction.Next.First.Value<string>();
                    }
                }
            }

            var card = new AdaptiveCard("1.1");
            
            //await stepContext.Context.SendActivityAsync(MessageFactory.Text("Scored Labels : " + scoreLabel));
            //await stepContext.Context.SendActivityAsync(MessageFactory.Text("Probability for Scored Label : " + scorePercentage));

            GetLeadPredictionCard(card, scoreLabel, scorePercentage);
            var adaptiveCardAttachment = new Attachment()
            {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(card)),
            };
            await stepContext.Context.SendActivityAsync(MessageFactory.Attachment(adaptiveCardAttachment), cancellationToken);

            //var messageText = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Reports - Top 3 Revenue Leads \n 5. Reports - Best Lead Sources\n";
            //await stepContext.Context.SendActivityAsync(messageText);

            var capabilityCard = CreateCapabilityCardAttachment();
            var capabilityresponse = MessageFactory.Attachment(capabilityCard);
            await stepContext.Context.SendActivityAsync(capabilityresponse, cancellationToken);


            return await stepContext.EndDialogAsync();

        }

        private Attachment CreateCapabilityCardAttachment()
        {
            var cardResourcePath = "PreludeSys.Cards.capability.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }

        }


        private static void GetLeadPredictionCard(AdaptiveCard card, string scoreLabel, string scorePercentage)
        {

            var headerContainer = new AdaptiveContainer();

            var header = new AdaptiveColumnSet();
            headerContainer.Items.Add(header);
            card.Body.Add(headerContainer);
            var headerColumn = new AdaptiveColumn();
            var textHeader = new AdaptiveTextBlock();
            textHeader.Size = AdaptiveTextSize.Medium;
            textHeader.Weight = AdaptiveTextWeight.Bolder;
            textHeader.Text = "Lead Prediction";

            headerColumn.Width = AdaptiveColumnWidth.Auto;
            headerColumn.Items.Add(textHeader);
            header.Columns.Add(headerColumn);
            headerContainer.Bleed = true;


            var TitleContainer = new AdaptiveContainer();
            var title = new AdaptiveColumnSet();
            TitleContainer.Items.Add(title);
            card.Body.Add(TitleContainer);
            var TitleColumn = new AdaptiveColumn();
            var emptyText = new AdaptiveTextBlock();
            emptyText.Text = " ";

            var textTitle = new AdaptiveTextBlock();
            textTitle.Weight = AdaptiveTextWeight.Bolder;
            textTitle.Text = "Scored Labels : " + scoreLabel;

            var scorePercentageBlock = new AdaptiveTextBlock();
            scorePercentageBlock.Weight = AdaptiveTextWeight.Bolder;
            scorePercentageBlock.Text = "Probability for Scored Label : " + scorePercentage;


            title.Columns.Add(TitleColumn);
            TitleColumn.Width = AdaptiveColumnWidth.Auto;
            TitleColumn.Items.Add(emptyText);
            TitleColumn.Items.Add(textTitle);
            TitleColumn.Items.Add(scorePercentageBlock);

            var ChartContainer = new AdaptiveContainer();
            var chart = new AdaptiveColumnSet();
            ChartContainer.Items.Add(chart);
            ChartContainer.Separator = true;
            card.Body.Add(ChartContainer);

        }


        private static Attachment CreateAdaptiveCardAttachment(string filePath)
        {
            var adaptiveCardJson = File.ReadAllText(filePath);
            var adaptiveCardAttachment = new Attachment()
            {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = JsonConvert.DeserializeObject(adaptiveCardJson),
            };
            return adaptiveCardAttachment;
        }

        private async Task<DialogTurnResult> GetLeadInformationAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var lead = (LeadInfo)stepContext.Options;
            //var leadResult = (LeadInfo)stepContext.Result;
            if (string.IsNullOrEmpty(lead.Company))
            {
                var opts = new PromptOptions
                {
                    Prompt = new Activity
                    {
                        Attachments = new List<Attachment>() { CreateAdaptiveCardAttachment(@".\Cards\LeadInfo.json") },
                        Type = ActivityTypes.Message
                    }
                };

                return await stepContext.PromptAsync(AdaptivePromptId, opts , cancellationToken);
            }
            return await stepContext.NextAsync((LeadInfo)stepContext.Result, cancellationToken);
        }
    }
}

﻿using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Recognizers.Text.DataTypes.TimexExpression;
using Newtonsoft.Json;
using PreludeSys.CognitiveModels;
using PreludeSys.Dto;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Constants = Microsoft.Recognizers.Text.DataTypes.TimexExpression.Constants;

namespace PreludeSys.Dialogs
{
    public class LeadRevenueDialog : CancelAndHelpDialog
    {
        private const string MoreDetailMsgText = "Do you want more details?";
        private readonly WeatherRecognizer _luisRecognizer;
        protected readonly ILogger Logger;
        private readonly IConfiguration configuration;


        public LeadRevenueDialog(WeatherRecognizer luisRecognizer, ILogger<MainDialog> logger)
         : base(nameof(LeadRevenueDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new DateResolverDialog());
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                //LocationStepAsync,
                //DateStepAsync,
                InitialInformtionAsync
                //,
                //FinalStepAsync,
            }));
            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private Attachment CreateCapabilityCardAttachment()
        {
            var cardResourcePath = "PreludeSys.Cards.capability.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }

        }


        private async Task<DialogTurnResult> InitialInformtionAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var LeadReportDetails = (LeadInfo)stepContext.Options;
            var LeadReportResults = (LeadInfo)stepContext.Result;
            if (stepContext.Options is LeadInfo result)
            {
                DataService dataService = new DataService(configuration);
                AdaptiveCard card = await dataService.GetTopLeadReportByRevenueAsync(LeadReportDetails.Datacount, LeadReportDetails.Type, LeadReportDetails.StartDate, LeadReportDetails.End_Date, LeadReportDetails.AnnualRevenue, LeadReportDetails.status, _luisRecognizer.BaseApiPath);
                if (stepContext.Context.Activity.ChannelId != "alexa" && stepContext.Context.Activity.ChannelId != "email")
                {
                    if (card.Body.Count != 0)
                    {
                        var adaptiveCardAttachment = new Attachment()
                        {
                            ContentType = "application/vnd.microsoft.card.adaptive",
                            Content = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(card)),
                        };

                        await stepContext.Context.SendActivityAsync(MessageFactory.Attachment(adaptiveCardAttachment), cancellationToken);

                        //var messageText1 = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Reports - Top 3 Revenue Leads \n 5. Reports - Best Lead Sources\n";
                        //await stepContext.Context.SendActivityAsync(MessageFactory.Text(messageText1));

                        var capabilityCard = CreateCapabilityCardAttachment();
                        var capabilityresponse = MessageFactory.Attachment(capabilityCard);
                        await stepContext.Context.SendActivityAsync(capabilityresponse, cancellationToken);


                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    }
                    else
                    {
                        await stepContext.Context.SendActivityAsync($"We don't find any {LeadReportDetails.Type} Data for Revenue greater than { LeadReportDetails.AnnualRevenue}. Ask  for other different range.");
                        //var messageText2 = "To know more about our capability type one of the following \n\n 1. Air Quality \n 2. Weather \n 3. Lead Prediction \n 4. Reports - Top 3 Revenue Leads \n 5. Reports - Best Lead Sources\n";
                        //await stepContext.Context.SendActivityAsync(messageText2);

                        var capabilityCard = CreateCapabilityCardAttachment();
                        var capabilityresponse = MessageFactory.Attachment(capabilityCard);
                        await stepContext.Context.SendActivityAsync(capabilityresponse, cancellationToken);


                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    }
                }
            }
            //var messageText =  "To know more about our capability type one of the following \n\n 1. air quality \n 2. weather \n 3. lead prediction \n 4. Reports - top 3 revenue leads \n 5. Reports - best lead sources\"";
            //var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
            //await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            //await stepContext.Context.SendActivityAsync(messageText);

            return await stepContext.NextAsync(LeadReportDetails, cancellationToken);
        }

        //private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        //{
        //    var weatherDetails = (WeatherDetails)stepContext.Options;
        //    if (stepContext.Context.Activity.ChannelId == "alexa" || stepContext.Context.Activity.ChannelId == "email")
        //    {
        //        // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
        //        var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(stepContext.Context, cancellationToken);
        //        if (luisResult.TopIntent().intent == Intent.I_Yes)
        //        {
        //            DataService action = new DataService(configuration);
        //            var prompt = await action.GetDetailsAsync(weatherDetails.Location, weatherDetails.ActualDate, weatherDetails.Date, weatherDetails.Type, _luisRecognizer.BaseApiPath, _luisRecognizer.APIXUKey);
        //            var message = MessageFactory.Text(prompt, prompt, InputHints.ExpectingInput);
        //            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
        //        }
        //        else if (luisResult.TopIntent().intent == Intent.I_No)
        //        {
        //            var prompt = "What else can I do for you?";
        //            var message = MessageFactory.Text(prompt, prompt, InputHints.ExpectingInput);
        //            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = message }, cancellationToken);
        //        }
        //        else
        //        {
        //            var notFountMsg = $"No information found for your query. Ask for other city or time period.";
        //            var promptMessage = MessageFactory.Text(notFountMsg, notFountMsg, InputHints.ExpectingInput);
        //            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
        //        }
        //    }
        //    else
        //    {
        //        // Call LUIS and gather any potential Weather details. (Note the TurnContext has the response to the prompt.)
        //        var luisResult = await _luisRecognizer.Dispatch.RecognizeAsync<WeatherInfo>(stepContext.Context, cancellationToken);
        //        if (luisResult.TopIntent().intent == Intent.I_Yes)
        //        {
        //            await ProcessWeatherAsync(stepContext.Context, weatherDetails, cancellationToken);
        //        }
        //        else if (luisResult.TopIntent().intent == Intent.I_No)
        //        {
        //            await stepContext.Context.SendActivityAsync($"What else can I do for you?");
        //        }
        //        else
        //        {
        //            await stepContext.Context.SendActivityAsync($"No information found for your query. Ask for other city or time period.");
        //        }
        //        return await stepContext.EndDialogAsync(null, cancellationToken);
        //    }
        //}


    }
}

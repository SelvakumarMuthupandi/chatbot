using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using PreludeSys.Dto;

namespace PreludeSys
{
    public class LeadService
    {
        public static async Task<bool> IsValidLead(ITurnContext turnContext, LeadInfo lead)
        {
            DateTime createdDate = DateTime.MinValue;
            Double annualRevenue;

            if (string.IsNullOrEmpty(lead.Company))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid company name and try again."));
                return false;
            }
            if (!double.TryParse(lead.AnnualRevenue, out annualRevenue))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid annual revenue and try again."));
                return false;
            }

            if (string.IsNullOrEmpty(lead.CreatedDate) || !DateTime.TryParse(lead.CreatedDate, out createdDate))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid created date and try again."));
                return false;
            }

            if (string.IsNullOrEmpty(lead.Subject))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid subject and try again."));
                return false;
            }
            if (string.IsNullOrEmpty(lead.TimeZone))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid time zone and try again."));
                return false;
            }

            if (string.IsNullOrEmpty(lead.Title))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid title and try again."));
                return false;
            }
            if (string.IsNullOrEmpty(lead.ValidationLink))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Please provide valid validation link and try again."));
                return false;
            }
            return true;
        }

        public static async Task<string> GetLeadPrediction(LeadInfo leadInfo)
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {
                    Inputs = new Dictionary<string, List<Dictionary<string, string>>>() {
                        {
                            "input1",
                            new List<Dictionary<string, string>>(){new Dictionary<string, string>(){
                                            {
                                                "Company", leadInfo.Company
                                            },
                                            {
                                                "Annual Revenue", leadInfo.AnnualRevenue
                                            },
                                            {
                                                "Created Date", leadInfo.CreatedDate 
                                            },
                                            {
                                                "Title",leadInfo.Title
                                            },
                                            {
                                                "Subject",  leadInfo.Subject
                                            },
                                            {
                                                "TimeZone", leadInfo.TimeZone
                                            },
                                            {
                                                "Number Validation", leadInfo.NumberValidation
                                            },
                                            {
                                                "Validation Link", leadInfo.ValidationLink
                                            },
                                }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };

                const string apiKey = "ldhB0otjE6t2OQtm+bfdFGyY2cTZx4CEwDKJL79xbKaHR5S9wy3yIn/yFkKK8dN7C5DaGATjh1avBZoG9gCr7g=="; // Replace this with the API key for the web service
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/8a6f82f023f44b8398e8dcdca2eee464/services/79bb36f046c445fd93bad4639f041efb/execute?api-version=2.0&format=swagger");


                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);
                string result = string.Empty;
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                return result;
            }
        }
    }
}

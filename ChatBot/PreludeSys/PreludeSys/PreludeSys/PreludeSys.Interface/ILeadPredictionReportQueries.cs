﻿using PreludeSys.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PreludeSys.Dto.ViewModel;
namespace PreludeSys.Interface
{
    public interface ILeadPredictionReportQueries
    {
        
       Task<IEnumerable<LeadPredictionOutPut>> GetLeadInfo(int count_of_Records_Fetched,DateTime startDate, DateTime endDate, double annualRevenueRange, string status);
        Task<IEnumerable<LeadSourceOutPut>> GetBestLeadSourceInfo();
       }
}

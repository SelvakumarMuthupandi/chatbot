
using PreludeSys.Dto;
using System.Threading.Tasks;


namespace PreludeSys.Interface
{
    public interface IAirQualityCommand
    {
        Task<AirQuality> SaveAirQualityAsync(AirQuality airQualityDto);
    }
}

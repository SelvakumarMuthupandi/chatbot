using Newtonsoft.Json;
using PreludeSys.Weather.Yahoo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace PreludeSys.Weather
{
    public interface IRepository
    {
        WeatherModel GetWeatherData(string key, GetBy getBy, string value, Days ForecastOfDays);
        WeatherModel GetWeatherData(string key, string place, string StartDate, string EndDate);
        WeatherModel GetWeatherDataByLatLong(string key, string latitude, string longitude, Days ForecastOfDays);
        WeatherModel GetWeatherDataByAutoIP(string key, Days ForecastOfDays);
        WeatherModel GetWeatherData(string key, GetBy getBy, string value);
        WeatherModel GetWeatherDataByLatLong(string key, string latitude, string longitude);
        WeatherModel GetWeatherDataByAutoIP(string key);
    }
    public class Repository : IRepository
    {
        private string APIURL = "https://api.weatherstack.com/historical";

        // yahoo weather API request params
        const string yahooAPIURL = "https://weather-ydn-yql.media.yahoo.com/forecastrss";
        const string cAppID = "wejZsj6s";
        const string cConsumerKey = "dj0yJmk9eFluMzBHeW5VeDEyJmQ9WVdrOWQyVnFXbk5xTm5NbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTZm";
        const string cConsumerSecret = "0f147e8e8eb5d62d067a2b45373b5568f2d62ea0";
        const string cOAuthVersion = "1.0";
        const string cOAuthSignMethod = "HMAC-SHA1";
        const string clocation = "sunnyvale";  // Amsterdam, The Netherlands
        const string cUnitID = "u=f";           // Metric units
        const string cFormat = "json";
        string weatherCondition = "http://l.yimg.com/a/i/us/we/52/code.gif";
        //http://l.yimg.com/a/i/us/nws/weather/gr/21d.png

        #region Get Weather Forecast Data

        public WeatherModel GetWeatherData(string key, GetBy getBy, string value, Days ForecastOfDays)
        {
            string requestUrl = RequestBuilder.PrepareRequest(MethodType.Forecast, key, getBy, value, ForecastOfDays);
            return GetData(yahooAPIURL + (requestUrl + "&" + cUnitID + "&format=" + cFormat), getBy, value, (int)ForecastOfDays);
        }

        public WeatherModel GetWeatherData(string key, string place, string StartDate, string EndDate)
        {
            return GetData(APIURL + RequestBuilder.PrepareRequest(key, place, StartDate, EndDate));
        }


        public WeatherModel GetWeatherDataByLatLong(string key, string latitude, string longitude, Days ForecastOfDays)
        {
            // return GetData(APIURL + RequestBuilder.PrepareRequestByLatLong(MethodType.Forecast, key, latitude, longitude, ForecastOfDays));
            string requestUrl = RequestBuilder.PrepareRequestByLatLong(MethodType.Forecast, key, latitude, longitude, ForecastOfDays);
            return GetData(yahooAPIURL + (requestUrl + "&" + cUnitID + "&format=" + cFormat), GetBy.LatLong, longitude, (int)ForecastOfDays);

        }

        public WeatherModel GetWeatherDataByAutoIP(string key, Days ForecastOfDays)
        {
            return GetData(APIURL + RequestBuilder.PrepareRequestByAutoIP(MethodType.Forecast, key, ForecastOfDays));

        }
        #endregion

        #region Get Weather Current Data

        public WeatherModel GetWeatherData(string key, GetBy getBy, string value)
        {
            return GetData(APIURL + RequestBuilder.PrepareRequest(MethodType.Current, key, getBy, value));
        }


        public WeatherModel GetWeatherDataByLatLong(string key, string latitude, string longitude)
        {
            return GetData(APIURL + RequestBuilder.PrepareRequestByLatLong(MethodType.Current, key, latitude, longitude));
        }

        public WeatherModel GetWeatherDataByAutoIP(string key)
        {
            return GetData(APIURL + RequestBuilder.PrepareRequestByAutoIP(MethodType.Current, key));
        }

        #endregion

        private WeatherModel GetData(string url)
        {
            string urlParameters = "";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call!
            var content = response.Content.ReadAsStringAsync().Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return JsonConvert.DeserializeObject<WeatherModel>(response.Content.ReadAsStringAsync().Result);
            }
            else
            {
                return new WeatherModel();
            }
        }

        #region Yahoo API
        // end _get_timestamp
        string GetTimeStamp()
        {
            TimeSpan lTS = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64(lTS.TotalSeconds).ToString();
        }

        string GetNonce()
        {
            return Convert.ToBase64String(
             new ASCIIEncoding().GetBytes(
              DateTime.Now.Ticks.ToString()
             )
            );
        }

        // NOTE: whenever the value of a parameter is changed, say cUnitID "u=c" => "location=sunnyvale,ca"
        // The order in lSign needs to be updated, i.e. re-sort lSign
        // Please don't simply change value of any parameter without re-sorting.
        string GetAuth(GetBy getBy, string value)
        {
            string lNonce = GetNonce();
            string lTimes = GetTimeStamp();
            string lCKey = string.Concat(cConsumerSecret, "&");
            string lSign = string.Empty;
            // location search
            lSign = string.Format(  // note the sort order !!!
                "format={0}&" +
                "location={1}&" +
                "oauth_consumer_key={2}&" +
                "oauth_nonce={3}&" +
                "oauth_signature_method={4}&" +
                "oauth_timestamp={5}&" +
                "oauth_version={6}&" +
                "{7}",
                cFormat,
                Uri.EscapeDataString(value),
                cConsumerKey,
                lNonce,
                cOAuthSignMethod,
                lTimes,
                cOAuthVersion,
                cUnitID
            );

            lSign = string.Concat("GET&", Uri.EscapeDataString(yahooAPIURL), "&", Uri.EscapeDataString(lSign));

            using (var lHasher = new HMACSHA1(Encoding.ASCII.GetBytes(lCKey)))
            {
                lSign = Convert.ToBase64String(
                 lHasher.ComputeHash(Encoding.ASCII.GetBytes(lSign))
                );
            }
            return "OAuth " +
                    "oauth_consumer_key=\"" + cConsumerKey + "\", " +
                    "oauth_nonce=\"" + lNonce + "\", " +
                    "oauth_timestamp=\"" + lTimes + "\", " +
                    "oauth_signature_method=\"" + cOAuthSignMethod + "\", " +
                    "oauth_signature=\"" + lSign + "\", " +
                    "oauth_version=\"" + cOAuthVersion + "\"";
        }
        /// <summary>
        /// Gets the weather info from yahoo API
        /// </summary>
        /// <param name="url">Request URL</param>
        /// <param name="getBy">Search BY ZIP/Name/LatLong</param>
        /// <param name="value">Search term</param>
        /// <param name="foreCastDays">No of days</param>
        /// <returns></returns>
        private WeatherModel GetData(string url, GetBy getBy, string value, int foreCastDays)
        {
            string urlParameters = "";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Add("X-Yahoo-App-Id", cAppID);
            client.DefaultRequestHeaders.Add("Authorization", GetAuth(getBy, value));

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var yahooModel = response.Content.ReadAsAsync<WeatherModelYahoo>().Result;
                    // convert the Yahoo object to Weather Model
                    return MapToWeatherModel(yahooModel, foreCastDays);
                }
                catch (Exception)
                {
                    return new WeatherModel();
                }
            }
            else
            {
                return new WeatherModel();
            }
        }

        /// <summary>
        /// Converts the Unix timestamp to local date time
        /// Yahoo API return UNIX timestamp
        /// </summary>
        /// <param name="unixTimeStamp">uixtimestamp</param>
        /// <returns>string</returns>
        private string UnixTimeStampToDateTime(string unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(unixTimeStamp)).ToLocalTime();
            return dtDateTime.ToString();
        }

        /// <summary>
        /// Get the wind direction
        /// </summary>
        /// <param name="windDirection"></param>
        /// <returns></returns>
        private string GetWindDirection(string windDirection)
        {
            int winddirection = 0;
            bool result = int.TryParse(windDirection, out winddirection);
            string direction = string.Empty;
            switch (winddirection)
            {
                case 0:
                    direction = "N";
                    break;
                case 90:
                    direction = "E";
                    break;
                case 180:
                    direction = "S";
                    break;
                case 270:
                    direction = "W";
                    break;
                case 360:
                    direction = "N";
                    break;
            }
            if (winddirection < 90)
            {
                direction = "NE";
            }
            else if (winddirection < 180)
            {
                direction = "SE";
            }
            else if (winddirection < 270)
            {
                direction = "SW";
            }
            else if (winddirection < 360)
            {
                direction = "NW";
            }
            return direction;
        }

        /// <summary>
        /// Converts the yahoo weather model to WeatherModel
        /// </summary>
        /// <param name="weatherModelYahoo">Yahoo Weather API model</param>
        /// <returns>WeatherModel</returns>
        private WeatherModel MapToWeatherModel(WeatherModelYahoo weatherModelYahoo, int foreCastDays)
        {
            WeatherModel model = new WeatherModel();
            model.location = new Location()
            {
                country = weatherModelYahoo.location.country,
                name = weatherModelYahoo.location.city,
                lat = weatherModelYahoo.location.lat,
                region = weatherModelYahoo.location.region,
                lon = weatherModelYahoo.location.longitude,
                timezone_id = weatherModelYahoo.location.timezone_id,
                localtime = DateTime.Now.ToLongTimeString()
            };
            model.current = new Current()
            {
                temperature = weatherModelYahoo.current_observation.condition.temperature,
                wind_speed = weatherModelYahoo.current_observation.wind.speed,
                wind_dir = GetWindDirection(weatherModelYahoo.current_observation.wind.direction),
                wind_degree = weatherModelYahoo.current_observation.wind.chill,
                pressure = weatherModelYahoo.current_observation.atmosphere.pressure,
                humidity = weatherModelYahoo.current_observation.atmosphere.humidity,
                feelslike = weatherModelYahoo.current_observation.wind.chill,
                last_updated = UnixTimeStampToDateTime(weatherModelYahoo.current_observation.pubDate),
                condition = new Condition()
                {
                    text = weatherModelYahoo.current_observation.condition.text,
                    code = weatherModelYahoo.current_observation.condition.code,
                    icon = weatherCondition.Replace("code", weatherModelYahoo.current_observation.condition.code.ToString())

                }
            };
            astro astro = new astro()
            {
                sunrise = weatherModelYahoo.current_observation.astronomy.sunrise,
                sunset = weatherModelYahoo.current_observation.astronomy.sunset
            };

            List<Forecastday> forecastdays = new List<Forecastday>();
            foreach (var tempForecast in weatherModelYahoo.forecasts.Take(foreCastDays))
            {
                forecastdays.Add(new Forecastday()
                {
                    date = UnixTimeStampToDateTime(tempForecast.date),
                    // date_epoch = tempForecast.day,
                    day = new Day()
                    {
                        mintemp_f = tempForecast.low,
                        maxtemp_f = tempForecast.high,
                        condition = new Condition()
                        {
                            text = tempForecast.text,
                            code = tempForecast.code,
                            icon = weatherCondition.Replace("code", tempForecast.code.ToString())
                        }
                    },
                    astro = astro,
                    hour = new List<Hour>()
                });
            }
            model.forecast = new Forecast()
            {
                forecastday = forecastdays
            };
            return model;
        }

        #endregion
    }
}

using Newtonsoft.Json;
using System.Collections.Generic;

namespace PreludeSys.Weather.Yahoo
{
    public class Location
    {
        public string city { get; set; }
        public string region { get; set; }
        public int woeid { get; set; }
        public string country { get; set; }
        public double lat { get; set; }
        [JsonProperty(PropertyName = "long")]
        public double longitude { get; set; }
        public string timezone_id { get; set; }
    }

    public class Wind
    {
        public int chill { get; set; }
        public string direction { get; set; }
        public double speed { get; set; }
    }

    public class Atmosphere
    {
        public int humidity { get; set; }
        public double visibility { get; set; }
        public double pressure { get; set; }
        public int rising { get; set; }
    }

    public class Astronomy
    {
        public string sunrise { get; set; }
        public string sunset { get; set; }
    }

    public class Condition
    {
        public string text { get; set; }
        public int code { get; set; }
        public int temperature { get; set; }
    }

    public class Current_Observation
    {
        public Wind wind { get; set; }
        public Atmosphere atmosphere { get; set; }
        public Astronomy astronomy { get; set; }
        public Condition condition { get; set; }
        public string pubDate { get; set; }
    }

    public class Forecasts
    {
        public string day { get; set; }
        public string date { get; set; }
        public int low { get; set; }
        public int high { get; set; }
        public string text { get; set; }
        public int code { get; set; }
    }

    public class WeatherModelYahoo
    {
        public Location location { get; set; }
        public Current_Observation current_observation { get; set; }
        public List<Forecasts> forecasts { get; set; }
    }

}

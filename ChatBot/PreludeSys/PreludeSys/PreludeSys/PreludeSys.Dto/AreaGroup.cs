using System;
using System.Collections.Generic;
using System.Text;

namespace PreludeSys.Dto
{
    public class AreaGroup
    {
        public int AreaGroupId { get; set; }
        public string AreaGroupName { get; set; }

        public int StateId { get; set; }
    }
}

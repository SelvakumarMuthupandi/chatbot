using System;

namespace PreludeSys.Dto
{
    public class AirQuality
    {
        public int AirQualityId { get; set; }
        public int AreaId { get; set; }
        public double AQI { get; set; }
        public DateTime ForecastDateTime { get; set; }
        public string AQICategory { get; set; }
        public string Pollutant { get; set; }
        public double PMTwoFive { get; set; }
        public double PMTen { get; set; }
        public double Ozone { get; set; }
        public double NitorgenDiOxide { get; set; }
        public double CarbonMonoOxide { get; set; }
        public double SulfurDiOxide { get; set; }
        public string ZipCode { get; set; }

        public int CityId { get; set; }
        public int CountyId { get; set; }
        public int StateId { get; set; }
    }
}

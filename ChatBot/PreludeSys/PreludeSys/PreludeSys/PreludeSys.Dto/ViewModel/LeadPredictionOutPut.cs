﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreludeSys.Dto.ViewModel
{
   public class LeadPredictionOutPut
    {

        public string Company { get; set; }

        public string AnnualRevenue { get; set; }

      
        public string Forcestatus { get; set; }

        public string CreatedByReplist { get; set; }

        public string CreatedDate { get; set; }

        public int FollowUpCount { get; set; }

    }
}

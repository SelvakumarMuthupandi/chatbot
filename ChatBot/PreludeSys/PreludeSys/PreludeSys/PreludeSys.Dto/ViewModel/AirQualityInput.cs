using System;
using System.Collections.Generic;
using System.Text;

namespace PreludeSys.Dto.ViewModel
{
    public class AirQualityInput
    {
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Duration { get; set; }
    }
}

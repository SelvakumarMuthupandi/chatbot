﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreludeSys.Dto.ViewModel
{
    public class LeadSourceOutPut
    {

        public string Leadscount { get; set; }

        public string Validationlink { get; set; }

        public string SuccessLeads { get; set; }

        public string InTop3 { get; set; }

        public string Highauthorities { get; set; }

        public string Phonevalidpercent { get; set; }

        public string Emailvalidpercent { get; set; }



    }
}


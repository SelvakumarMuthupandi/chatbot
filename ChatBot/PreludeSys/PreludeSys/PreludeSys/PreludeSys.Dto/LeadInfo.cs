using System;
using System.ComponentModel;

namespace PreludeSys.Dto
{
    public class LeadInfo
    {
        public string action { get; set; }
        public string Company { get; set; }
        public string AnnualRevenue { get; set; }

        public string AnnualRevenueLabel { get; set; }

        public string CreatedDate { get; set; }
        public string Title { get; set; }
        public string Subject { get; set; }
        public string TimeZone { get; set; }
        public string NumberValidation { get; set; }
        public string ValidationLink { get; set; }
        
        public string Datacount { get; set; }
        public string Type { get; set; }

        public string Expression { get; set; }
        public string Marketingrecords { get; set; }

        public int Status_Od_Lead { get; set; }
        public string Lead_Rep { get; set; }
        public int Follow_Upcount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime End_Date { get; set; }

        public string status { get; set; }

    }



}

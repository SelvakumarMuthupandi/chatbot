namespace PreludeSys.Dto
{
    public class AlexaCardContent 
    {
        public string title { get; set; }
        public string text { get; set; }
    }
}

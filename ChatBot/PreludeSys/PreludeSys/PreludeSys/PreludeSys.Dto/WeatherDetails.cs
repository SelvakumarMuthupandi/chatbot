namespace PreludeSys.Dto
{
    public class WeatherDetails
    {
        public string Location { get; set; }
        public string Date { get; set; }
        public string ActualDate { get; set; }
        public string Type { get; set; }
    }
}

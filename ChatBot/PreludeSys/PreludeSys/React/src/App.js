import React, { Component } from "react";
import MinimizableWebChat from "./MinimizableWebChat";
import "./App.css";
import WebPageBackground from "./PreludeBotMainPage.png";

class App extends Component {
  render() {
    return (
      <div className="App">
        <img alt="product background" src={WebPageBackground} />
        <MinimizableWebChat />
      </div>
    );
  }
}

export default App;

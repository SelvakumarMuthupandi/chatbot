using System;
using System.Linq;
using System.Net.Http;
//using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using ExcelDataReader;
using System.Data;
using System.Text.RegularExpressions;

namespace Services_Setup.Data
{
    public class LUIS_ConfiguratorService
    {
        static string kb = "";

        static List<string> IntentList = new List<string>();

        static List<EntityList> EntityLists = new List<EntityList>();
        static List<EntityList> EntityTypeList = new List<EntityList>();
        static string LUIS_authoringKey = "c5afaaf56dce46f2b4bc2e93128c09ec";

        static string LUIS_appName = "Lead Report App";
        static string LUIS_appCulture = "en-us";
        static string LUIS_versionId = "Dispatch"; // "0.1";
       public static  string AppId = "";
        static int BatchCount = 100;


        public class LUISqueryobj
        {
            public string Intent { set; get; }
            public string Utterance { set; get; }
            public string Entity { set; get; }
            public string EntityType { set; get; }
          
        }

        public class EntityList
        {
            public string EnityName { set; get; }
            public string EntityType { set; get; }
        }

        public class Utterance
        {
            public string text { get; set; }
            //public Int64 ExampleId { get; set; }
            public string intentName { get; set; }
            public List<EntityLabel> entityLabels { get; set; }
        }
        public class EntityLabel
        {
            public string entityName { get; set; }
            public int startCharIndex { get; set; }
            public int endCharIndex { get; set; }
        }
        public class RootObject
        {
            public string converted_date { get; set; }
            public List<Utterance> utterances { get; set; }
        }
        public class SplittedUtterance
        {
            public List<Utterance> utterances { get; set; }
        }

        public static void PrepareData(Stream file)
        {
            try
            {
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateReader(file))
                {
                    var x_config = new ExcelDataSetConfiguration();
                    x_config.ConfigureDataTable = tableReader => new ExcelDataTableConfiguration() { UseHeaderRow = true };
                    DataSet result = excelReader.AsDataSet(x_config);

                    var resultobjquery = from r in result.Tables[0].AsEnumerable()
                                         select new LUISqueryobj
                                         {
                                             Intent = r.Field<string>("Action Category"),
                                             Utterance = r.Field<string>("User Query"),
                                             Entity = Convert.ToString( r.Field<object>("Action Object")),
                                             EntityType = r.Field<string>("Object Type")

                                         };

                    var resultobj = resultobjquery.Distinct().ToList();


                    IntentList = resultobj.Select(x => x.Intent).OfType<string>().Distinct().ToList();

                   //  EntityTypeList = resultobj.Select(x => x.EntityType).OfType<string>().Distinct().ToList();
                    var EntityListqry = from res in resultobj
                                        select new EntityList
                                        {
                                            EnityName = res.Entity,
                                            EntityType = res.EntityType
                                        };

                    EntityLists = EntityListqry
                        .GroupBy(p => p.EnityName)
                        .Select(g => g.First())
                         .ToList();
                    EntityTypeList =  EntityListqry
                        .GroupBy(p => p.EntityType)
                        .Select(g => g.First())
                         .ToList();
                    string JSONstring = @" 
                       {
                         'converted_date': '" + DateTime.UtcNow.ToString() + @"',
                         'utterances': [";



                    foreach (LUISqueryobj row in resultobj)
                    {
                        if (row.Utterance is null)
                            goto EndText;
                        if (JSONstring.Contains("text"))
                            JSONstring = JSONstring + ",";
                       // var utterwords = row.Utterance.ToString().ToUpperInvariant().Split(" ");
                         var utterwords = row.Utterance.ToString().ToUpperInvariant();

                       //   var Entities = EntityLists.Where(x => utterwords.Any(y => x.EnityName.ToString().ToUpperInvariant().Contains(y.ToUpperInvariant()))).ToList();



                        //var regexp = new Regex(@"(\stest[\s,\.])|(^test[\s,\.])");
                        //var Entities = new List<EntityList>();
                        
                        //foreach (EntityList str in EntityLists)
                        //{
                        //    regexp = new Regex(@"(\s" + str.EnityName.ToUpper() + @"[\s,\"",])");
                        //   if ( regexp.Match(utterwords).Success )
                        //        Entities.Add(str);

                        //}


                  



                        var Entities = EntityLists.Where(x => utterwords.Contains(x.EnityName.ToUpperInvariant() )).ToList();


                        JSONstring = JSONstring + @"
                           {
                           
                           'text':'" + row.Utterance.ToString() + @"',
                          'intentName': '" + row.Intent.ToString() + @"',
                           'entityLabels': [";
                        int entitycntr = 0;
                        foreach (EntityList en in Entities)
                        {
                            int startIndex = 0;
                            int endIndex = 0;
                            startIndex = row.Utterance.ToString().ToUpperInvariant().IndexOf(en.EnityName.ToUpperInvariant());
                            endIndex = startIndex + ( en.EnityName.Length-1);
                            if (entitycntr > 0)
                                JSONstring = JSONstring + ",";

                            JSONstring = JSONstring + @" { 
                            'entityName':'" + en.EntityType + @"',
                            'startCharIndex':" + startIndex + @",
                            'endCharIndex':" + endIndex + @"
                            }";
                            entitycntr++;
                        }

                        JSONstring = JSONstring + @"    ]
                                }
                          ";
                    EndText:;

                    }


                    JSONstring = JSONstring + @"    ]
                                }
                          ";



                    kb = JSONstring;
                }




            }
            catch (Exception ex)
            {

            }
        }
        public static Task<string>  CreateAppRequest()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            var postParams = new Dictionary<string, string> { { "name", LUIS_appName }, { "culture", LUIS_appCulture } };

            try
            {
                var uri = "https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/" + queryString;

                HttpRequestMessage requestMessage = new HttpRequestMessage(new HttpMethod("POST"), uri);

                if (postParams != null)
                {

                    requestMessage.Content = new FormUrlEncodedContent(postParams);   // This is where your content gets added to the request body
                }

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", LUIS_authoringKey);



                HttpResponseMessage response;


                response = client.SendAsync(requestMessage).Result;
                try
                {
                    string apiResponse = response.Content.ReadAsStringAsync().Result;
                    apiResponse = apiResponse.Replace("\"", "");
                    AppId = apiResponse;
                    // Attempt to deserialise the reponse to the desired type, otherwise throw an expetion with the response from the api.
                    if (apiResponse != "")
                    {

                       if (response.Content.ReadAsStringAsync().IsCompletedSuccessfully)
                        return Task.FromResult( apiResponse);
                        else
                        return Task.FromResult($"An error ocurred while calling the API. It responded with the following message: {response.StatusCode} {response.ReasonPhrase}");
                    }
                    else
                        throw new Exception();
                }
                catch (Exception ex)
                {
                    return Task.FromResult($"An error ocurred while calling the API. It responded with the following message: {response.StatusCode} {response.ReasonPhrase}");
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        public static Task<string> UploadIntentsRequest()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            try
            {
                var uri = "https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/{appId}/versions/{versionId}/intents" + queryString;

                uri = uri.Replace("{appId}", AppId).Replace("{versionId}", LUIS_versionId);


              
                string apiPromises = string.Empty;
                foreach (string intent in IntentList)
                {
                    var postParams = new Dictionary<string, string> { { "name", intent } };
                    HttpRequestMessage requestMessage = new HttpRequestMessage(new HttpMethod("POST"), uri);

                    if (postParams != null)
                    {

                        requestMessage.Content = new FormUrlEncodedContent(postParams);   // This is where your content gets added to the request body
                    }

                    // Request headers
                    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", LUIS_authoringKey);

                    HttpResponseMessage response;
                    response = client.SendAsync(requestMessage).Result;
                    try
                    {
                        string apiResponse = response.Content.ReadAsStringAsync().Status.ToString();

                        // Attempt to deserialise the reponse to the desired type, otherwise throw an expetion with the response from the api.
                        if (apiResponse != "")
                            apiPromises = apiPromises + @"
                                  "
                              +  (" Response of adding Intent : " + intent + " : " + apiResponse);
                        else
                            throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        apiPromises = apiPromises + @"
                                  "
                             + "An error ocurred while calling the API. It responded with the following message: { " + response.StatusCode + "} {" + response.ReasonPhrase + "}";
                    }
                   
                }
                return Task.FromResult(apiPromises);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static Task<string> UploadEntitiesRequest()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            try
            {
                var uri= "https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/{appId}/versions/{versionId}/entities" + queryString;

                uri = uri.Replace("{appId}", AppId.ToString()).Replace("{versionId}", LUIS_versionId);


                string apiPromises = string.Empty;
                foreach (EntityList entity in EntityTypeList)
                {
                    var postParams = new Dictionary<string, string> { { "name", entity.EntityType } };

                    HttpRequestMessage requestMessage = new HttpRequestMessage(new HttpMethod("POST"), uri);

                    if (postParams != null)
                    {

                        requestMessage.Content = new FormUrlEncodedContent(postParams);   // This is where your content gets added to the request body
                    }

                    // Request headers
                    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", LUIS_authoringKey);

                    HttpResponseMessage response;
                    response = client.SendAsync(requestMessage).Result;
                    try
                    {
                        string apiResponse = response.Content.ReadAsStringAsync().Result;

                        // Attempt to deserialise the reponse to the desired type, otherwise throw an expetion with the response from the api.
                        if (apiResponse != "")
                            apiPromises = apiPromises + @"
                                  "
                              + (" Response of adding Intent : " + entity + " : " + apiResponse);
                        else
                            throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        apiPromises = apiPromises + @"
                                  "
                             + "An error ocurred while calling the API. It responded with the following message: { " + response.StatusCode + "} {" + response.ReasonPhrase + "}";
                    }

                }
                return Task.FromResult(apiPromises);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static Task<string> UploadUtterancesRequest()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            try
            {
                var uri = "https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/{appId}/versions/{versionId}/examples" + queryString;

                uri = uri.Replace("{appId}", AppId.ToString()).Replace("{versionId}", LUIS_versionId);
                List<SplittedUtterance> pages = getPagesForBatch(kb, BatchCount);

               

             
                string UtterancePromises = string.Empty;
                int Batchcntr = 0;

                foreach (SplittedUtterance utterancePartial in pages)
                {
                    
                    var postParams = JsonConvert.SerializeObject(utterancePartial);
                    postParams = postParams.Remove(postParams.Length - 1);
                    postParams = postParams.Replace("{\"utterances\":", "");

                    HttpRequestMessage requestMessage = new HttpRequestMessage(new HttpMethod("POST"), uri);

                    if (postParams != null)
                    {
                        var content = new StringContent(postParams, Encoding.UTF8, "application/json");
                        requestMessage.Content = content;
                        // This is where your content gets added to the request body
                    }

                    // Request headers
                    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", LUIS_authoringKey);

                    HttpResponseMessage response;
                    response = client.SendAsync(requestMessage).Result;
                    try
                    {
                        string apiResponse = response.Content.ReadAsStringAsync().Result;

                        // Attempt to deserialise the reponse to the desired type, otherwise throw an expetion with the response from the api.
                        if (apiResponse != "")
                            UtterancePromises = UtterancePromises + @"
                                  "
                              + (" Response of adding  batch: " + Batchcntr + apiResponse);
                        else
                            throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        UtterancePromises = UtterancePromises + @"
                                  "
                              + "An error ocurred while calling the API. It responded with the following message: { " + response.StatusCode + "} {" + response.ReasonPhrase + "}";
                    }

                    Batchcntr++;

                }
                return Task.FromResult(UtterancePromises);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static Task<string> TrainingRequest()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            try
            {
            
                var uri = "https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/{appId}/versions/{versionId}/train" + queryString;

                uri = uri.Replace("{appId}", AppId.ToString()).Replace("{versionId}", LUIS_versionId);


                string apiPromises = string.Empty;


                HttpRequestMessage requestMessage = new HttpRequestMessage(new HttpMethod("POST"), uri);

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", LUIS_authoringKey);

                    HttpResponseMessage response;
                    response = client.SendAsync(requestMessage).Result;
                    try
                    {
                        string apiResponse = response.Content.ReadAsStringAsync().Result;

                        // Attempt to deserialise the reponse to the desired type, otherwise throw an expetion with the response from the api.
                        if (apiResponse != "")
                            apiPromises = apiPromises + @"
                                  "
                              + (" Response of Training Version : " + LUIS_versionId + " : " + apiResponse);
                        else
                            throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        apiPromises = apiPromises + @"
                                  "
                             + "An error ocurred while calling the API. It responded with the following message: { " + response.StatusCode + "} {" + response.ReasonPhrase + "}";
                    }

                
                return Task.FromResult(apiPromises);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static List<SplittedUtterance> getPagesForBatch (string batch, int maxItems)
        {

            try
            {
               
                var currentPage = 0;
                RootObject jsonObject = JsonConvert.DeserializeObject<RootObject>(batch);
                List<Utterance> BatchUtterance = jsonObject.utterances;

                var pageCount = (BatchUtterance.Count % maxItems == 0) ? Math.Round((decimal)BatchUtterance.Count / maxItems) : Math.Round((decimal)(BatchUtterance.Count / maxItems) + 1);
                List<SplittedUtterance> BatchData = new List<SplittedUtterance>();


                for ( int i = 0; i < pageCount; i++)
                {
                    SplittedUtterance batrecord = new SplittedUtterance();
                    batrecord.utterances = new List<Utterance>();
                    var currentStart = currentPage * maxItems;
                    var remainingrecords = BatchUtterance.Count - currentStart;
                    var currentEnd = currentStart + ((remainingrecords < maxItems)? remainingrecords : maxItems ) ;
                    var RecordsToFetch =  (remainingrecords < maxItems) ? remainingrecords : maxItems;

                    var pagedBatch = BatchUtterance.GetRange(currentStart, RecordsToFetch);

                    var j = 0;

                    //foreach (Utterance utt in pagedBatch)
                    //{
                    //    utt.ExampleId = j++;
                    //}

                    batrecord.utterances.AddRange(pagedBatch);

                    BatchData.Add(batrecord);

                    currentPage++;
                }
                return BatchData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

}



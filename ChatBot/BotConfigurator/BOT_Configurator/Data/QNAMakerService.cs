﻿using ExcelDataReader;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Services_Setup.Data
{
    public class QNAMaker_Service          
    {

        private const string subscriptionKeyVar = "5dade351dcd243849b79cbbb2f89f06a";
        private const string endpointVar = "https://qnaprogdata.cognitiveservices.azure.com";

        private static readonly string subscriptionKey = Environment.GetEnvironmentVariable(subscriptionKeyVar);
        private static readonly string endpoint = Environment.GetEnvironmentVariable(endpointVar);
        private static readonly string KBName = "KB QNA Manipulated";
        // Represents the various elements used to create HTTP request URIs
        // for QnA Maker operations.
        static string service = "/qnamaker/v4.0";
        static string method = "/knowledgebases/create";


        /// <summary>
        /// Defines the data source used to create the knowledge base.
        /// The data source includes a QnA pair, with metadata, 
        /// the URL for the QnA Maker FAQ article, and 
        /// the URL for the Azure Bot Service FAQ article.
        /// </summary>
        static string kb = @"
{
  'name': 'QnA Maker 26Feb',
  'qnaList': [
    {
      'id': 0,
      'answer': 'You can use our REST APIs to manage your knowledge base. See here for details: https://westus.dev.cognitive.microsoft.com/docs/services/58994a073d9e04097c7ba6fe/operations/58994a073d9e041ad42d9baa',
      'source': 'Custom Editorial',
      'questions': [
        'How do I programmatically update my knowledge base?'
      ],
      'metadata': [
        {
          'name': 'category',
          'value': 'api'
        }
      ]
    }
  ],
  'urls': [
    'https://docs.microsoft.com/en-in/azure/cognitive-services/qnamaker/faqs'
  ],
  'files': []
}
";
        static QNAMaker_Service()
        {
            subscriptionKey =subscriptionKeyVar;
            endpoint = endpointVar;
         }

        public struct Response
        {
            public HttpResponseHeaders headers;
            public string response;

            public Response(HttpResponseHeaders headers, string response)
            {
                this.headers = headers;
                this.response = response;
            }
        }

        public class QNAqueryobj
        {
            public double QuestionId { set; get; }
            public string Answer { set; get; }
            public string Source { set; get; }
            public List<string> Questions { set; get; }
            public List<string> Metanames { set; get; }
            public List<string> Metavalues { set; get; }


        }

        /// <summary>
        /// Formats and indents JSON for display.
        /// </summary>
        /// <param name="s">The JSON to format and indent.</param>
        /// <returns>A string containing formatted and indented JSON.</returns>
        public static string PrettyPrint(string s)
        {
            return JsonConvert.SerializeObject(JsonConvert.DeserializeObject(s), Formatting.Indented);
        }


        public static void ExportToJSON(Stream file)
        {          
            try
            {
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
               
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateReader(file))
                {
                    var x_config = new ExcelDataSetConfiguration();
                    x_config.ConfigureDataTable = tableReader => new ExcelDataTableConfiguration() { UseHeaderRow = true };
                    DataSet result = excelReader.AsDataSet(x_config);

                    var resultobjquery = from r in result.Tables[0].AsEnumerable()
                                    group r by r.Field<double>("QNAID") into IdGroup
                                         let questions = IdGroup.Select(r => r.Field<string>("Question")).OfType<string>().ToList()
                                         let metanames = IdGroup.Select(r => r.Field<string>("key for tagging")).OfType<string>().ToList()
                                         let metavalues = IdGroup.Select(r => r.Field<string>("value of key")).OfType<string>().ToList()
                                         let answers = IdGroup.Select(r => r.Field<string>("Answer")).OfType<string>().ToList()
                                         let Sources = IdGroup.Select(r => r.Field<string>("Source")).OfType<string>().ToList()
                                         select new QNAqueryobj
                                         {
                                             QuestionId = IdGroup.Key,
                                             Answer = answers.SingleOrDefault().ToString(),
                                             Source = Sources.SingleOrDefault().ToString(),
                                             Questions = questions,
                                             Metanames = metanames,
                                             Metavalues = metavalues

                                         };

                    var resultobj = resultobjquery.Distinct().ToList();



                    string JSONstring = @"
                        {  'name': '" + KBName + @"',

                          'qnaList': [
                             ";

                    
                    foreach (QNAqueryobj row in resultobj)
                    {
                        if (JSONstring.Contains("questions"))
                            JSONstring = JSONstring + ",";
                        JSONstring = JSONstring + @"
                           {
                           'id':" + row.QuestionId.ToString() + @",
                          'answer': '" + row.Answer.ToString() + @"',
                          'source': '" + row.Source.ToString() + @"',   
                          'questions': [";
                        int cntr = 0;
                        foreach (string q in row.Questions)
                        {
                            if (cntr > 0)
                                JSONstring = JSONstring + ",'" + q + "'";
                            else
                                JSONstring = JSONstring + "'" + q + "'";
                            cntr = cntr + 1;
                        }

                        cntr = 0;

                        JSONstring = JSONstring + @"  ],
                         'metadata': [
                          ";
                        foreach (string r in row.Metanames)
                        {

                            if (cntr <= 0)
                            {
                                JSONstring = JSONstring + "{ 'name': '" + r + "',";
                                JSONstring = JSONstring + "'value': '" +  ( row.Metavalues != null ? ((row.Metavalues.Count > cntr) ? row.Metavalues[cntr] : "") : "") + "' }";
                            }
                            else
                            {
                                JSONstring = JSONstring + @"
                                ,{ 'name': '" + r + "',";
                                JSONstring = JSONstring + "'value': '" + ( row.Metavalues != null ? ((row.Metavalues.Count > cntr) ? row.Metavalues[cntr] : "") : "" ) + "' }";
                            }
                            cntr = cntr + 1;
                        }


                        JSONstring = JSONstring + @"                                 
                               ] ";

                        JSONstring = JSONstring + "}";

                    }

                    JSONstring = JSONstring + @"   
                           ],   ";
                    
                    JSONstring = JSONstring + @"      'urls': [],
                         'files': []
                       } ";

                    kb = JSONstring;
                }

               


            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Asynchronously sends a POST HTTP request.
        /// </summary>
        /// <param name="uri">The URI of the HTTP request.</param>
        /// <param name="body">The body of the HTTP request.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}(QnAMaker.Program.Response)"/> 
        /// object that represents the HTTP response."</returns>
        public async static Task<Response> Post(string uri, string body)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                request.Headers.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                var response = await client.SendAsync(request);
                var responseBody = await response.Content.ReadAsStringAsync();
                return new Response(response.Headers, responseBody);
            }
        }

        /// <summary>
        /// Asynchronously sends a GET HTTP request.
        /// </summary>
        /// <param name="uri">The URI of the HTTP request.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}(QnAMaker.Program.Response)"/> 
        /// object that represents the HTTP response."</returns>
        public async static Task<Response> Get(string uri)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Get;
                request.RequestUri = new Uri(uri);
                request.Headers.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                var response = await client.SendAsync(request);
                var responseBody = await response.Content.ReadAsStringAsync();
                return new Response(response.Headers, responseBody);
            }
        }

        /// <summary>
        /// Creates a knowledge base.
        /// </summary>
        /// <param name="kb">The data source for the knowledge base.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}(QnAMaker.Program.Response)"/> 
        /// object that represents the HTTP response."</returns>
        /// <remarks>The method constructs the URI to create a knowledge base in QnA Maker, and then
        /// asynchronously invokes the <see cref="QnAMaker.Program.Post(string, string)"/> method
        /// to send the HTTP request.</remarks>
        public async static Task<Response> PostCreateKB()
        {
            // Builds the HTTP request URI.
            string uri = endpoint + service + method;

            // Writes the HTTP request URI to the console, for display purposes.
            Console.WriteLine("Calling " + uri + ".");

            // Asynchronously invokes the Post(string, string) method, using the
            // HTTP request URI and the specified data source.
            return await Post(uri, kb);
        }

        /// <summary>
        /// Gets the status of the specified QnA Maker operation.
        /// </summary>
        /// <param name="operation">The QnA Maker operation to check.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}(QnAMaker.Program.Response)"/> 
        /// object that represents the HTTP response."</returns>
        /// <remarks>The method constructs the URI to get the status of a QnA Maker operation, and
        /// then asynchronously invokes the <see cref="QnAMaker.Program.Get(string)"/> method
        /// to send the HTTP request.</remarks>
        public async static Task<Response> GetStatus(string operation)
        {
            // Builds the HTTP request URI.
            string uri = endpoint + service + operation;

            // Writes the HTTP request URI to the console, for display purposes.
            Console.WriteLine("Calling " + uri + ".");

            // Asynchronously invokes the Get(string) method, using the
            // HTTP request URI.
            return await Get(uri);
        }

      


    }
}
